﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    private WebCamTexture backCam;
    private static Camera _instance;

    public static Camera Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<Camera>();
                if (_instance == null)
                {
                    GameObject go = new GameObject();
                    go.name = typeof(Camera).Name;
                    _instance = go.AddComponent<Camera>();
                    DontDestroyOnLoad(go);

                    _instance.initializeCamera();
                }
            }
            return _instance;
        }
    }

    public void initializeCamera()
    {
        WebCamDevice[] devices = WebCamTexture.devices;

        if (devices.Length == 0)
        {
            Debug.Log("No camera detected");
            return;
        }

        for (int i = 0; i < devices.Length; i++)
        {
            #if (UNITY_ANDROID)
            if (!devices[i].isFrontFacing)
            {
                backCam = new WebCamTexture(devices[i].name, Screen.width, Screen.height);
            }
            #endif
            #if (UNITY_EDITOR)
            backCam = new WebCamTexture(devices[i].name, Screen.width, Screen.height);
            #endif
        }
    }

    public WebCamTexture BackCam
    {
        get
        {
            return backCam;
        }
    }
}
