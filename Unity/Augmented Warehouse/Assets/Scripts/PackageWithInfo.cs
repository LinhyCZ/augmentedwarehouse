﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PackageWithInfoList
{
    public List<PackageWithInfo> list;
}

[Serializable]
public class PackageWithInfo
{
    public string baleni_id;

    public string baleni_qr_code;

    public string cena_ks;

    public string kategorie;

    public string nazev_jednotky;

    public string pocet_jednotek_vks;

    public string pocet_ks;

    public string police_id;

    public string popis_zbozi;

    public string pozice_id;

    public string sloupec_id;

    public string slozeni;

    public string stojan_id;

    public string zbozi_ean;

    override
    public string ToString()
    {
        return string.Format("{0}: {1} (Pozice: {2})", this.baleni_id, this.popis_zbozi, this.pozice_id);
    }
}

public static class PackageWithInfoUtils
{
    public static PackageWithInfoList getZboziInSkladPageable(int pageSize, int page)
    {
        PackageWithInfoList packageList = new PackageWithInfoList();
        packageList = UnityEngine.JsonUtility.FromJson<PackageWithInfoList>("{\n\"list\":" + RestClient.Instance.GetPackageWithInfoPageable(VariablesScript.WarehouseId + "?pageNo=" + page + "&pageSize=" + pageSize) + "\n}");
        return packageList;
    }

    public static PackageWithInfoList getZboziInSkladByQRPositionPageable(string qr, int pageSize, int page)
    {
        PackageWithInfoList packageList = new PackageWithInfoList();
        packageList = UnityEngine.JsonUtility.FromJson<PackageWithInfoList>("{\n\"list\":" + RestClient.Instance.GetPackageWithInfoPageable(VariablesScript.WarehouseId + "/byPositionQr/" + qr + "?pageNo=" + page + "&pageSize=" + pageSize) + "\n}");
        return packageList;
    }

    public static PackageWithInfoList getZboziInSkladByQRPageable(string qr, int pageSize, int page)
    {
        PackageWithInfoList packageList = new PackageWithInfoList();
        packageList = UnityEngine.JsonUtility.FromJson<PackageWithInfoList>("{\n\"list\":" + RestClient.Instance.GetPackageWithInfoPageable(VariablesScript.WarehouseId + "/byQr/" + qr + "?pageNo=" + page + "&pageSize=" + pageSize) + "\n}");
        return packageList;
    }

    public static string getPackageInfoString(PackageWithInfo package)
    {
        string toReturn = "";
        toReturn += "Složení: " + package.slozeni + "\n";
        toReturn += "Kategorie: " + package.kategorie + "\n";
        toReturn += "Počet kusů v balení: " + package.pocet_ks + "\n";
        toReturn += "Jeden ks obsahuje: " + package.pocet_jednotek_vks + package.nazev_jednotky + "\n";
        toReturn += "Cena za ks: " + package.cena_ks + "\n";
        toReturn += "Umístění: \n";
        toReturn += "   - stojan: " + package.stojan_id + "\n";
        toReturn += "   - police: " + package.police_id + "\n";
        toReturn += "   - sloupec: " + package.sloupec_id + "\n";
        toReturn += "   - pozice: " + package.pozice_id + "\n";
        return toReturn;
    }
}
