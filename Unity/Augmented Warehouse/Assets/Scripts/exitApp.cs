﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using static Camera;

public class exitApp : MonoBehaviour
{
    public void exitApplication()
    {
        Camera.Instance.BackCam.Stop();
        Debug.Log("Exiting app");
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
    #endif
    }
}
