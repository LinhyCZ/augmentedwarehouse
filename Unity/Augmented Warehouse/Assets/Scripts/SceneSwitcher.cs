﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
    private static SceneSwitcher _instance;

    public static SceneSwitcher Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<SceneSwitcher>();
                if (_instance == null)
                {
                    GameObject go = new GameObject();
                    go.name = typeof(SceneSwitcher).Name;
                    _instance = go.AddComponent<SceneSwitcher>();
                    DontDestroyOnLoad(go);
                }
            }
            return _instance;
        }
    }

    public void showInfoScene()
    {
        Debug.Log("Showing info scene");
        SceneManager.LoadScene("InfoScene");
    }

    public void showPhotoScene()
    {
        Debug.Log("Showing camera scene");
        SceneManager.LoadScene("PhoneCamera");
    }

    public void showWarehouseScene()
    {
        Debug.Log("Showing warehouse select scene");
        SceneManager.LoadScene("SelectWarehouse");
    }

    public void showSettingsScene()
    {
        Debug.Log("Showing settings scene");
        SceneManager.LoadScene("Settings");
    }
}
