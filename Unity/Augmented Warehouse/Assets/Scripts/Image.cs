﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Image
{
    public string obrazek;
}

public static class ImageUtils
{
    public static Image getImage(int packageId)
    {
        Image image = new Image();
        image = UnityEngine.JsonUtility.FromJson<Image>(RestClient.Instance.GetImage(packageId.ToString()));
        return image;
    }
}
