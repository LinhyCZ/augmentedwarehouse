using System;
using System.Collections.Generic;
using UnityEngine;
using static RestClient;

[Serializable]
public class WarehouseList
{
    public List<Warehouse> list;
}

[Serializable]
public class Warehouse
{
    public string adresa;

    public string cislo_orientacni;

    public string cislo_popisne;

    public string id;

    public string mesto;

    public string p_sc;

    override
    public string ToString()
    {
        return string.Format("{0}: {1} {2}/{3}, {4} {5}", this.id, this.adresa, this.cislo_popisne, this.cislo_orientacni, this.p_sc, this.mesto);
    }

}

public static class WarehouseUtilities
{
    public static WarehouseList GetWarehouses()
    {
        WarehouseList whList = new WarehouseList();
        whList = UnityEngine.JsonUtility.FromJson<WarehouseList>("{\n\"list\":" + RestClient.Instance.GetWarehouses() + "\n}");
        return whList;
    }
}