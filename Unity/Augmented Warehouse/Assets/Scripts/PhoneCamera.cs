﻿using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using ZXing;

using static VariablesScript;
using static SceneSwitcher;
using static Camera;

public class PhoneCamera : MonoBehaviour
{
    private bool camAvaliable = true;
    private WebCamTexture backCam;
    private Texture defaultBackground;
    private Color32[] backCamData;
    private int cameraWidth;
    private int cameraHeight;
    private Thread t;

    public RawImage background;
    public AspectRatioFitter fit;

    public Text qrtxt;
    public Image panel;

    public bool processingQR = false;
    public bool processedQR = false;

    private int counter = 0;

    private void Start()
    {
        processingQR = false;
        processedQR = false;
        defaultBackground = background.texture;
        backCam = Camera.Instance.BackCam;

        if (backCam == null)
        {
            camAvaliable = false;
            Debug.Log("Back camera not found");
            return;
        }

        //Plays camera image
        backCam.Play();

        //WebCamTexture inherits from textures and can be used as a texture
        background.texture = backCam;
    }


    //Unity is not great with camera, delayed few frames
    private void Update()
    {
        if (!camAvaliable)
            return;

        if (processedQR)
        {
            processingQR = true;
            processedQR = false;
            SceneSwitcher.Instance.showInfoScene();
        }

        if (!processingQR)
        {
            backCamData = backCam.GetPixels32();
            cameraHeight = backCam.height;
            cameraWidth = backCam.width;
            processingQR = true;
            t = new Thread(processQR);
            t.Start();
        }


        //Calculates the ratio, float is important we would lose precision otherwise
        float ratio = (float)backCam.width / (float)backCam.height;
        fit.aspectRatio = ratio;

        if (backCam.videoRotationAngle == 90)
        {
            float scaleY = backCam.videoVerticallyMirrored ? -1 : 1;
            background.rectTransform.localScale = new Vector3(1 / 2f, scaleY / 2f, 1 / 2f);

        }
        else
        {
            float scaleY = backCam.videoVerticallyMirrored ? -1f : 1f;
            background.rectTransform.localScale = new Vector3(1 / 1f, scaleY / 1f, 1 / 1f);
        }


        int orient = -backCam.videoRotationAngle;
        background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);
        counter++;
    }


    public void setWarehouseScene()
    {
        SceneSwitcher.Instance.showWarehouseScene();
    }

    public void showAllStuff()
    {
        VariablesScript.QRCode = null;
        SceneSwitcher.Instance.showInfoScene();
    }

    public void processQR()
    {
        Debug.Log("Trying to process QR code");
        try
        {
            counter = 0;
            IBarcodeReader barcodeReader = new BarcodeReader();
            // decode the current frame
            var result = barcodeReader.Decode(backCamData,
                cameraWidth, cameraHeight);
            if (result != null && !processedQR)
            {
                backCamData = null;
                Debug.Log("DECODED TEXT FROM QR: " + result.Text);
                VariablesScript.QRCode = result.Text;
                processedQR = true;
            }
            processingQR = false;
        }
        catch (Exception ex) {
            processingQR = false;
            Debug.LogWarning(ex.Message);
        }
    }
}
