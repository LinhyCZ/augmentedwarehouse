﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using static VariablesScript;

public class SettingsScene : MonoBehaviour
{
    public InputField pageSizeField;
    // Start is called before the first frame update
    void Start()
    {
        int pageSize = VariablesScript.PageSize;

        pageSizeField.text = pageSize + "";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OkButtonHandler()
    {
        int result;

        bool success = int.TryParse(pageSizeField.text, out result);

        if (success && result > 0)
        {
            VariablesScript.PageSize = result;
        }
        else
        {
            int pageSize = VariablesScript.PageSize;

            pageSizeField.text = pageSize + "";
        }
    }
}
