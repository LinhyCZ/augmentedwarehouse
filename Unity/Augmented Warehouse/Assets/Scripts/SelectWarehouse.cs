﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using static VariablesScript;
using static SceneSwitcher;

public class SelectWarehouse : MonoBehaviour
{
    public Dropdown dropdown;
    List<string> options;

    // Start is called before the first frame update
    void Start()
    {
        options = new List<string>();
        List<Warehouse> warehouses = WarehouseUtilities.GetWarehouses().list;
        foreach (var option in warehouses)
        {
            options.Add(option.ToString());
        }

        dropdown.ClearOptions();
        dropdown.AddOptions(options);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setWarehouseId()
    {
        string menuValue = options[dropdown.value];
        string split = menuValue.Split(':')[0];

        int warehouseId = int.Parse(split);

        Debug.Log("Index: " + warehouseId);

        VariablesScript.WarehouseId = warehouseId;
        SceneSwitcher.Instance.showPhotoScene();
    }
}
