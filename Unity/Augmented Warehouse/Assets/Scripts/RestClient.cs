﻿
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class RestClient : MonoBehaviour
{

    private static RestClient _instance;

    private string requestTempData;


    public static RestClient Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<RestClient>();
                if (_instance == null)
                {
                    GameObject go = new GameObject();
                    go.name = typeof(RestClient).Name;
                    _instance = go.AddComponent<RestClient>();
                    DontDestroyOnLoad(go);
                }
            }
            return _instance;
        }
    }

    public static string id = "tomami";
    public static string pass = "tomami";

    public static string authAPI = "/api/authenticate";

    public static string url = "http://linhy.cz:8082";
    //public static string url = "http://localhost:8080";
    //public static string url = "http://192.168.1.28:8080";
    public static string token;


    public void setToken()
    {
        IEnumerator e = setTokenRoutine();

        while (e.MoveNext())
        {
            /* wait for get to finish */
        }

    }

    public IEnumerator setTokenRoutine()
    {
        string jsonData = "{ \"password\": \""
            + RestClient.pass
            + "\", \"rememberMe\": true, \"username\": \""
            + RestClient.id
            + "\"}";
        string url = RestClient.url + RestClient.authAPI;

        Debug.Log(url);


        using (UnityWebRequest www = UnityWebRequest.Post(url, jsonData))
        {
            www.SetRequestHeader("Content-Type", "application/json");
            www.uploadHandler = new UploadHandlerRaw(System.Text.Encoding.UTF8.GetBytes(jsonData));

            yield return www.SendWebRequest();
            if (www.isNetworkError)
            {
                Debug.Log("webrequest error: " + www.error);
            }
            else
            {
                while (!www.isDone)
                    yield return null;


                if (www.isDone)
                {
                    string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                    RestClient.token = jsonResult.Split('"')[3];
                    Debug.Log("Token set");

                }
            }

        }
    }

    public string GetPackageWithInfo(string parameters)
    {
        return GetRequest(url + "/custom-api/zbozi/" + parameters);
    }

    public string GetPackageWithInfoPageable(string parameters)
    {
        return GetRequest(url + "/custom-api/pageable/zbozi/" + parameters);
    }

    public string GetImage(string parameters)
    {
        return GetRequest(url + "/custom-api/image/" + parameters);
    }

    public string GetWarehouses()
    {
        return GetRequest(url + "/custom-api/warehouses");
    }

    private string GetRequest(string url)
    {

        IEnumerator e = Get(url);

        while (e.MoveNext())
        {
            /* wait for get to finish */
        }
            
        return this.requestTempData;
    }

    private IEnumerator Get(string url)
    {
        if (token == null)
        {
            setToken();
        }

        using (UnityWebRequest www = UnityWebRequest.Get(url))
        {
            www.SetRequestHeader("Content-Type", "application/json");
            www.SetRequestHeader("Authorization", "Bearer " + RestClient.token);
            yield return www.SendWebRequest();
            if (www.isNetworkError)
            {
                Debug.Log("webrequest error: " + www.error);
                    

            }
            else
            {
                while (!www.isDone)
                    yield return null;

                if(www.isDone)
                {
                    string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                    this.requestTempData = jsonResult;
                }
            }
        }
    }
}

