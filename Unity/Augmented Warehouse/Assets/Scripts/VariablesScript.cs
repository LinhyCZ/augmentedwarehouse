﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using static PackageWithInfoList;

public static class VariablesScript
{
    private static string qrCode;

    private static int warehouseId;

    private static PackageWithInfoList packageWithInfoList;

    private static int pageSize = 10;

    public static string QRCode
    {
        get
        {
            return qrCode;
        }

        set
        {
            qrCode = value;
        }
    }

    public static int WarehouseId
    {
        get
        {
            return warehouseId;
        }

        set
        {
            warehouseId = value;
        }
    }

    public static PackageWithInfoList PackageWithInfoList
    {
        get
        {
            return packageWithInfoList;
        }

        set
        {
            packageWithInfoList = value;
        }
    }

    public static int PageSize
    {
        get
        {
            return pageSize;
        }

        set
        {
            pageSize = value;
        }
    }
}
