﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using static VariablesScript;
using static PackageWithInfoUtils;
using static ImageUtils;

public class InfoScene : MonoBehaviour
{
    private int pageNumber = 0;

    public Text pageNumberText;

    public GameObject itemPrefab;

    // Start is called before the first frame update
    void Start()
    {
        updateData();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void plusButtonHandler()
    {
        pageNumber++;
        if(!updateData())
        {
            pageNumber--;
        }

        updateLabel();
    }

    public void minusButtonHandler()
    {
        if (pageNumber > 0)
        {
            pageNumber--;
            if (!updateData())
            {
                pageNumber++;
            }

            updateLabel();
        }
    }

    bool updateData()
    {
        PackageWithInfoList packageList;
        
        if (VariablesScript.QRCode == null)
        {
            packageList = PackageWithInfoUtils.getZboziInSkladPageable(VariablesScript.PageSize, pageNumber);
        }
        else
        {
            string qr = VariablesScript.QRCode;
            Debug.Log("QR: " + qr);
            if (qr.Length > 9)
            {
                string positionIdentificator = qr.Substring(0, 9);
                Debug.Log("Identificator: " + positionIdentificator);
                if (positionIdentificator.Equals("position_")) {
                    string qrWithoutIdentificator = qr.Substring(9, qr.Length - 9);
                    Debug.Log("QR without ID: " + qrWithoutIdentificator);
                    packageList = PackageWithInfoUtils.getZboziInSkladByQRPositionPageable(qrWithoutIdentificator, VariablesScript.PageSize, pageNumber);
                } else
                {
                    packageList = PackageWithInfoUtils.getZboziInSkladByQRPageable(qr, VariablesScript.PageSize, pageNumber);
                }
            } else
            {
                packageList = PackageWithInfoUtils.getZboziInSkladByQRPageable(qr, VariablesScript.PageSize, pageNumber);
            }
        }

        if (packageList.list.Count != 0)
        {
            clearPage();

            foreach (var package in packageList.list)
            {
                int packageId = int.Parse(package.baleni_id);

                GameObject go = Instantiate(itemPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                go.transform.SetParent(GameObject.Find("ScrollContent").transform, false);
                go.name = "ItemDetail" + packageId;
                go.transform.Find("LoadImage").GetComponent<Button>().onClick.AddListener(() => { loadImage(packageId); });
                go.transform.Find("Content").GetComponent<Text>().text = PackageWithInfoUtils.getPackageInfoString(package);
                go.transform.Find("Header").GetComponent<Text>().text = package.popis_zbozi;
            }

            return true;
        }

        return false;
    }

    void updateLabel()
    {
        pageNumberText.text = (pageNumber + 1) + "";
    }

    void clearPage()
    {
        foreach(Transform child in GameObject.Find("ScrollContent").transform)
        {
            Destroy(child.gameObject);
        }
    }

    public void loadImage(int id)
    {
        Debug.Log("Package ID: " + id);
        Image image = ImageUtils.getImage(id);

        List<Transform> objects = new List<Transform>();

        Transform searchTransform = GameObject.Find("ScrollContent").transform;
        foreach (Transform child in searchTransform)
        {
            Debug.Log(child.name);
            if (child.name.Equals("ItemDetail" + id))
            {
                objects.Add(child);
            }
        }

        byte[] imageBytes = Convert.FromBase64String(image.obrazek);
        Texture2D tex = new Texture2D(2, 2);
        tex.LoadImage(imageBytes);
        Sprite sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);

        foreach (Transform tr in objects)
        {
            Destroy(tr.Find("LoadImage").GetComponent<Button>());
            UnityEngine.UI.Image img = tr.Find("Image").GetComponent<UnityEngine.UI.Image>();
            img.overrideSprite = sprite;
            img.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, img.rectTransform.rect.width);
        }

        Debug.Log("Size: " + objects.Count);
    }
}
