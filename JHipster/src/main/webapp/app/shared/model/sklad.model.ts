import { IStojan } from 'app/shared/model/stojan.model';

export interface ISklad {
  id?: number;
  adresa?: string;
  cisloPopisne?: number;
  cisloOrientacni?: number;
  mesto?: string;
  pSC?: number;
  stojans?: IStojan[];
}

export class Sklad implements ISklad {
  constructor(
    public id?: number,
    public adresa?: string,
    public cisloPopisne?: number,
    public cisloOrientacni?: number,
    public mesto?: string,
    public pSC?: number,
    public stojans?: IStojan[]
  ) {}
}
