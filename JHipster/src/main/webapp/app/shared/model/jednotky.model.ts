import { ITypKS } from 'app/shared/model/typ-ks.model';
import { HlavniJednotky } from 'app/shared/model/enumerations/hlavni-jednotky.model';

export interface IJednotky {
  id?: number;
  hlavniJednotka?: HlavniJednotky;
  ratioDoHlavniJednotky?: number;
  nazevJednotky?: string;
  typKS?: ITypKS[];
}

export class Jednotky implements IJednotky {
  constructor(
    public id?: number,
    public hlavniJednotka?: HlavniJednotky,
    public ratioDoHlavniJednotky?: number,
    public nazevJednotky?: string,
    public typKS?: ITypKS[]
  ) {}
}
