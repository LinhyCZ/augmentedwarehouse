import { IPozice } from 'app/shared/model/pozice.model';
import { IPolice } from 'app/shared/model/police.model';

export interface ISloupec {
  id?: number;
  pozices?: IPozice[];
  police?: IPolice;
}

export class Sloupec implements ISloupec {
  constructor(public id?: number, public pozices?: IPozice[], public police?: IPolice) {}
}
