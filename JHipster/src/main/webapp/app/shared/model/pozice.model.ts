import { IBaleni } from 'app/shared/model/baleni.model';
import { ISloupec } from 'app/shared/model/sloupec.model';

export interface IPozice {
  id?: number;
  qrCode?: string;
  balenis?: IBaleni[];
  sloupec?: ISloupec;
}

export class Pozice implements IPozice {
  constructor(public id?: number, public qrCode?: string, public balenis?: IBaleni[], public sloupec?: ISloupec) {}
}
