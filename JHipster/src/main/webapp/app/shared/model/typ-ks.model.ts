import { IBaleni } from 'app/shared/model/baleni.model';
import { IJednotky } from 'app/shared/model/jednotky.model';

export interface ITypKS {
  id?: number;
  pocetJednotekVKS?: number;
  cenaKS?: number;
  balenis?: IBaleni[];
  jednotky?: IJednotky;
}

export class TypKS implements ITypKS {
  constructor(
    public id?: number,
    public pocetJednotekVKS?: number,
    public cenaKS?: number,
    public balenis?: IBaleni[],
    public jednotky?: IJednotky
  ) {}
}
