import { ISloupec } from 'app/shared/model/sloupec.model';
import { IStojan } from 'app/shared/model/stojan.model';

export interface IPolice {
  id?: number;
  sloupecs?: ISloupec[];
  stojan?: IStojan;
}

export class Police implements IPolice {
  constructor(public id?: number, public sloupecs?: ISloupec[], public stojan?: IStojan) {}
}
