import { IBaleni } from 'app/shared/model/baleni.model';

export interface IZbozi {
  id?: number;
  kategorie?: string;
  slozeni?: string;
  popisZbozi?: string;
  ean?: string;
  obrazek?: any;
  balenis?: IBaleni[];
}

export class Zbozi implements IZbozi {
  constructor(
    public id?: number,
    public kategorie?: string,
    public slozeni?: string,
    public popisZbozi?: string,
    public ean?: string,
    public obrazek?: any,
    public balenis?: IBaleni[]
  ) {}
}
