import { IZbozi } from 'app/shared/model/zbozi.model';
import { ITypKS } from 'app/shared/model/typ-ks.model';
import { IPozice } from 'app/shared/model/pozice.model';

export interface IBaleni {
  id?: number;
  pocetKS?: number;
  qrCode?: string;
  zbozi?: IZbozi;
  typKS?: ITypKS;
  pozices?: IPozice[];
}

export class Baleni implements IBaleni {
  constructor(
    public id?: number,
    public pocetKS?: number,
    public qrCode?: string,
    public zbozi?: IZbozi,
    public typKS?: ITypKS,
    public pozices?: IPozice[]
  ) {}
}
