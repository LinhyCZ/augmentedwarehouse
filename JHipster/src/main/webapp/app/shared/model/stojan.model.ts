import { IPolice } from 'app/shared/model/police.model';
import { ISklad } from 'app/shared/model/sklad.model';

export interface IStojan {
  id?: number;
  police?: IPolice[];
  sklad?: ISklad;
}

export class Stojan implements IStojan {
  constructor(public id?: number, public police?: IPolice[], public sklad?: ISklad) {}
}
