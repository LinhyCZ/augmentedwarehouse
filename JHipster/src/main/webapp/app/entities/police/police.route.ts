import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IPolice, Police } from 'app/shared/model/police.model';
import { PoliceService } from './police.service';
import { PoliceComponent } from './police.component';
import { PoliceDetailComponent } from './police-detail.component';
import { PoliceUpdateComponent } from './police-update.component';

@Injectable({ providedIn: 'root' })
export class PoliceResolve implements Resolve<IPolice> {
  constructor(private service: PoliceService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPolice> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((police: HttpResponse<Police>) => {
          if (police.body) {
            return of(police.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Police());
  }
}

export const policeRoute: Routes = [
  {
    path: '',
    component: PoliceComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.police.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: PoliceDetailComponent,
    resolve: {
      police: PoliceResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.police.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PoliceUpdateComponent,
    resolve: {
      police: PoliceResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.police.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PoliceUpdateComponent,
    resolve: {
      police: PoliceResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.police.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
