import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IPolice } from 'app/shared/model/police.model';

type EntityResponseType = HttpResponse<IPolice>;
type EntityArrayResponseType = HttpResponse<IPolice[]>;

@Injectable({ providedIn: 'root' })
export class PoliceService {
  public resourceUrl = SERVER_API_URL + 'api/police';

  constructor(protected http: HttpClient) {}

  create(police: IPolice): Observable<EntityResponseType> {
    return this.http.post<IPolice>(this.resourceUrl, police, { observe: 'response' });
  }

  update(police: IPolice): Observable<EntityResponseType> {
    return this.http.put<IPolice>(this.resourceUrl, police, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPolice>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPolice[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
