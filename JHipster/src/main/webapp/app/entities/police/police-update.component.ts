import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IPolice, Police } from 'app/shared/model/police.model';
import { PoliceService } from './police.service';
import { IStojan } from 'app/shared/model/stojan.model';
import { StojanService } from 'app/entities/stojan/stojan.service';

@Component({
  selector: 'jhi-police-update',
  templateUrl: './police-update.component.html'
})
export class PoliceUpdateComponent implements OnInit {
  isSaving = false;
  stojans: IStojan[] = [];

  editForm = this.fb.group({
    id: [],
    stojan: []
  });

  constructor(
    protected policeService: PoliceService,
    protected stojanService: StojanService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ police }) => {
      this.updateForm(police);

      this.stojanService.query().subscribe((res: HttpResponse<IStojan[]>) => (this.stojans = res.body || []));
    });
  }

  updateForm(police: IPolice): void {
    this.editForm.patchValue({
      id: police.id,
      stojan: police.stojan
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const police = this.createFromForm();
    if (police.id !== undefined) {
      this.subscribeToSaveResponse(this.policeService.update(police));
    } else {
      this.subscribeToSaveResponse(this.policeService.create(police));
    }
  }

  private createFromForm(): IPolice {
    return {
      ...new Police(),
      id: this.editForm.get(['id'])!.value,
      stojan: this.editForm.get(['stojan'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPolice>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IStojan): any {
    return item.id;
  }
}
