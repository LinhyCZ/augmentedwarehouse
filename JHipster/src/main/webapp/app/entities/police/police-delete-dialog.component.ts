import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPolice } from 'app/shared/model/police.model';
import { PoliceService } from './police.service';

@Component({
  templateUrl: './police-delete-dialog.component.html'
})
export class PoliceDeleteDialogComponent {
  police?: IPolice;

  constructor(protected policeService: PoliceService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.policeService.delete(id).subscribe(() => {
      this.eventManager.broadcast('policeListModification');
      this.activeModal.close();
    });
  }
}
