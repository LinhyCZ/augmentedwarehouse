import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AugmentedWarehouseSharedModule } from 'app/shared/shared.module';
import { PoliceComponent } from './police.component';
import { PoliceDetailComponent } from './police-detail.component';
import { PoliceUpdateComponent } from './police-update.component';
import { PoliceDeleteDialogComponent } from './police-delete-dialog.component';
import { policeRoute } from './police.route';

@NgModule({
  imports: [AugmentedWarehouseSharedModule, RouterModule.forChild(policeRoute)],
  declarations: [PoliceComponent, PoliceDetailComponent, PoliceUpdateComponent, PoliceDeleteDialogComponent],
  entryComponents: [PoliceDeleteDialogComponent]
})
export class AugmentedWarehousePoliceModule {}
