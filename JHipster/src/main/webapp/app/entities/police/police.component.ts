import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPolice } from 'app/shared/model/police.model';
import { PoliceService } from './police.service';
import { PoliceDeleteDialogComponent } from './police-delete-dialog.component';

@Component({
  selector: 'jhi-police',
  templateUrl: './police.component.html'
})
export class PoliceComponent implements OnInit, OnDestroy {
  police?: IPolice[];
  eventSubscriber?: Subscription;

  constructor(protected policeService: PoliceService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.policeService.query().subscribe((res: HttpResponse<IPolice[]>) => (this.police = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInPolice();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IPolice): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInPolice(): void {
    this.eventSubscriber = this.eventManager.subscribe('policeListModification', () => this.loadAll());
  }

  delete(police: IPolice): void {
    const modalRef = this.modalService.open(PoliceDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.police = police;
  }
}
