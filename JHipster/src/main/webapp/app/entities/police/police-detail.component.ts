import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPolice } from 'app/shared/model/police.model';

@Component({
  selector: 'jhi-police-detail',
  templateUrl: './police-detail.component.html'
})
export class PoliceDetailComponent implements OnInit {
  police: IPolice | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ police }) => (this.police = police));
  }

  previousState(): void {
    window.history.back();
  }
}
