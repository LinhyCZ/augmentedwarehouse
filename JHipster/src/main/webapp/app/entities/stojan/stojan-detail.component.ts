import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IStojan } from 'app/shared/model/stojan.model';

@Component({
  selector: 'jhi-stojan-detail',
  templateUrl: './stojan-detail.component.html'
})
export class StojanDetailComponent implements OnInit {
  stojan: IStojan | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ stojan }) => (this.stojan = stojan));
  }

  previousState(): void {
    window.history.back();
  }
}
