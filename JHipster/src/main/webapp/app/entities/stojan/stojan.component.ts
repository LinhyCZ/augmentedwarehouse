import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IStojan } from 'app/shared/model/stojan.model';
import { StojanService } from './stojan.service';
import { StojanDeleteDialogComponent } from './stojan-delete-dialog.component';

@Component({
  selector: 'jhi-stojan',
  templateUrl: './stojan.component.html'
})
export class StojanComponent implements OnInit, OnDestroy {
  stojans?: IStojan[];
  eventSubscriber?: Subscription;

  constructor(protected stojanService: StojanService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.stojanService.query().subscribe((res: HttpResponse<IStojan[]>) => (this.stojans = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInStojans();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IStojan): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInStojans(): void {
    this.eventSubscriber = this.eventManager.subscribe('stojanListModification', () => this.loadAll());
  }

  delete(stojan: IStojan): void {
    const modalRef = this.modalService.open(StojanDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.stojan = stojan;
  }
}
