import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IStojan } from 'app/shared/model/stojan.model';
import { StojanService } from './stojan.service';

@Component({
  templateUrl: './stojan-delete-dialog.component.html'
})
export class StojanDeleteDialogComponent {
  stojan?: IStojan;

  constructor(protected stojanService: StojanService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.stojanService.delete(id).subscribe(() => {
      this.eventManager.broadcast('stojanListModification');
      this.activeModal.close();
    });
  }
}
