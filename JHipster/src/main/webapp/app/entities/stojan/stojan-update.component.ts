import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IStojan, Stojan } from 'app/shared/model/stojan.model';
import { StojanService } from './stojan.service';
import { ISklad } from 'app/shared/model/sklad.model';
import { SkladService } from 'app/entities/sklad/sklad.service';

@Component({
  selector: 'jhi-stojan-update',
  templateUrl: './stojan-update.component.html'
})
export class StojanUpdateComponent implements OnInit {
  isSaving = false;
  sklads: ISklad[] = [];

  editForm = this.fb.group({
    id: [],
    sklad: []
  });

  constructor(
    protected stojanService: StojanService,
    protected skladService: SkladService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ stojan }) => {
      this.updateForm(stojan);

      this.skladService.query().subscribe((res: HttpResponse<ISklad[]>) => (this.sklads = res.body || []));
    });
  }

  updateForm(stojan: IStojan): void {
    this.editForm.patchValue({
      id: stojan.id,
      sklad: stojan.sklad
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const stojan = this.createFromForm();
    if (stojan.id !== undefined) {
      this.subscribeToSaveResponse(this.stojanService.update(stojan));
    } else {
      this.subscribeToSaveResponse(this.stojanService.create(stojan));
    }
  }

  private createFromForm(): IStojan {
    return {
      ...new Stojan(),
      id: this.editForm.get(['id'])!.value,
      sklad: this.editForm.get(['sklad'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStojan>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ISklad): any {
    return item.id;
  }
}
