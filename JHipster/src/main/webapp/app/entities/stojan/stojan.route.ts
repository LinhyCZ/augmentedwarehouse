import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IStojan, Stojan } from 'app/shared/model/stojan.model';
import { StojanService } from './stojan.service';
import { StojanComponent } from './stojan.component';
import { StojanDetailComponent } from './stojan-detail.component';
import { StojanUpdateComponent } from './stojan-update.component';

@Injectable({ providedIn: 'root' })
export class StojanResolve implements Resolve<IStojan> {
  constructor(private service: StojanService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IStojan> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((stojan: HttpResponse<Stojan>) => {
          if (stojan.body) {
            return of(stojan.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Stojan());
  }
}

export const stojanRoute: Routes = [
  {
    path: '',
    component: StojanComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.stojan.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: StojanDetailComponent,
    resolve: {
      stojan: StojanResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.stojan.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: StojanUpdateComponent,
    resolve: {
      stojan: StojanResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.stojan.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: StojanUpdateComponent,
    resolve: {
      stojan: StojanResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.stojan.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
