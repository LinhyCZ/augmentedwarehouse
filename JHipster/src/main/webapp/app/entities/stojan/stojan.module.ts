import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AugmentedWarehouseSharedModule } from 'app/shared/shared.module';
import { StojanComponent } from './stojan.component';
import { StojanDetailComponent } from './stojan-detail.component';
import { StojanUpdateComponent } from './stojan-update.component';
import { StojanDeleteDialogComponent } from './stojan-delete-dialog.component';
import { stojanRoute } from './stojan.route';

@NgModule({
  imports: [AugmentedWarehouseSharedModule, RouterModule.forChild(stojanRoute)],
  declarations: [StojanComponent, StojanDetailComponent, StojanUpdateComponent, StojanDeleteDialogComponent],
  entryComponents: [StojanDeleteDialogComponent]
})
export class AugmentedWarehouseStojanModule {}
