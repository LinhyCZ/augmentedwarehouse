import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'sklad',
        loadChildren: () => import('./sklad/sklad.module').then(m => m.AugmentedWarehouseSkladModule)
      },
      {
        path: 'stojan',
        loadChildren: () => import('./stojan/stojan.module').then(m => m.AugmentedWarehouseStojanModule)
      },
      {
        path: 'police',
        loadChildren: () => import('./police/police.module').then(m => m.AugmentedWarehousePoliceModule)
      },
      {
        path: 'sloupec',
        loadChildren: () => import('./sloupec/sloupec.module').then(m => m.AugmentedWarehouseSloupecModule)
      },
      {
        path: 'pozice',
        loadChildren: () => import('./pozice/pozice.module').then(m => m.AugmentedWarehousePoziceModule)
      },
      {
        path: 'baleni',
        loadChildren: () => import('./baleni/baleni.module').then(m => m.AugmentedWarehouseBaleniModule)
      },
      {
        path: 'typ-ks',
        loadChildren: () => import('./typ-ks/typ-ks.module').then(m => m.AugmentedWarehouseTypKSModule)
      },
      {
        path: 'zbozi',
        loadChildren: () => import('./zbozi/zbozi.module').then(m => m.AugmentedWarehouseZboziModule)
      },
      {
        path: 'jednotky',
        loadChildren: () => import('./jednotky/jednotky.module').then(m => m.AugmentedWarehouseJednotkyModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class AugmentedWarehouseEntityModule {}
