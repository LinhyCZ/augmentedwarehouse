import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITypKS, TypKS } from 'app/shared/model/typ-ks.model';
import { TypKSService } from './typ-ks.service';
import { TypKSComponent } from './typ-ks.component';
import { TypKSDetailComponent } from './typ-ks-detail.component';
import { TypKSUpdateComponent } from './typ-ks-update.component';

@Injectable({ providedIn: 'root' })
export class TypKSResolve implements Resolve<ITypKS> {
  constructor(private service: TypKSService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITypKS> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((typKS: HttpResponse<TypKS>) => {
          if (typKS.body) {
            return of(typKS.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TypKS());
  }
}

export const typKSRoute: Routes = [
  {
    path: '',
    component: TypKSComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.typKS.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TypKSDetailComponent,
    resolve: {
      typKS: TypKSResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.typKS.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TypKSUpdateComponent,
    resolve: {
      typKS: TypKSResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.typKS.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TypKSUpdateComponent,
    resolve: {
      typKS: TypKSResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.typKS.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
