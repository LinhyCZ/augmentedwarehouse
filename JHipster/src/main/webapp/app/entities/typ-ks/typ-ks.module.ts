import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AugmentedWarehouseSharedModule } from 'app/shared/shared.module';
import { TypKSComponent } from './typ-ks.component';
import { TypKSDetailComponent } from './typ-ks-detail.component';
import { TypKSUpdateComponent } from './typ-ks-update.component';
import { TypKSDeleteDialogComponent } from './typ-ks-delete-dialog.component';
import { typKSRoute } from './typ-ks.route';

@NgModule({
  imports: [AugmentedWarehouseSharedModule, RouterModule.forChild(typKSRoute)],
  declarations: [TypKSComponent, TypKSDetailComponent, TypKSUpdateComponent, TypKSDeleteDialogComponent],
  entryComponents: [TypKSDeleteDialogComponent]
})
export class AugmentedWarehouseTypKSModule {}
