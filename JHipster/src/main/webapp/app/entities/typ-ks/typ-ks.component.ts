import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ITypKS } from 'app/shared/model/typ-ks.model';
import { TypKSService } from './typ-ks.service';
import { TypKSDeleteDialogComponent } from './typ-ks-delete-dialog.component';

@Component({
  selector: 'jhi-typ-ks',
  templateUrl: './typ-ks.component.html'
})
export class TypKSComponent implements OnInit, OnDestroy {
  typKS?: ITypKS[];
  eventSubscriber?: Subscription;

  constructor(protected typKSService: TypKSService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.typKSService.query().subscribe((res: HttpResponse<ITypKS[]>) => (this.typKS = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInTypKS();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ITypKS): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInTypKS(): void {
    this.eventSubscriber = this.eventManager.subscribe('typKSListModification', () => this.loadAll());
  }

  delete(typKS: ITypKS): void {
    const modalRef = this.modalService.open(TypKSDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.typKS = typKS;
  }
}
