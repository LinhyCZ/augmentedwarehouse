import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITypKS } from 'app/shared/model/typ-ks.model';

@Component({
  selector: 'jhi-typ-ks-detail',
  templateUrl: './typ-ks-detail.component.html'
})
export class TypKSDetailComponent implements OnInit {
  typKS: ITypKS | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ typKS }) => (this.typKS = typKS));
  }

  previousState(): void {
    window.history.back();
  }
}
