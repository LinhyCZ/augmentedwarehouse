import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITypKS } from 'app/shared/model/typ-ks.model';

type EntityResponseType = HttpResponse<ITypKS>;
type EntityArrayResponseType = HttpResponse<ITypKS[]>;

@Injectable({ providedIn: 'root' })
export class TypKSService {
  public resourceUrl = SERVER_API_URL + 'api/typ-ks';

  constructor(protected http: HttpClient) {}

  create(typKS: ITypKS): Observable<EntityResponseType> {
    return this.http.post<ITypKS>(this.resourceUrl, typKS, { observe: 'response' });
  }

  update(typKS: ITypKS): Observable<EntityResponseType> {
    return this.http.put<ITypKS>(this.resourceUrl, typKS, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITypKS>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITypKS[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
