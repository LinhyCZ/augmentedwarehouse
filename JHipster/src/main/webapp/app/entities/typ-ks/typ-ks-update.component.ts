import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ITypKS, TypKS } from 'app/shared/model/typ-ks.model';
import { TypKSService } from './typ-ks.service';
import { IJednotky } from 'app/shared/model/jednotky.model';
import { JednotkyService } from 'app/entities/jednotky/jednotky.service';

@Component({
  selector: 'jhi-typ-ks-update',
  templateUrl: './typ-ks-update.component.html'
})
export class TypKSUpdateComponent implements OnInit {
  isSaving = false;
  jednotkies: IJednotky[] = [];

  editForm = this.fb.group({
    id: [],
    pocetJednotekVKS: [],
    cenaKS: [],
    jednotky: []
  });

  constructor(
    protected typKSService: TypKSService,
    protected jednotkyService: JednotkyService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ typKS }) => {
      this.updateForm(typKS);

      this.jednotkyService.query().subscribe((res: HttpResponse<IJednotky[]>) => (this.jednotkies = res.body || []));
    });
  }

  updateForm(typKS: ITypKS): void {
    this.editForm.patchValue({
      id: typKS.id,
      pocetJednotekVKS: typKS.pocetJednotekVKS,
      cenaKS: typKS.cenaKS,
      jednotky: typKS.jednotky
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const typKS = this.createFromForm();
    if (typKS.id !== undefined) {
      this.subscribeToSaveResponse(this.typKSService.update(typKS));
    } else {
      this.subscribeToSaveResponse(this.typKSService.create(typKS));
    }
  }

  private createFromForm(): ITypKS {
    return {
      ...new TypKS(),
      id: this.editForm.get(['id'])!.value,
      pocetJednotekVKS: this.editForm.get(['pocetJednotekVKS'])!.value,
      cenaKS: this.editForm.get(['cenaKS'])!.value,
      jednotky: this.editForm.get(['jednotky'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITypKS>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IJednotky): any {
    return item.id;
  }
}
