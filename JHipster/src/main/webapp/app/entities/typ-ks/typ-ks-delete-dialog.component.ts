import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITypKS } from 'app/shared/model/typ-ks.model';
import { TypKSService } from './typ-ks.service';

@Component({
  templateUrl: './typ-ks-delete-dialog.component.html'
})
export class TypKSDeleteDialogComponent {
  typKS?: ITypKS;

  constructor(protected typKSService: TypKSService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.typKSService.delete(id).subscribe(() => {
      this.eventManager.broadcast('typKSListModification');
      this.activeModal.close();
    });
  }
}
