import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ISloupec } from 'app/shared/model/sloupec.model';
import { SloupecService } from './sloupec.service';
import { SloupecDeleteDialogComponent } from './sloupec-delete-dialog.component';

@Component({
  selector: 'jhi-sloupec',
  templateUrl: './sloupec.component.html'
})
export class SloupecComponent implements OnInit, OnDestroy {
  sloupecs?: ISloupec[];
  eventSubscriber?: Subscription;

  constructor(protected sloupecService: SloupecService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.sloupecService.query().subscribe((res: HttpResponse<ISloupec[]>) => (this.sloupecs = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInSloupecs();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ISloupec): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInSloupecs(): void {
    this.eventSubscriber = this.eventManager.subscribe('sloupecListModification', () => this.loadAll());
  }

  delete(sloupec: ISloupec): void {
    const modalRef = this.modalService.open(SloupecDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.sloupec = sloupec;
  }
}
