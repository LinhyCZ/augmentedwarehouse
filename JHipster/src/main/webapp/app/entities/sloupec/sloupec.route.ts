import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ISloupec, Sloupec } from 'app/shared/model/sloupec.model';
import { SloupecService } from './sloupec.service';
import { SloupecComponent } from './sloupec.component';
import { SloupecDetailComponent } from './sloupec-detail.component';
import { SloupecUpdateComponent } from './sloupec-update.component';

@Injectable({ providedIn: 'root' })
export class SloupecResolve implements Resolve<ISloupec> {
  constructor(private service: SloupecService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISloupec> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((sloupec: HttpResponse<Sloupec>) => {
          if (sloupec.body) {
            return of(sloupec.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Sloupec());
  }
}

export const sloupecRoute: Routes = [
  {
    path: '',
    component: SloupecComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.sloupec.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SloupecDetailComponent,
    resolve: {
      sloupec: SloupecResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.sloupec.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SloupecUpdateComponent,
    resolve: {
      sloupec: SloupecResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.sloupec.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SloupecUpdateComponent,
    resolve: {
      sloupec: SloupecResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.sloupec.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
