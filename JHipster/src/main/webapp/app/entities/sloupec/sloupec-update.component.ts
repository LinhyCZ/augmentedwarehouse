import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ISloupec, Sloupec } from 'app/shared/model/sloupec.model';
import { SloupecService } from './sloupec.service';
import { IPolice } from 'app/shared/model/police.model';
import { PoliceService } from 'app/entities/police/police.service';

@Component({
  selector: 'jhi-sloupec-update',
  templateUrl: './sloupec-update.component.html'
})
export class SloupecUpdateComponent implements OnInit {
  isSaving = false;
  police: IPolice[] = [];

  editForm = this.fb.group({
    id: [],
    police: []
  });

  constructor(
    protected sloupecService: SloupecService,
    protected policeService: PoliceService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ sloupec }) => {
      this.updateForm(sloupec);

      this.policeService.query().subscribe((res: HttpResponse<IPolice[]>) => (this.police = res.body || []));
    });
  }

  updateForm(sloupec: ISloupec): void {
    this.editForm.patchValue({
      id: sloupec.id,
      police: sloupec.police
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const sloupec = this.createFromForm();
    if (sloupec.id !== undefined) {
      this.subscribeToSaveResponse(this.sloupecService.update(sloupec));
    } else {
      this.subscribeToSaveResponse(this.sloupecService.create(sloupec));
    }
  }

  private createFromForm(): ISloupec {
    return {
      ...new Sloupec(),
      id: this.editForm.get(['id'])!.value,
      police: this.editForm.get(['police'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISloupec>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IPolice): any {
    return item.id;
  }
}
