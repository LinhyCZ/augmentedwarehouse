import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ISloupec } from 'app/shared/model/sloupec.model';

type EntityResponseType = HttpResponse<ISloupec>;
type EntityArrayResponseType = HttpResponse<ISloupec[]>;

@Injectable({ providedIn: 'root' })
export class SloupecService {
  public resourceUrl = SERVER_API_URL + 'api/sloupecs';

  constructor(protected http: HttpClient) {}

  create(sloupec: ISloupec): Observable<EntityResponseType> {
    return this.http.post<ISloupec>(this.resourceUrl, sloupec, { observe: 'response' });
  }

  update(sloupec: ISloupec): Observable<EntityResponseType> {
    return this.http.put<ISloupec>(this.resourceUrl, sloupec, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISloupec>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISloupec[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
