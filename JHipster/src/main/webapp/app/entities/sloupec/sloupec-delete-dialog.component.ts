import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISloupec } from 'app/shared/model/sloupec.model';
import { SloupecService } from './sloupec.service';

@Component({
  templateUrl: './sloupec-delete-dialog.component.html'
})
export class SloupecDeleteDialogComponent {
  sloupec?: ISloupec;

  constructor(protected sloupecService: SloupecService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.sloupecService.delete(id).subscribe(() => {
      this.eventManager.broadcast('sloupecListModification');
      this.activeModal.close();
    });
  }
}
