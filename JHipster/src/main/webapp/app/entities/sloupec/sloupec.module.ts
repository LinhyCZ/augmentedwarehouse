import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AugmentedWarehouseSharedModule } from 'app/shared/shared.module';
import { SloupecComponent } from './sloupec.component';
import { SloupecDetailComponent } from './sloupec-detail.component';
import { SloupecUpdateComponent } from './sloupec-update.component';
import { SloupecDeleteDialogComponent } from './sloupec-delete-dialog.component';
import { sloupecRoute } from './sloupec.route';

@NgModule({
  imports: [AugmentedWarehouseSharedModule, RouterModule.forChild(sloupecRoute)],
  declarations: [SloupecComponent, SloupecDetailComponent, SloupecUpdateComponent, SloupecDeleteDialogComponent],
  entryComponents: [SloupecDeleteDialogComponent]
})
export class AugmentedWarehouseSloupecModule {}
