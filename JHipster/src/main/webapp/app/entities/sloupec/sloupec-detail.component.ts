import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISloupec } from 'app/shared/model/sloupec.model';

@Component({
  selector: 'jhi-sloupec-detail',
  templateUrl: './sloupec-detail.component.html'
})
export class SloupecDetailComponent implements OnInit {
  sloupec: ISloupec | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ sloupec }) => (this.sloupec = sloupec));
  }

  previousState(): void {
    window.history.back();
  }
}
