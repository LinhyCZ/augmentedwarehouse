import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IZbozi } from 'app/shared/model/zbozi.model';
import { ZboziService } from './zbozi.service';

@Component({
  templateUrl: './zbozi-delete-dialog.component.html'
})
export class ZboziDeleteDialogComponent {
  zbozi?: IZbozi;

  constructor(protected zboziService: ZboziService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.zboziService.delete(id).subscribe(() => {
      this.eventManager.broadcast('zboziListModification');
      this.activeModal.close();
    });
  }
}
