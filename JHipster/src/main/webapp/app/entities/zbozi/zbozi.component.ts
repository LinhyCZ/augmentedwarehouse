import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IZbozi } from 'app/shared/model/zbozi.model';
import { ZboziService } from './zbozi.service';
import { ZboziDeleteDialogComponent } from './zbozi-delete-dialog.component';

@Component({
  selector: 'jhi-zbozi',
  templateUrl: './zbozi.component.html'
})
export class ZboziComponent implements OnInit, OnDestroy {
  zbozis?: IZbozi[];
  eventSubscriber?: Subscription;

  constructor(
    protected zboziService: ZboziService,
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.zboziService.query().subscribe((res: HttpResponse<IZbozi[]>) => (this.zbozis = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInZbozis();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IZbozi): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    return this.dataUtils.openFile(contentType, base64String);
  }

  registerChangeInZbozis(): void {
    this.eventSubscriber = this.eventManager.subscribe('zboziListModification', () => this.loadAll());
  }

  delete(zbozi: IZbozi): void {
    const modalRef = this.modalService.open(ZboziDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.zbozi = zbozi;
  }
}
