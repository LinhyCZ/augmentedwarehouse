import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AugmentedWarehouseSharedModule } from 'app/shared/shared.module';
import { ZboziComponent } from './zbozi.component';
import { ZboziDetailComponent } from './zbozi-detail.component';
import { ZboziUpdateComponent } from './zbozi-update.component';
import { ZboziDeleteDialogComponent } from './zbozi-delete-dialog.component';
import { zboziRoute } from './zbozi.route';

@NgModule({
  imports: [AugmentedWarehouseSharedModule, RouterModule.forChild(zboziRoute)],
  declarations: [ZboziComponent, ZboziDetailComponent, ZboziUpdateComponent, ZboziDeleteDialogComponent],
  entryComponents: [ZboziDeleteDialogComponent]
})
export class AugmentedWarehouseZboziModule {}
