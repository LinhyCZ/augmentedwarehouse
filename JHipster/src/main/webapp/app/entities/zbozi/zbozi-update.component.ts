import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { IZbozi, Zbozi } from 'app/shared/model/zbozi.model';
import { ZboziService } from './zbozi.service';
import { AlertError } from 'app/shared/alert/alert-error.model';

@Component({
  selector: 'jhi-zbozi-update',
  templateUrl: './zbozi-update.component.html'
})
export class ZboziUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    kategorie: [],
    slozeni: [],
    popisZbozi: [],
    ean: [],
    obrazek: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected zboziService: ZboziService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ zbozi }) => {
      this.updateForm(zbozi);
    });
  }

  updateForm(zbozi: IZbozi): void {
    this.editForm.patchValue({
      id: zbozi.id,
      kategorie: zbozi.kategorie,
      slozeni: zbozi.slozeni,
      popisZbozi: zbozi.popisZbozi,
      ean: zbozi.ean,
      obrazek: zbozi.obrazek
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('augmentedWarehouseApp.error', { ...err, key: 'error.file.' + err.key })
      );
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const zbozi = this.createFromForm();
    if (zbozi.id !== undefined) {
      this.subscribeToSaveResponse(this.zboziService.update(zbozi));
    } else {
      this.subscribeToSaveResponse(this.zboziService.create(zbozi));
    }
  }

  private createFromForm(): IZbozi {
    return {
      ...new Zbozi(),
      id: this.editForm.get(['id'])!.value,
      kategorie: this.editForm.get(['kategorie'])!.value,
      slozeni: this.editForm.get(['slozeni'])!.value,
      popisZbozi: this.editForm.get(['popisZbozi'])!.value,
      ean: this.editForm.get(['ean'])!.value,
      obrazek: this.editForm.get(['obrazek'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IZbozi>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
