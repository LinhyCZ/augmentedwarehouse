import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IZbozi, Zbozi } from 'app/shared/model/zbozi.model';
import { ZboziService } from './zbozi.service';
import { ZboziComponent } from './zbozi.component';
import { ZboziDetailComponent } from './zbozi-detail.component';
import { ZboziUpdateComponent } from './zbozi-update.component';

@Injectable({ providedIn: 'root' })
export class ZboziResolve implements Resolve<IZbozi> {
  constructor(private service: ZboziService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IZbozi> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((zbozi: HttpResponse<Zbozi>) => {
          if (zbozi.body) {
            return of(zbozi.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Zbozi());
  }
}

export const zboziRoute: Routes = [
  {
    path: '',
    component: ZboziComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.zbozi.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ZboziDetailComponent,
    resolve: {
      zbozi: ZboziResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.zbozi.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ZboziUpdateComponent,
    resolve: {
      zbozi: ZboziResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.zbozi.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ZboziUpdateComponent,
    resolve: {
      zbozi: ZboziResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.zbozi.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
