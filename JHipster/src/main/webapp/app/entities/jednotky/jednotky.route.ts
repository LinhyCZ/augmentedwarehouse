import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IJednotky, Jednotky } from 'app/shared/model/jednotky.model';
import { JednotkyService } from './jednotky.service';
import { JednotkyComponent } from './jednotky.component';
import { JednotkyDetailComponent } from './jednotky-detail.component';
import { JednotkyUpdateComponent } from './jednotky-update.component';

@Injectable({ providedIn: 'root' })
export class JednotkyResolve implements Resolve<IJednotky> {
  constructor(private service: JednotkyService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IJednotky> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((jednotky: HttpResponse<Jednotky>) => {
          if (jednotky.body) {
            return of(jednotky.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Jednotky());
  }
}

export const jednotkyRoute: Routes = [
  {
    path: '',
    component: JednotkyComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.jednotky.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: JednotkyDetailComponent,
    resolve: {
      jednotky: JednotkyResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.jednotky.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: JednotkyUpdateComponent,
    resolve: {
      jednotky: JednotkyResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.jednotky.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: JednotkyUpdateComponent,
    resolve: {
      jednotky: JednotkyResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.jednotky.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
