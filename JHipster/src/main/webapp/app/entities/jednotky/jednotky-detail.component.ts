import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IJednotky } from 'app/shared/model/jednotky.model';

@Component({
  selector: 'jhi-jednotky-detail',
  templateUrl: './jednotky-detail.component.html'
})
export class JednotkyDetailComponent implements OnInit {
  jednotky: IJednotky | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ jednotky }) => (this.jednotky = jednotky));
  }

  previousState(): void {
    window.history.back();
  }
}
