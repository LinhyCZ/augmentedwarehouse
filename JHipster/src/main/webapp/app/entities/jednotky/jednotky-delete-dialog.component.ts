import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IJednotky } from 'app/shared/model/jednotky.model';
import { JednotkyService } from './jednotky.service';

@Component({
  templateUrl: './jednotky-delete-dialog.component.html'
})
export class JednotkyDeleteDialogComponent {
  jednotky?: IJednotky;

  constructor(protected jednotkyService: JednotkyService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.jednotkyService.delete(id).subscribe(() => {
      this.eventManager.broadcast('jednotkyListModification');
      this.activeModal.close();
    });
  }
}
