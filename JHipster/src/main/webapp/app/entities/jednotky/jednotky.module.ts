import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AugmentedWarehouseSharedModule } from 'app/shared/shared.module';
import { JednotkyComponent } from './jednotky.component';
import { JednotkyDetailComponent } from './jednotky-detail.component';
import { JednotkyUpdateComponent } from './jednotky-update.component';
import { JednotkyDeleteDialogComponent } from './jednotky-delete-dialog.component';
import { jednotkyRoute } from './jednotky.route';

@NgModule({
  imports: [AugmentedWarehouseSharedModule, RouterModule.forChild(jednotkyRoute)],
  declarations: [JednotkyComponent, JednotkyDetailComponent, JednotkyUpdateComponent, JednotkyDeleteDialogComponent],
  entryComponents: [JednotkyDeleteDialogComponent]
})
export class AugmentedWarehouseJednotkyModule {}
