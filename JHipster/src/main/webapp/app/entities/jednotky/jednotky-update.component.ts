import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IJednotky, Jednotky } from 'app/shared/model/jednotky.model';
import { JednotkyService } from './jednotky.service';

@Component({
  selector: 'jhi-jednotky-update',
  templateUrl: './jednotky-update.component.html'
})
export class JednotkyUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    hlavniJednotka: [],
    ratioDoHlavniJednotky: [],
    nazevJednotky: []
  });

  constructor(protected jednotkyService: JednotkyService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ jednotky }) => {
      this.updateForm(jednotky);
    });
  }

  updateForm(jednotky: IJednotky): void {
    this.editForm.patchValue({
      id: jednotky.id,
      hlavniJednotka: jednotky.hlavniJednotka,
      ratioDoHlavniJednotky: jednotky.ratioDoHlavniJednotky,
      nazevJednotky: jednotky.nazevJednotky
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const jednotky = this.createFromForm();
    if (jednotky.id !== undefined) {
      this.subscribeToSaveResponse(this.jednotkyService.update(jednotky));
    } else {
      this.subscribeToSaveResponse(this.jednotkyService.create(jednotky));
    }
  }

  private createFromForm(): IJednotky {
    return {
      ...new Jednotky(),
      id: this.editForm.get(['id'])!.value,
      hlavniJednotka: this.editForm.get(['hlavniJednotka'])!.value,
      ratioDoHlavniJednotky: this.editForm.get(['ratioDoHlavniJednotky'])!.value,
      nazevJednotky: this.editForm.get(['nazevJednotky'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IJednotky>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
