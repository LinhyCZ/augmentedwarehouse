import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IJednotky } from 'app/shared/model/jednotky.model';
import { JednotkyService } from './jednotky.service';
import { JednotkyDeleteDialogComponent } from './jednotky-delete-dialog.component';

@Component({
  selector: 'jhi-jednotky',
  templateUrl: './jednotky.component.html'
})
export class JednotkyComponent implements OnInit, OnDestroy {
  jednotkies?: IJednotky[];
  eventSubscriber?: Subscription;

  constructor(protected jednotkyService: JednotkyService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.jednotkyService.query().subscribe((res: HttpResponse<IJednotky[]>) => (this.jednotkies = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInJednotkies();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IJednotky): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInJednotkies(): void {
    this.eventSubscriber = this.eventManager.subscribe('jednotkyListModification', () => this.loadAll());
  }

  delete(jednotky: IJednotky): void {
    const modalRef = this.modalService.open(JednotkyDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.jednotky = jednotky;
  }
}
