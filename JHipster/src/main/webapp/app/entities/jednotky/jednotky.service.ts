import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IJednotky } from 'app/shared/model/jednotky.model';

type EntityResponseType = HttpResponse<IJednotky>;
type EntityArrayResponseType = HttpResponse<IJednotky[]>;

@Injectable({ providedIn: 'root' })
export class JednotkyService {
  public resourceUrl = SERVER_API_URL + 'api/jednotkies';

  constructor(protected http: HttpClient) {}

  create(jednotky: IJednotky): Observable<EntityResponseType> {
    return this.http.post<IJednotky>(this.resourceUrl, jednotky, { observe: 'response' });
  }

  update(jednotky: IJednotky): Observable<EntityResponseType> {
    return this.http.put<IJednotky>(this.resourceUrl, jednotky, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IJednotky>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IJednotky[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
