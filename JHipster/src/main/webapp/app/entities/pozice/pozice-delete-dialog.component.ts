import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPozice } from 'app/shared/model/pozice.model';
import { PoziceService } from './pozice.service';

@Component({
  templateUrl: './pozice-delete-dialog.component.html'
})
export class PoziceDeleteDialogComponent {
  pozice?: IPozice;

  constructor(protected poziceService: PoziceService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.poziceService.delete(id).subscribe(() => {
      this.eventManager.broadcast('poziceListModification');
      this.activeModal.close();
    });
  }
}
