import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IPozice, Pozice } from 'app/shared/model/pozice.model';
import { PoziceService } from './pozice.service';
import { PoziceComponent } from './pozice.component';
import { PoziceDetailComponent } from './pozice-detail.component';
import { PoziceUpdateComponent } from './pozice-update.component';

@Injectable({ providedIn: 'root' })
export class PoziceResolve implements Resolve<IPozice> {
  constructor(private service: PoziceService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPozice> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((pozice: HttpResponse<Pozice>) => {
          if (pozice.body) {
            return of(pozice.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Pozice());
  }
}

export const poziceRoute: Routes = [
  {
    path: '',
    component: PoziceComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.pozice.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: PoziceDetailComponent,
    resolve: {
      pozice: PoziceResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.pozice.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PoziceUpdateComponent,
    resolve: {
      pozice: PoziceResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.pozice.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PoziceUpdateComponent,
    resolve: {
      pozice: PoziceResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.pozice.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
