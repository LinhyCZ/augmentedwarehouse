import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IPozice, Pozice } from 'app/shared/model/pozice.model';
import { PoziceService } from './pozice.service';
import { IBaleni } from 'app/shared/model/baleni.model';
import { BaleniService } from 'app/entities/baleni/baleni.service';
import { ISloupec } from 'app/shared/model/sloupec.model';
import { SloupecService } from 'app/entities/sloupec/sloupec.service';

type SelectableEntity = IBaleni | ISloupec;

@Component({
  selector: 'jhi-pozice-update',
  templateUrl: './pozice-update.component.html'
})
export class PoziceUpdateComponent implements OnInit {
  isSaving = false;
  balenis: IBaleni[] = [];
  sloupecs: ISloupec[] = [];

  editForm = this.fb.group({
    id: [],
    qrCode: [],
    balenis: [],
    sloupec: []
  });

  constructor(
    protected poziceService: PoziceService,
    protected baleniService: BaleniService,
    protected sloupecService: SloupecService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ pozice }) => {
      this.updateForm(pozice);

      this.baleniService.query().subscribe((res: HttpResponse<IBaleni[]>) => (this.balenis = res.body || []));

      this.sloupecService.query().subscribe((res: HttpResponse<ISloupec[]>) => (this.sloupecs = res.body || []));
    });
  }

  updateForm(pozice: IPozice): void {
    this.editForm.patchValue({
      id: pozice.id,
      qrCode: pozice.qrCode,
      balenis: pozice.balenis,
      sloupec: pozice.sloupec
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const pozice = this.createFromForm();
    if (pozice.id !== undefined) {
      this.subscribeToSaveResponse(this.poziceService.update(pozice));
    } else {
      this.subscribeToSaveResponse(this.poziceService.create(pozice));
    }
  }

  private createFromForm(): IPozice {
    return {
      ...new Pozice(),
      id: this.editForm.get(['id'])!.value,
      qrCode: this.editForm.get(['qrCode'])!.value,
      balenis: this.editForm.get(['balenis'])!.value,
      sloupec: this.editForm.get(['sloupec'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPozice>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

  getSelected(selectedVals: IBaleni[], option: IBaleni): IBaleni {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
