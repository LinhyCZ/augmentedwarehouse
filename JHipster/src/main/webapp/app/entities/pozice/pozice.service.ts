import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IPozice } from 'app/shared/model/pozice.model';

type EntityResponseType = HttpResponse<IPozice>;
type EntityArrayResponseType = HttpResponse<IPozice[]>;

@Injectable({ providedIn: 'root' })
export class PoziceService {
  public resourceUrl = SERVER_API_URL + 'api/pozices';

  constructor(protected http: HttpClient) {}

  create(pozice: IPozice): Observable<EntityResponseType> {
    return this.http.post<IPozice>(this.resourceUrl, pozice, { observe: 'response' });
  }

  update(pozice: IPozice): Observable<EntityResponseType> {
    return this.http.put<IPozice>(this.resourceUrl, pozice, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPozice>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPozice[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
