import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPozice } from 'app/shared/model/pozice.model';
import { PoziceService } from './pozice.service';
import { PoziceDeleteDialogComponent } from './pozice-delete-dialog.component';

@Component({
  selector: 'jhi-pozice',
  templateUrl: './pozice.component.html'
})
export class PoziceComponent implements OnInit, OnDestroy {
  pozices?: IPozice[];
  eventSubscriber?: Subscription;

  constructor(protected poziceService: PoziceService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.poziceService.query().subscribe((res: HttpResponse<IPozice[]>) => (this.pozices = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInPozices();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IPozice): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInPozices(): void {
    this.eventSubscriber = this.eventManager.subscribe('poziceListModification', () => this.loadAll());
  }

  delete(pozice: IPozice): void {
    const modalRef = this.modalService.open(PoziceDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.pozice = pozice;
  }
}
