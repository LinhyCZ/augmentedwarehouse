import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPozice } from 'app/shared/model/pozice.model';

@Component({
  selector: 'jhi-pozice-detail',
  templateUrl: './pozice-detail.component.html'
})
export class PoziceDetailComponent implements OnInit {
  pozice: IPozice | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ pozice }) => (this.pozice = pozice));
  }

  previousState(): void {
    window.history.back();
  }
}
