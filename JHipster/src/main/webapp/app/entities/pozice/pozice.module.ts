import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AugmentedWarehouseSharedModule } from 'app/shared/shared.module';
import { PoziceComponent } from './pozice.component';
import { PoziceDetailComponent } from './pozice-detail.component';
import { PoziceUpdateComponent } from './pozice-update.component';
import { PoziceDeleteDialogComponent } from './pozice-delete-dialog.component';
import { poziceRoute } from './pozice.route';

@NgModule({
  imports: [AugmentedWarehouseSharedModule, RouterModule.forChild(poziceRoute)],
  declarations: [PoziceComponent, PoziceDetailComponent, PoziceUpdateComponent, PoziceDeleteDialogComponent],
  entryComponents: [PoziceDeleteDialogComponent]
})
export class AugmentedWarehousePoziceModule {}
