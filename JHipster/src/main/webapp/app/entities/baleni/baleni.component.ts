import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBaleni } from 'app/shared/model/baleni.model';
import { BaleniService } from './baleni.service';
import { BaleniDeleteDialogComponent } from './baleni-delete-dialog.component';

@Component({
  selector: 'jhi-baleni',
  templateUrl: './baleni.component.html'
})
export class BaleniComponent implements OnInit, OnDestroy {
  balenis?: IBaleni[];
  eventSubscriber?: Subscription;

  constructor(protected baleniService: BaleniService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.baleniService.query().subscribe((res: HttpResponse<IBaleni[]>) => (this.balenis = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInBalenis();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IBaleni): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInBalenis(): void {
    this.eventSubscriber = this.eventManager.subscribe('baleniListModification', () => this.loadAll());
  }

  delete(baleni: IBaleni): void {
    const modalRef = this.modalService.open(BaleniDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.baleni = baleni;
  }
}
