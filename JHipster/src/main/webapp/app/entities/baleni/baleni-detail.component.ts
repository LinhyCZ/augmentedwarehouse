import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBaleni } from 'app/shared/model/baleni.model';

@Component({
  selector: 'jhi-baleni-detail',
  templateUrl: './baleni-detail.component.html'
})
export class BaleniDetailComponent implements OnInit {
  baleni: IBaleni | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ baleni }) => (this.baleni = baleni));
  }

  previousState(): void {
    window.history.back();
  }
}
