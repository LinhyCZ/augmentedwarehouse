import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBaleni } from 'app/shared/model/baleni.model';

type EntityResponseType = HttpResponse<IBaleni>;
type EntityArrayResponseType = HttpResponse<IBaleni[]>;

@Injectable({ providedIn: 'root' })
export class BaleniService {
  public resourceUrl = SERVER_API_URL + 'api/balenis';

  constructor(protected http: HttpClient) {}

  create(baleni: IBaleni): Observable<EntityResponseType> {
    return this.http.post<IBaleni>(this.resourceUrl, baleni, { observe: 'response' });
  }

  update(baleni: IBaleni): Observable<EntityResponseType> {
    return this.http.put<IBaleni>(this.resourceUrl, baleni, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBaleni>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBaleni[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
