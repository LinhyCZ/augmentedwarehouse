import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBaleni } from 'app/shared/model/baleni.model';
import { BaleniService } from './baleni.service';

@Component({
  templateUrl: './baleni-delete-dialog.component.html'
})
export class BaleniDeleteDialogComponent {
  baleni?: IBaleni;

  constructor(protected baleniService: BaleniService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.baleniService.delete(id).subscribe(() => {
      this.eventManager.broadcast('baleniListModification');
      this.activeModal.close();
    });
  }
}
