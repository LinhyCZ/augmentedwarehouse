import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBaleni, Baleni } from 'app/shared/model/baleni.model';
import { BaleniService } from './baleni.service';
import { BaleniComponent } from './baleni.component';
import { BaleniDetailComponent } from './baleni-detail.component';
import { BaleniUpdateComponent } from './baleni-update.component';

@Injectable({ providedIn: 'root' })
export class BaleniResolve implements Resolve<IBaleni> {
  constructor(private service: BaleniService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBaleni> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((baleni: HttpResponse<Baleni>) => {
          if (baleni.body) {
            return of(baleni.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Baleni());
  }
}

export const baleniRoute: Routes = [
  {
    path: '',
    component: BaleniComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.baleni.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BaleniDetailComponent,
    resolve: {
      baleni: BaleniResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.baleni.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: BaleniUpdateComponent,
    resolve: {
      baleni: BaleniResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.baleni.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BaleniUpdateComponent,
    resolve: {
      baleni: BaleniResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.baleni.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
