import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AugmentedWarehouseSharedModule } from 'app/shared/shared.module';
import { BaleniComponent } from './baleni.component';
import { BaleniDetailComponent } from './baleni-detail.component';
import { BaleniUpdateComponent } from './baleni-update.component';
import { BaleniDeleteDialogComponent } from './baleni-delete-dialog.component';
import { baleniRoute } from './baleni.route';

@NgModule({
  imports: [AugmentedWarehouseSharedModule, RouterModule.forChild(baleniRoute)],
  declarations: [BaleniComponent, BaleniDetailComponent, BaleniUpdateComponent, BaleniDeleteDialogComponent],
  entryComponents: [BaleniDeleteDialogComponent]
})
export class AugmentedWarehouseBaleniModule {}
