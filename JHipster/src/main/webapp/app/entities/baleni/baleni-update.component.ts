import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IBaleni, Baleni } from 'app/shared/model/baleni.model';
import { BaleniService } from './baleni.service';
import { IZbozi } from 'app/shared/model/zbozi.model';
import { ZboziService } from 'app/entities/zbozi/zbozi.service';
import { ITypKS } from 'app/shared/model/typ-ks.model';
import { TypKSService } from 'app/entities/typ-ks/typ-ks.service';

type SelectableEntity = IZbozi | ITypKS;

@Component({
  selector: 'jhi-baleni-update',
  templateUrl: './baleni-update.component.html'
})
export class BaleniUpdateComponent implements OnInit {
  isSaving = false;
  zbozis: IZbozi[] = [];
  typks: ITypKS[] = [];

  editForm = this.fb.group({
    id: [],
    pocetKS: [],
    qrCode: [],
    zbozi: [],
    typKS: []
  });

  constructor(
    protected baleniService: BaleniService,
    protected zboziService: ZboziService,
    protected typKSService: TypKSService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ baleni }) => {
      this.updateForm(baleni);

      this.zboziService.query().subscribe((res: HttpResponse<IZbozi[]>) => (this.zbozis = res.body || []));

      this.typKSService.query().subscribe((res: HttpResponse<ITypKS[]>) => (this.typks = res.body || []));
    });
  }

  updateForm(baleni: IBaleni): void {
    this.editForm.patchValue({
      id: baleni.id,
      pocetKS: baleni.pocetKS,
      qrCode: baleni.qrCode,
      zbozi: baleni.zbozi,
      typKS: baleni.typKS
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const baleni = this.createFromForm();
    if (baleni.id !== undefined) {
      this.subscribeToSaveResponse(this.baleniService.update(baleni));
    } else {
      this.subscribeToSaveResponse(this.baleniService.create(baleni));
    }
  }

  private createFromForm(): IBaleni {
    return {
      ...new Baleni(),
      id: this.editForm.get(['id'])!.value,
      pocetKS: this.editForm.get(['pocetKS'])!.value,
      qrCode: this.editForm.get(['qrCode'])!.value,
      zbozi: this.editForm.get(['zbozi'])!.value,
      typKS: this.editForm.get(['typKS'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBaleni>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
