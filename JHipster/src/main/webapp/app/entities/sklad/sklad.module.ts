import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AugmentedWarehouseSharedModule } from 'app/shared/shared.module';
import { SkladComponent } from './sklad.component';
import { SkladDetailComponent } from './sklad-detail.component';
import { SkladUpdateComponent } from './sklad-update.component';
import { SkladDeleteDialogComponent } from './sklad-delete-dialog.component';
import { skladRoute } from './sklad.route';

@NgModule({
  imports: [AugmentedWarehouseSharedModule, RouterModule.forChild(skladRoute)],
  declarations: [SkladComponent, SkladDetailComponent, SkladUpdateComponent, SkladDeleteDialogComponent],
  entryComponents: [SkladDeleteDialogComponent]
})
export class AugmentedWarehouseSkladModule {}
