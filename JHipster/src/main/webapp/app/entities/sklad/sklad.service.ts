import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ISklad } from 'app/shared/model/sklad.model';

type EntityResponseType = HttpResponse<ISklad>;
type EntityArrayResponseType = HttpResponse<ISklad[]>;

@Injectable({ providedIn: 'root' })
export class SkladService {
  public resourceUrl = SERVER_API_URL + 'api/sklads';

  constructor(protected http: HttpClient) {}

  create(sklad: ISklad): Observable<EntityResponseType> {
    return this.http.post<ISklad>(this.resourceUrl, sklad, { observe: 'response' });
  }

  update(sklad: ISklad): Observable<EntityResponseType> {
    return this.http.put<ISklad>(this.resourceUrl, sklad, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISklad>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISklad[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
