import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ISklad, Sklad } from 'app/shared/model/sklad.model';
import { SkladService } from './sklad.service';

@Component({
  selector: 'jhi-sklad-update',
  templateUrl: './sklad-update.component.html'
})
export class SkladUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    adresa: [],
    cisloPopisne: [],
    cisloOrientacni: [],
    mesto: [],
    pSC: [null, [Validators.min(10000), Validators.max(99999)]]
  });

  constructor(protected skladService: SkladService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ sklad }) => {
      this.updateForm(sklad);
    });
  }

  updateForm(sklad: ISklad): void {
    this.editForm.patchValue({
      id: sklad.id,
      adresa: sklad.adresa,
      cisloPopisne: sklad.cisloPopisne,
      cisloOrientacni: sklad.cisloOrientacni,
      mesto: sklad.mesto,
      pSC: sklad.pSC
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const sklad = this.createFromForm();
    if (sklad.id !== undefined) {
      this.subscribeToSaveResponse(this.skladService.update(sklad));
    } else {
      this.subscribeToSaveResponse(this.skladService.create(sklad));
    }
  }

  private createFromForm(): ISklad {
    return {
      ...new Sklad(),
      id: this.editForm.get(['id'])!.value,
      adresa: this.editForm.get(['adresa'])!.value,
      cisloPopisne: this.editForm.get(['cisloPopisne'])!.value,
      cisloOrientacni: this.editForm.get(['cisloOrientacni'])!.value,
      mesto: this.editForm.get(['mesto'])!.value,
      pSC: this.editForm.get(['pSC'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISklad>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
