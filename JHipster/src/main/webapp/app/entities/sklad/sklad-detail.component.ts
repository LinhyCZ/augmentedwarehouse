import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISklad } from 'app/shared/model/sklad.model';

@Component({
  selector: 'jhi-sklad-detail',
  templateUrl: './sklad-detail.component.html'
})
export class SkladDetailComponent implements OnInit {
  sklad: ISklad | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ sklad }) => (this.sklad = sklad));
  }

  previousState(): void {
    window.history.back();
  }
}
