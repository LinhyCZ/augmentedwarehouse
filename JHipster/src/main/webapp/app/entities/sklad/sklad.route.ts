import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ISklad, Sklad } from 'app/shared/model/sklad.model';
import { SkladService } from './sklad.service';
import { SkladComponent } from './sklad.component';
import { SkladDetailComponent } from './sklad-detail.component';
import { SkladUpdateComponent } from './sklad-update.component';

@Injectable({ providedIn: 'root' })
export class SkladResolve implements Resolve<ISklad> {
  constructor(private service: SkladService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISklad> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((sklad: HttpResponse<Sklad>) => {
          if (sklad.body) {
            return of(sklad.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Sklad());
  }
}

export const skladRoute: Routes = [
  {
    path: '',
    component: SkladComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.sklad.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SkladDetailComponent,
    resolve: {
      sklad: SkladResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.sklad.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SkladUpdateComponent,
    resolve: {
      sklad: SkladResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.sklad.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SkladUpdateComponent,
    resolve: {
      sklad: SkladResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'augmentedWarehouseApp.sklad.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
