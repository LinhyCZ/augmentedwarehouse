import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISklad } from 'app/shared/model/sklad.model';
import { SkladService } from './sklad.service';

@Component({
  templateUrl: './sklad-delete-dialog.component.html'
})
export class SkladDeleteDialogComponent {
  sklad?: ISklad;

  constructor(protected skladService: SkladService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.skladService.delete(id).subscribe(() => {
      this.eventManager.broadcast('skladListModification');
      this.activeModal.close();
    });
  }
}
