import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ISklad } from 'app/shared/model/sklad.model';
import { SkladService } from './sklad.service';
import { SkladDeleteDialogComponent } from './sklad-delete-dialog.component';

@Component({
  selector: 'jhi-sklad',
  templateUrl: './sklad.component.html'
})
export class SkladComponent implements OnInit, OnDestroy {
  sklads?: ISklad[];
  eventSubscriber?: Subscription;

  constructor(protected skladService: SkladService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.skladService.query().subscribe((res: HttpResponse<ISklad[]>) => (this.sklads = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInSklads();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ISklad): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInSklads(): void {
    this.eventSubscriber = this.eventManager.subscribe('skladListModification', () => this.loadAll());
  }

  delete(sklad: ISklad): void {
    const modalRef = this.modalService.open(SkladDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.sklad = sklad;
  }
}
