import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { AugmentedWarehouseSharedModule } from 'app/shared/shared.module';
import { AugmentedWarehouseCoreModule } from 'app/core/core.module';
import { AugmentedWarehouseAppRoutingModule } from './app-routing.module';
import { AugmentedWarehouseHomeModule } from './home/home.module';
import { AugmentedWarehouseEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    AugmentedWarehouseSharedModule,
    AugmentedWarehouseCoreModule,
    AugmentedWarehouseHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    AugmentedWarehouseEntityModule,
    AugmentedWarehouseAppRoutingModule
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent]
})
export class AugmentedWarehouseAppModule {}
