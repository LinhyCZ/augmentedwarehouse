package cz.zcu.kiv.augmentedwarehouse.service;

import cz.zcu.kiv.augmentedwarehouse.service.dto.SloupecDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link cz.zcu.kiv.augmentedwarehouse.domain.Sloupec}.
 */
public interface SloupecService {

    /**
     * Save a sloupec.
     *
     * @param sloupecDTO the entity to save.
     * @return the persisted entity.
     */
    SloupecDTO save(SloupecDTO sloupecDTO);

    /**
     * Get all the sloupecs.
     *
     * @return the list of entities.
     */
    List<SloupecDTO> findAll();

    /**
     * Get the "id" sloupec.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SloupecDTO> findOne(Long id);

    /**
     * Delete the "id" sloupec.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
