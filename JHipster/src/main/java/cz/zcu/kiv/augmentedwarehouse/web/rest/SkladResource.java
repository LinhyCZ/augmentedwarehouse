package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.domain.Sklad;
import cz.zcu.kiv.augmentedwarehouse.repository.SkladRepository;
import cz.zcu.kiv.augmentedwarehouse.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link cz.zcu.kiv.augmentedwarehouse.domain.Sklad}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class SkladResource {

    private final Logger log = LoggerFactory.getLogger(SkladResource.class);

    private static final String ENTITY_NAME = "sklad";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SkladRepository skladRepository;

    public SkladResource(SkladRepository skladRepository) {
        this.skladRepository = skladRepository;
    }

    /**
     * {@code POST  /sklads} : Create a new sklad.
     *
     * @param sklad the sklad to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new sklad, or with status {@code 400 (Bad Request)} if the sklad has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/sklads")
    public ResponseEntity<Sklad> createSklad(@Valid @RequestBody Sklad sklad) throws URISyntaxException {
        log.debug("REST request to save Sklad : {}", sklad);
        if (sklad.getId() != null) {
            throw new BadRequestAlertException("A new sklad cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Sklad result = skladRepository.save(sklad);
        return ResponseEntity.created(new URI("/api/sklads/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /sklads} : Updates an existing sklad.
     *
     * @param sklad the sklad to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sklad,
     * or with status {@code 400 (Bad Request)} if the sklad is not valid,
     * or with status {@code 500 (Internal Server Error)} if the sklad couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/sklads")
    public ResponseEntity<Sklad> updateSklad(@Valid @RequestBody Sklad sklad) throws URISyntaxException {
        log.debug("REST request to update Sklad : {}", sklad);
        if (sklad.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Sklad result = skladRepository.save(sklad);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, sklad.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /sklads} : get all the sklads.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of sklads in body.
     */
    @GetMapping("/sklads")
    public List<Sklad> getAllSklads() {
        log.debug("REST request to get all Sklads");
        return skladRepository.findAll();
    }

    /**
     * {@code GET  /sklads/:id} : get the "id" sklad.
     *
     * @param id the id of the sklad to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the sklad, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/sklads/{id}")
    public ResponseEntity<Sklad> getSklad(@PathVariable Long id) {
        log.debug("REST request to get Sklad : {}", id);
        Optional<Sklad> sklad = skladRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(sklad);
    }

    /**
     * {@code DELETE  /sklads/:id} : delete the "id" sklad.
     *
     * @param id the id of the sklad to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/sklads/{id}")
    public ResponseEntity<Void> deleteSklad(@PathVariable Long id) {
        log.debug("REST request to delete Sklad : {}", id);
        skladRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
