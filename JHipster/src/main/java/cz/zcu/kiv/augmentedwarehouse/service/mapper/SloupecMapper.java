package cz.zcu.kiv.augmentedwarehouse.service.mapper;


import cz.zcu.kiv.augmentedwarehouse.domain.*;
import cz.zcu.kiv.augmentedwarehouse.service.dto.SloupecDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Sloupec} and its DTO {@link SloupecDTO}.
 */
@Mapper(componentModel = "spring", uses = {PoliceMapper.class})
public interface SloupecMapper extends EntityMapper<SloupecDTO, Sloupec> {

    @Mapping(source = "police.id", target = "policeId")
    SloupecDTO toDto(Sloupec sloupec);

    @Mapping(target = "pozices", ignore = true)
    @Mapping(target = "removePozice", ignore = true)
    @Mapping(source = "policeId", target = "police")
    Sloupec toEntity(SloupecDTO sloupecDTO);

    default Sloupec fromId(Long id) {
        if (id == null) {
            return null;
        }
        Sloupec sloupec = new Sloupec();
        sloupec.setId(id);
        return sloupec;
    }
}
