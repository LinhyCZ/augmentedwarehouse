package cz.zcu.kiv.augmentedwarehouse.service.mapper;


import cz.zcu.kiv.augmentedwarehouse.domain.*;
import cz.zcu.kiv.augmentedwarehouse.service.dto.TypKSDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TypKS} and its DTO {@link TypKSDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TypKSMapper extends EntityMapper<TypKSDTO, TypKS> {


    @Mapping(target = "balenis", ignore = true)
    @Mapping(target = "removeBaleni", ignore = true)
    TypKS toEntity(TypKSDTO typKSDTO);

    default TypKS fromId(Long id) {
        if (id == null) {
            return null;
        }
        TypKS typKS = new TypKS();
        typKS.setId(id);
        return typKS;
    }
}
