package cz.zcu.kiv.augmentedwarehouse.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A Baleni.
 */
@Entity
@Table(name = "baleni")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Baleni implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "pocet_ks")
    private Integer pocetKS;

    @Column(name = "qr_code")
    private String qrCode;

    @ManyToOne
    @JsonIgnoreProperties("balenis")
    private Zbozi zbozi;

    @ManyToOne
    @JsonIgnoreProperties("balenis")
    private TypKS typKS;

    @ManyToMany(mappedBy = "balenis")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<Pozice> pozices = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPocetKS() {
        return pocetKS;
    }

    public Baleni pocetKS(Integer pocetKS) {
        this.pocetKS = pocetKS;
        return this;
    }

    public void setPocetKS(Integer pocetKS) {
        this.pocetKS = pocetKS;
    }

    public String getQrCode() {
        return qrCode;
    }

    public Baleni qrCode(String qrCode) {
        this.qrCode = qrCode;
        return this;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public Zbozi getZbozi() {
        return zbozi;
    }

    public Baleni zbozi(Zbozi zbozi) {
        this.zbozi = zbozi;
        return this;
    }

    public void setZbozi(Zbozi zbozi) {
        this.zbozi = zbozi;
    }

    public TypKS getTypKS() {
        return typKS;
    }

    public Baleni typKS(TypKS typKS) {
        this.typKS = typKS;
        return this;
    }

    public void setTypKS(TypKS typKS) {
        this.typKS = typKS;
    }

    public Set<Pozice> getPozices() {
        return pozices;
    }

    public Baleni pozices(Set<Pozice> pozices) {
        this.pozices = pozices;
        return this;
    }

    public Baleni addPozice(Pozice pozice) {
        this.pozices.add(pozice);
        pozice.getBalenis().add(this);
        return this;
    }

    public Baleni removePozice(Pozice pozice) {
        this.pozices.remove(pozice);
        pozice.getBalenis().remove(this);
        return this;
    }

    public void setPozices(Set<Pozice> pozices) {
        this.pozices = pozices;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Baleni)) {
            return false;
        }
        return id != null && id.equals(((Baleni) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Baleni{" +
            "id=" + getId() +
            ", pocetKS=" + getPocetKS() +
            ", qrCode='" + getQrCode() + "'" +
            "}";
    }
}
