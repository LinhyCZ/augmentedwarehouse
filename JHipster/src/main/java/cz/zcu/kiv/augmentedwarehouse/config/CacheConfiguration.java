package cz.zcu.kiv.augmentedwarehouse.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import org.hibernate.cache.jcache.ConfigSettings;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache = jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.domain.User.class.getName());
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.domain.Authority.class.getName());
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.domain.User.class.getName() + ".authorities");
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.domain.Sklad.class.getName());
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.domain.Sklad.class.getName() + ".stojans");
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.domain.Stojan.class.getName());
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.domain.Stojan.class.getName() + ".police");
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.domain.Police.class.getName());
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.domain.Police.class.getName() + ".sloupecs");
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.domain.Sloupec.class.getName());
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.domain.Sloupec.class.getName() + ".pozices");
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.domain.Pozice.class.getName());
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.domain.Pozice.class.getName() + ".balenis");
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.domain.Baleni.class.getName());
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.domain.Baleni.class.getName() + ".pozices");
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.domain.TypKS.class.getName());
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.domain.TypKS.class.getName() + ".balenis");
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.domain.Zbozi.class.getName());
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.domain.Zbozi.class.getName() + ".balenis");
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.domain.Jednotky.class.getName());
            createCache(cm, cz.zcu.kiv.augmentedwarehouse.domain.Jednotky.class.getName() + ".typKS");
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache == null) {
            cm.createCache(cacheName, jcacheConfiguration);
        }
    }

}
