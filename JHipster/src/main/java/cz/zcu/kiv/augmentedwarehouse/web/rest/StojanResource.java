package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.domain.Stojan;
import cz.zcu.kiv.augmentedwarehouse.repository.StojanRepository;
import cz.zcu.kiv.augmentedwarehouse.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link cz.zcu.kiv.augmentedwarehouse.domain.Stojan}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class StojanResource {

    private final Logger log = LoggerFactory.getLogger(StojanResource.class);

    private static final String ENTITY_NAME = "stojan";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final StojanRepository stojanRepository;

    public StojanResource(StojanRepository stojanRepository) {
        this.stojanRepository = stojanRepository;
    }

    /**
     * {@code POST  /stojans} : Create a new stojan.
     *
     * @param stojan the stojan to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new stojan, or with status {@code 400 (Bad Request)} if the stojan has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/stojans")
    public ResponseEntity<Stojan> createStojan(@RequestBody Stojan stojan) throws URISyntaxException {
        log.debug("REST request to save Stojan : {}", stojan);
        if (stojan.getId() != null) {
            throw new BadRequestAlertException("A new stojan cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Stojan result = stojanRepository.save(stojan);
        return ResponseEntity.created(new URI("/api/stojans/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /stojans} : Updates an existing stojan.
     *
     * @param stojan the stojan to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated stojan,
     * or with status {@code 400 (Bad Request)} if the stojan is not valid,
     * or with status {@code 500 (Internal Server Error)} if the stojan couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/stojans")
    public ResponseEntity<Stojan> updateStojan(@RequestBody Stojan stojan) throws URISyntaxException {
        log.debug("REST request to update Stojan : {}", stojan);
        if (stojan.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Stojan result = stojanRepository.save(stojan);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, stojan.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /stojans} : get all the stojans.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of stojans in body.
     */
    @GetMapping("/stojans")
    public List<Stojan> getAllStojans() {
        log.debug("REST request to get all Stojans");
        return stojanRepository.findAll();
    }

    /**
     * {@code GET  /stojans/:id} : get the "id" stojan.
     *
     * @param id the id of the stojan to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the stojan, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/stojans/{id}")
    public ResponseEntity<Stojan> getStojan(@PathVariable Long id) {
        log.debug("REST request to get Stojan : {}", id);
        Optional<Stojan> stojan = stojanRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(stojan);
    }

    /**
     * {@code DELETE  /stojans/:id} : delete the "id" stojan.
     *
     * @param id the id of the stojan to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/stojans/{id}")
    public ResponseEntity<Void> deleteStojan(@PathVariable Long id) {
        log.debug("REST request to delete Stojan : {}", id);
        stojanRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
