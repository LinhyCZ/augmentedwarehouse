package cz.zcu.kiv.augmentedwarehouse.service;

import cz.zcu.kiv.augmentedwarehouse.repository.CustomWarehousesProjection;

import java.util.List;

public interface CustomWarehouseService {
    public List<CustomWarehousesProjection> getAllWarehouses();
    public List<CustomWarehousesProjection> getAllWarehousesPageable(Integer pageNo, Integer pageSize, String sortBy);
}
