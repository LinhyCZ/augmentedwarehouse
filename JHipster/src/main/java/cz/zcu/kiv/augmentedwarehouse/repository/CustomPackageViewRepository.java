package cz.zcu.kiv.augmentedwarehouse.repository;

import cz.zcu.kiv.augmentedwarehouse.domain.Baleni;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomPackageViewRepository extends JpaRepository<Baleni, Long> {
    @Query(value = "select * from baleni where id=:baleniId", nativeQuery = true)
    CustomPackageViewProjection findPackageByData(@Param("baleniId") int baleniId);
}
