package cz.zcu.kiv.augmentedwarehouse.service;

import cz.zcu.kiv.augmentedwarehouse.service.dto.PoliceDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link cz.zcu.kiv.augmentedwarehouse.domain.Police}.
 */
public interface PoliceService {

    /**
     * Save a police.
     *
     * @param policeDTO the entity to save.
     * @return the persisted entity.
     */
    PoliceDTO save(PoliceDTO policeDTO);

    /**
     * Get all the police.
     *
     * @return the list of entities.
     */
    List<PoliceDTO> findAll();

    /**
     * Get the "id" police.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PoliceDTO> findOne(Long id);

    /**
     * Delete the "id" police.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
