package cz.zcu.kiv.augmentedwarehouse.service;

import cz.zcu.kiv.augmentedwarehouse.service.dto.StojanDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link cz.zcu.kiv.augmentedwarehouse.domain.Stojan}.
 */
public interface StojanService {

    /**
     * Save a stojan.
     *
     * @param stojanDTO the entity to save.
     * @return the persisted entity.
     */
    StojanDTO save(StojanDTO stojanDTO);

    /**
     * Get all the stojans.
     *
     * @return the list of entities.
     */
    List<StojanDTO> findAll();

    /**
     * Get the "id" stojan.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<StojanDTO> findOne(Long id);

    /**
     * Delete the "id" stojan.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
