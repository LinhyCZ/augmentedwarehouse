package cz.zcu.kiv.augmentedwarehouse.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A Sklad.
 */
@Entity
@Table(name = "sklad")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Sklad implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "adresa")
    private String adresa;

    @Column(name = "cislo_popisne")
    private Integer cisloPopisne;

    @Column(name = "cislo_orientacni")
    private Integer cisloOrientacni;

    @Column(name = "mesto")
    private String mesto;

    @Min(value = 10000)
    @Max(value = 99999)
    @Column(name = "p_sc")
    private Integer pSC;

    @OneToMany(mappedBy = "sklad")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Stojan> stojans = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAdresa() {
        return adresa;
    }

    public Sklad adresa(String adresa) {
        this.adresa = adresa;
        return this;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public Integer getCisloPopisne() {
        return cisloPopisne;
    }

    public Sklad cisloPopisne(Integer cisloPopisne) {
        this.cisloPopisne = cisloPopisne;
        return this;
    }

    public void setCisloPopisne(Integer cisloPopisne) {
        this.cisloPopisne = cisloPopisne;
    }

    public Integer getCisloOrientacni() {
        return cisloOrientacni;
    }

    public Sklad cisloOrientacni(Integer cisloOrientacni) {
        this.cisloOrientacni = cisloOrientacni;
        return this;
    }

    public void setCisloOrientacni(Integer cisloOrientacni) {
        this.cisloOrientacni = cisloOrientacni;
    }

    public String getMesto() {
        return mesto;
    }

    public Sklad mesto(String mesto) {
        this.mesto = mesto;
        return this;
    }

    public void setMesto(String mesto) {
        this.mesto = mesto;
    }

    public Integer getpSC() {
        return pSC;
    }

    public Sklad pSC(Integer pSC) {
        this.pSC = pSC;
        return this;
    }

    public void setpSC(Integer pSC) {
        this.pSC = pSC;
    }

    public Set<Stojan> getStojans() {
        return stojans;
    }

    public Sklad stojans(Set<Stojan> stojans) {
        this.stojans = stojans;
        return this;
    }

    public Sklad addStojan(Stojan stojan) {
        this.stojans.add(stojan);
        stojan.setSklad(this);
        return this;
    }

    public Sklad removeStojan(Stojan stojan) {
        this.stojans.remove(stojan);
        stojan.setSklad(null);
        return this;
    }

    public void setStojans(Set<Stojan> stojans) {
        this.stojans = stojans;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Sklad)) {
            return false;
        }
        return id != null && id.equals(((Sklad) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Sklad{" +
            "id=" + getId() +
            ", adresa='" + getAdresa() + "'" +
            ", cisloPopisne=" + getCisloPopisne() +
            ", cisloOrientacni=" + getCisloOrientacni() +
            ", mesto='" + getMesto() + "'" +
            ", pSC=" + getpSC() +
            "}";
    }
}
