/**
 * View Models used by Spring MVC REST controllers.
 */
package cz.zcu.kiv.augmentedwarehouse.web.rest.vm;
