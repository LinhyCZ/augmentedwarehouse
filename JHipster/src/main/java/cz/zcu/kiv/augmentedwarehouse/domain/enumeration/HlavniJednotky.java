package cz.zcu.kiv.augmentedwarehouse.domain.enumeration;

/**
 * The HlavniJednotky enumeration.
 */
public enum HlavniJednotky {
    Ks, Mm, L, Kg
}
