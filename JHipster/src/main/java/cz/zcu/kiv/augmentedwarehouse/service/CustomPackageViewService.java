package cz.zcu.kiv.augmentedwarehouse.service;

import cz.zcu.kiv.augmentedwarehouse.repository.CustomPackageViewProjection;

import java.util.List;

public interface CustomPackageViewService {
    public CustomPackageViewProjection findItemViewProjection(int packageId);
}
