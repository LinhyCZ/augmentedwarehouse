package cz.zcu.kiv.augmentedwarehouse.service;

import cz.zcu.kiv.augmentedwarehouse.service.dto.PoziceDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link cz.zcu.kiv.augmentedwarehouse.domain.Pozice}.
 */
public interface PoziceService {

    /**
     * Save a pozice.
     *
     * @param poziceDTO the entity to save.
     * @return the persisted entity.
     */
    PoziceDTO save(PoziceDTO poziceDTO);

    /**
     * Get all the pozices.
     *
     * @return the list of entities.
     */
    List<PoziceDTO> findAll();

    /**
     * Get all the pozices with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<PoziceDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" pozice.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PoziceDTO> findOne(Long id);

    /**
     * Delete the "id" pozice.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
