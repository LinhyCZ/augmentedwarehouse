package cz.zcu.kiv.augmentedwarehouse.repository;

public interface CustomPackageViewProjection {
    String getPocet_ks();
    String getQr_code();
    String getZbozi_id();
    String getTypks_id();
}
