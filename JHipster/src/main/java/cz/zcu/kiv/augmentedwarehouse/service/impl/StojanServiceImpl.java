package cz.zcu.kiv.augmentedwarehouse.service.impl;

import cz.zcu.kiv.augmentedwarehouse.service.StojanService;
import cz.zcu.kiv.augmentedwarehouse.domain.Stojan;
import cz.zcu.kiv.augmentedwarehouse.repository.StojanRepository;
import cz.zcu.kiv.augmentedwarehouse.service.dto.StojanDTO;
import cz.zcu.kiv.augmentedwarehouse.service.mapper.StojanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Stojan}.
 */
@Service
@Transactional
public class StojanServiceImpl implements StojanService {

    private final Logger log = LoggerFactory.getLogger(StojanServiceImpl.class);

    private final StojanRepository stojanRepository;

    private final StojanMapper stojanMapper;

    public StojanServiceImpl(StojanRepository stojanRepository, StojanMapper stojanMapper) {
        this.stojanRepository = stojanRepository;
        this.stojanMapper = stojanMapper;
    }

    /**
     * Save a stojan.
     *
     * @param stojanDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public StojanDTO save(StojanDTO stojanDTO) {
        log.debug("Request to save Stojan : {}", stojanDTO);
        Stojan stojan = stojanMapper.toEntity(stojanDTO);
        stojan = stojanRepository.save(stojan);
        return stojanMapper.toDto(stojan);
    }

    /**
     * Get all the stojans.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<StojanDTO> findAll() {
        log.debug("Request to get all Stojans");
        return stojanRepository.findAll().stream()
            .map(stojanMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one stojan by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<StojanDTO> findOne(Long id) {
        log.debug("Request to get Stojan : {}", id);
        return stojanRepository.findById(id)
            .map(stojanMapper::toDto);
    }

    /**
     * Delete the stojan by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Stojan : {}", id);
        stojanRepository.deleteById(id);
    }
}
