package cz.zcu.kiv.augmentedwarehouse.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A Sloupec.
 */
@Entity
@Table(name = "sloupec")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Sloupec implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "sloupec")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Pozice> pozices = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("sloupecs")
    private Police police;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Pozice> getPozices() {
        return pozices;
    }

    public Sloupec pozices(Set<Pozice> pozices) {
        this.pozices = pozices;
        return this;
    }

    public Sloupec addPozice(Pozice pozice) {
        this.pozices.add(pozice);
        pozice.setSloupec(this);
        return this;
    }

    public Sloupec removePozice(Pozice pozice) {
        this.pozices.remove(pozice);
        pozice.setSloupec(null);
        return this;
    }

    public void setPozices(Set<Pozice> pozices) {
        this.pozices = pozices;
    }

    public Police getPolice() {
        return police;
    }

    public Sloupec police(Police police) {
        this.police = police;
        return this;
    }

    public void setPolice(Police police) {
        this.police = police;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Sloupec)) {
            return false;
        }
        return id != null && id.equals(((Sloupec) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Sloupec{" +
            "id=" + getId() +
            "}";
    }
}
