package cz.zcu.kiv.augmentedwarehouse.repository;

import cz.zcu.kiv.augmentedwarehouse.domain.Sklad;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Sklad entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SkladRepository extends JpaRepository<Sklad, Long> {
}
