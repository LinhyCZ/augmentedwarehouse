/**
 * Spring MVC REST controllers.
 */
package cz.zcu.kiv.augmentedwarehouse.web.rest;
