package cz.zcu.kiv.augmentedwarehouse.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

import cz.zcu.kiv.augmentedwarehouse.domain.enumeration.HlavniJednotky;

/**
 * A Jednotky.
 */
@Entity
@Table(name = "jednotky")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Jednotky implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "hlavni_jednotka")
    private HlavniJednotky hlavniJednotka;

    @Column(name = "ratio_do_hlavni_jednotky")
    private Double ratioDoHlavniJednotky;

    @Column(name = "nazev_jednotky")
    private String nazevJednotky;

    @OneToMany(mappedBy = "jednotky")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<TypKS> typKS = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public HlavniJednotky getHlavniJednotka() {
        return hlavniJednotka;
    }

    public Jednotky hlavniJednotka(HlavniJednotky hlavniJednotka) {
        this.hlavniJednotka = hlavniJednotka;
        return this;
    }

    public void setHlavniJednotka(HlavniJednotky hlavniJednotka) {
        this.hlavniJednotka = hlavniJednotka;
    }

    public Double getRatioDoHlavniJednotky() {
        return ratioDoHlavniJednotky;
    }

    public Jednotky ratioDoHlavniJednotky(Double ratioDoHlavniJednotky) {
        this.ratioDoHlavniJednotky = ratioDoHlavniJednotky;
        return this;
    }

    public void setRatioDoHlavniJednotky(Double ratioDoHlavniJednotky) {
        this.ratioDoHlavniJednotky = ratioDoHlavniJednotky;
    }

    public String getNazevJednotky() {
        return nazevJednotky;
    }

    public Jednotky nazevJednotky(String nazevJednotky) {
        this.nazevJednotky = nazevJednotky;
        return this;
    }

    public void setNazevJednotky(String nazevJednotky) {
        this.nazevJednotky = nazevJednotky;
    }

    public Set<TypKS> getTypKS() {
        return typKS;
    }

    public Jednotky typKS(Set<TypKS> typKS) {
        this.typKS = typKS;
        return this;
    }

    public Jednotky addTypKS(TypKS typKS) {
        this.typKS.add(typKS);
        typKS.setJednotky(this);
        return this;
    }

    public Jednotky removeTypKS(TypKS typKS) {
        this.typKS.remove(typKS);
        typKS.setJednotky(null);
        return this;
    }

    public void setTypKS(Set<TypKS> typKS) {
        this.typKS = typKS;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Jednotky)) {
            return false;
        }
        return id != null && id.equals(((Jednotky) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Jednotky{" +
            "id=" + getId() +
            ", hlavniJednotka='" + getHlavniJednotka() + "'" +
            ", ratioDoHlavniJednotky=" + getRatioDoHlavniJednotky() +
            ", nazevJednotky='" + getNazevJednotky() + "'" +
            "}";
    }
}
