package cz.zcu.kiv.augmentedwarehouse.repository;

import cz.zcu.kiv.augmentedwarehouse.domain.Zbozi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomItemImageRepository extends JpaRepository<Zbozi, Long> {
    @Query(value = "SELECT "
        + "    zbozi.obrazek"
        + " FROM zbozi"
        + " WHERE zbozi.id = :zboziId", nativeQuery = true)
    CustomItemImageProjection getImageData(@Param("zboziId") int zboziId);
}
