package cz.zcu.kiv.augmentedwarehouse.service.impl;

import cz.zcu.kiv.augmentedwarehouse.service.PoziceService;
import cz.zcu.kiv.augmentedwarehouse.domain.Pozice;
import cz.zcu.kiv.augmentedwarehouse.repository.PoziceRepository;
import cz.zcu.kiv.augmentedwarehouse.service.dto.PoziceDTO;
import cz.zcu.kiv.augmentedwarehouse.service.mapper.PoziceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Pozice}.
 */
@Service
@Transactional
public class PoziceServiceImpl implements PoziceService {

    private final Logger log = LoggerFactory.getLogger(PoziceServiceImpl.class);

    private final PoziceRepository poziceRepository;

    private final PoziceMapper poziceMapper;

    public PoziceServiceImpl(PoziceRepository poziceRepository, PoziceMapper poziceMapper) {
        this.poziceRepository = poziceRepository;
        this.poziceMapper = poziceMapper;
    }

    /**
     * Save a pozice.
     *
     * @param poziceDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PoziceDTO save(PoziceDTO poziceDTO) {
        log.debug("Request to save Pozice : {}", poziceDTO);
        Pozice pozice = poziceMapper.toEntity(poziceDTO);
        pozice = poziceRepository.save(pozice);
        return poziceMapper.toDto(pozice);
    }

    /**
     * Get all the pozices.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<PoziceDTO> findAll() {
        log.debug("Request to get all Pozices");
        return poziceRepository.findAllWithEagerRelationships().stream()
            .map(poziceMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get all the pozices with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<PoziceDTO> findAllWithEagerRelationships(Pageable pageable) {
        return poziceRepository.findAllWithEagerRelationships(pageable).map(poziceMapper::toDto);
    }

    /**
     * Get one pozice by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PoziceDTO> findOne(Long id) {
        log.debug("Request to get Pozice : {}", id);
        return poziceRepository.findOneWithEagerRelationships(id)
            .map(poziceMapper::toDto);
    }

    /**
     * Delete the pozice by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Pozice : {}", id);
        poziceRepository.deleteById(id);
    }
}
