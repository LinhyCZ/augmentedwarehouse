package cz.zcu.kiv.augmentedwarehouse.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A Zbozi.
 */
@Entity
@Table(name = "zbozi")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Zbozi implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "kategorie")
    private String kategorie;

    @Column(name = "slozeni")
    private String slozeni;

    @Column(name = "popis_zbozi")
    private String popisZbozi;

    @Column(name = "ean")
    private String ean;

    @Lob
    @Column(name = "obrazek")
    private String obrazek;

    @OneToMany(mappedBy = "zbozi")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Baleni> balenis = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKategorie() {
        return kategorie;
    }

    public Zbozi kategorie(String kategorie) {
        this.kategorie = kategorie;
        return this;
    }

    public void setKategorie(String kategorie) {
        this.kategorie = kategorie;
    }

    public String getSlozeni() {
        return slozeni;
    }

    public Zbozi slozeni(String slozeni) {
        this.slozeni = slozeni;
        return this;
    }

    public void setSlozeni(String slozeni) {
        this.slozeni = slozeni;
    }

    public String getPopisZbozi() {
        return popisZbozi;
    }

    public Zbozi popisZbozi(String popisZbozi) {
        this.popisZbozi = popisZbozi;
        return this;
    }

    public void setPopisZbozi(String popisZbozi) {
        this.popisZbozi = popisZbozi;
    }

    public String getEan() {
        return ean;
    }

    public Zbozi ean(String ean) {
        this.ean = ean;
        return this;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public String getObrazek() {
        return obrazek;
    }

    public Zbozi obrazek(String obrazek) {
        this.obrazek = obrazek;
        return this;
    }

    public void setObrazek(String obrazek) {
        this.obrazek = obrazek;
    }

    public Set<Baleni> getBalenis() {
        return balenis;
    }

    public Zbozi balenis(Set<Baleni> balenis) {
        this.balenis = balenis;
        return this;
    }

    public Zbozi addBaleni(Baleni baleni) {
        this.balenis.add(baleni);
        baleni.setZbozi(this);
        return this;
    }

    public Zbozi removeBaleni(Baleni baleni) {
        this.balenis.remove(baleni);
        baleni.setZbozi(null);
        return this;
    }

    public void setBalenis(Set<Baleni> balenis) {
        this.balenis = balenis;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Zbozi)) {
            return false;
        }
        return id != null && id.equals(((Zbozi) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Zbozi{" +
            "id=" + getId() +
            ", kategorie='" + getKategorie() + "'" +
            ", slozeni='" + getSlozeni() + "'" +
            ", popisZbozi='" + getPopisZbozi() + "'" +
            ", ean='" + getEan() + "'" +
            ", obrazek='" + getObrazek() + "'" +
            "}";
    }
}
