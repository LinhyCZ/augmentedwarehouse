package cz.zcu.kiv.augmentedwarehouse.repository;

public interface CustomWarehousesProjection {
    String getId();
    String getCislo_popisne();
    String getCislo_orientacni();
    String getMesto();
    String getAdresa();
    String getP_sc();
}
