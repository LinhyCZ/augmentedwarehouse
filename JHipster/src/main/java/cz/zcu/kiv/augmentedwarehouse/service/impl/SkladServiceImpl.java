package cz.zcu.kiv.augmentedwarehouse.service.impl;

import cz.zcu.kiv.augmentedwarehouse.service.SkladService;
import cz.zcu.kiv.augmentedwarehouse.domain.Sklad;
import cz.zcu.kiv.augmentedwarehouse.repository.SkladRepository;
import cz.zcu.kiv.augmentedwarehouse.service.dto.SkladDTO;
import cz.zcu.kiv.augmentedwarehouse.service.mapper.SkladMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Sklad}.
 */
@Service
@Transactional
public class SkladServiceImpl implements SkladService {

    private final Logger log = LoggerFactory.getLogger(SkladServiceImpl.class);

    private final SkladRepository skladRepository;

    private final SkladMapper skladMapper;

    public SkladServiceImpl(SkladRepository skladRepository, SkladMapper skladMapper) {
        this.skladRepository = skladRepository;
        this.skladMapper = skladMapper;
    }

    /**
     * Save a sklad.
     *
     * @param skladDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public SkladDTO save(SkladDTO skladDTO) {
        log.debug("Request to save Sklad : {}", skladDTO);
        Sklad sklad = skladMapper.toEntity(skladDTO);
        sklad = skladRepository.save(sklad);
        return skladMapper.toDto(sklad);
    }

    /**
     * Get all the sklads.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<SkladDTO> findAll() {
        log.debug("Request to get all Sklads");
        return skladRepository.findAll().stream()
            .map(skladMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one sklad by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SkladDTO> findOne(Long id) {
        log.debug("Request to get Sklad : {}", id);
        return skladRepository.findById(id)
            .map(skladMapper::toDto);
    }

    /**
     * Delete the sklad by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Sklad : {}", id);
        skladRepository.deleteById(id);
    }
}
