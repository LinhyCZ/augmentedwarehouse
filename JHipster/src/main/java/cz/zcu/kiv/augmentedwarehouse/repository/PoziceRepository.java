package cz.zcu.kiv.augmentedwarehouse.repository;

import cz.zcu.kiv.augmentedwarehouse.domain.Pozice;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Pozice entity.
 */
@Repository
public interface PoziceRepository extends JpaRepository<Pozice, Long> {

    @Query(value = "select distinct pozice from Pozice pozice left join fetch pozice.balenis",
        countQuery = "select count(distinct pozice) from Pozice pozice")
    Page<Pozice> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct pozice from Pozice pozice left join fetch pozice.balenis")
    List<Pozice> findAllWithEagerRelationships();

    @Query("select pozice from Pozice pozice left join fetch pozice.balenis where pozice.id =:id")
    Optional<Pozice> findOneWithEagerRelationships(@Param("id") Long id);
}
