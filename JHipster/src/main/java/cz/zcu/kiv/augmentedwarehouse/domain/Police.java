package cz.zcu.kiv.augmentedwarehouse.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A Police.
 */
@Entity
@Table(name = "police")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Police implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "police")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Sloupec> sloupecs = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("police")
    private Stojan stojan;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Sloupec> getSloupecs() {
        return sloupecs;
    }

    public Police sloupecs(Set<Sloupec> sloupecs) {
        this.sloupecs = sloupecs;
        return this;
    }

    public Police addSloupec(Sloupec sloupec) {
        this.sloupecs.add(sloupec);
        sloupec.setPolice(this);
        return this;
    }

    public Police removeSloupec(Sloupec sloupec) {
        this.sloupecs.remove(sloupec);
        sloupec.setPolice(null);
        return this;
    }

    public void setSloupecs(Set<Sloupec> sloupecs) {
        this.sloupecs = sloupecs;
    }

    public Stojan getStojan() {
        return stojan;
    }

    public Police stojan(Stojan stojan) {
        this.stojan = stojan;
        return this;
    }

    public void setStojan(Stojan stojan) {
        this.stojan = stojan;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Police)) {
            return false;
        }
        return id != null && id.equals(((Police) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Police{" +
            "id=" + getId() +
            "}";
    }
}
