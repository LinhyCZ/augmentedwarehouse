package cz.zcu.kiv.augmentedwarehouse.service.impl;

import cz.zcu.kiv.augmentedwarehouse.service.PoliceService;
import cz.zcu.kiv.augmentedwarehouse.domain.Police;
import cz.zcu.kiv.augmentedwarehouse.repository.PoliceRepository;
import cz.zcu.kiv.augmentedwarehouse.service.dto.PoliceDTO;
import cz.zcu.kiv.augmentedwarehouse.service.mapper.PoliceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Police}.
 */
@Service
@Transactional
public class PoliceServiceImpl implements PoliceService {

    private final Logger log = LoggerFactory.getLogger(PoliceServiceImpl.class);

    private final PoliceRepository policeRepository;

    private final PoliceMapper policeMapper;

    public PoliceServiceImpl(PoliceRepository policeRepository, PoliceMapper policeMapper) {
        this.policeRepository = policeRepository;
        this.policeMapper = policeMapper;
    }

    /**
     * Save a police.
     *
     * @param policeDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PoliceDTO save(PoliceDTO policeDTO) {
        log.debug("Request to save Police : {}", policeDTO);
        Police police = policeMapper.toEntity(policeDTO);
        police = policeRepository.save(police);
        return policeMapper.toDto(police);
    }

    /**
     * Get all the police.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<PoliceDTO> findAll() {
        log.debug("Request to get all Police");
        return policeRepository.findAll().stream()
            .map(policeMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one police by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PoliceDTO> findOne(Long id) {
        log.debug("Request to get Police : {}", id);
        return policeRepository.findById(id)
            .map(policeMapper::toDto);
    }

    /**
     * Delete the police by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Police : {}", id);
        policeRepository.deleteById(id);
    }
}
