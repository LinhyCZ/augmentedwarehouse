package cz.zcu.kiv.augmentedwarehouse.service.impl;

import cz.zcu.kiv.augmentedwarehouse.service.BaleniService;
import cz.zcu.kiv.augmentedwarehouse.domain.Baleni;
import cz.zcu.kiv.augmentedwarehouse.repository.BaleniRepository;
import cz.zcu.kiv.augmentedwarehouse.service.dto.BaleniDTO;
import cz.zcu.kiv.augmentedwarehouse.service.mapper.BaleniMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Baleni}.
 */
@Service
@Transactional
public class BaleniServiceImpl implements BaleniService {

    private final Logger log = LoggerFactory.getLogger(BaleniServiceImpl.class);

    private final BaleniRepository baleniRepository;

    private final BaleniMapper baleniMapper;

    public BaleniServiceImpl(BaleniRepository baleniRepository, BaleniMapper baleniMapper) {
        this.baleniRepository = baleniRepository;
        this.baleniMapper = baleniMapper;
    }

    /**
     * Save a baleni.
     *
     * @param baleniDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public BaleniDTO save(BaleniDTO baleniDTO) {
        log.debug("Request to save Baleni : {}", baleniDTO);
        Baleni baleni = baleniMapper.toEntity(baleniDTO);
        baleni = baleniRepository.save(baleni);
        return baleniMapper.toDto(baleni);
    }

    /**
     * Get all the balenis.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<BaleniDTO> findAll() {
        log.debug("Request to get all Balenis");
        return baleniRepository.findAll().stream()
            .map(baleniMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one baleni by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<BaleniDTO> findOne(Long id) {
        log.debug("Request to get Baleni : {}", id);
        return baleniRepository.findById(id)
            .map(baleniMapper::toDto);
    }

    /**
     * Delete the baleni by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Baleni : {}", id);
        baleniRepository.deleteById(id);
    }
}
