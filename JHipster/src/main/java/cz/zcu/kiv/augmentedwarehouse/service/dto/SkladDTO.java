package cz.zcu.kiv.augmentedwarehouse.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link cz.zcu.kiv.augmentedwarehouse.domain.Sklad} entity.
 */
public class SkladDTO implements Serializable {
    
    private Long id;

    private String adresa;

    private Integer cisloPopisne;

    private Integer cisloOrientacni;

    private String mesto;

    @Min(value = 10000)
    @Max(value = 99999)
    private Integer pSC;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public Integer getCisloPopisne() {
        return cisloPopisne;
    }

    public void setCisloPopisne(Integer cisloPopisne) {
        this.cisloPopisne = cisloPopisne;
    }

    public Integer getCisloOrientacni() {
        return cisloOrientacni;
    }

    public void setCisloOrientacni(Integer cisloOrientacni) {
        this.cisloOrientacni = cisloOrientacni;
    }

    public String getMesto() {
        return mesto;
    }

    public void setMesto(String mesto) {
        this.mesto = mesto;
    }

    public Integer getpSC() {
        return pSC;
    }

    public void setpSC(Integer pSC) {
        this.pSC = pSC;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SkladDTO skladDTO = (SkladDTO) o;
        if (skladDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), skladDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SkladDTO{" +
            "id=" + getId() +
            ", adresa='" + getAdresa() + "'" +
            ", cisloPopisne=" + getCisloPopisne() +
            ", cisloOrientacni=" + getCisloOrientacni() +
            ", mesto='" + getMesto() + "'" +
            ", pSC=" + getpSC() +
            "}";
    }
}
