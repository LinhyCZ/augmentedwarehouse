package cz.zcu.kiv.augmentedwarehouse.service.mapper;


import cz.zcu.kiv.augmentedwarehouse.domain.*;
import cz.zcu.kiv.augmentedwarehouse.service.dto.StojanDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Stojan} and its DTO {@link StojanDTO}.
 */
@Mapper(componentModel = "spring", uses = {SkladMapper.class})
public interface StojanMapper extends EntityMapper<StojanDTO, Stojan> {

    @Mapping(source = "sklad.id", target = "skladId")
    StojanDTO toDto(Stojan stojan);

    @Mapping(target = "police", ignore = true)
    @Mapping(target = "removePolice", ignore = true)
    @Mapping(source = "skladId", target = "sklad")
    Stojan toEntity(StojanDTO stojanDTO);

    default Stojan fromId(Long id) {
        if (id == null) {
            return null;
        }
        Stojan stojan = new Stojan();
        stojan.setId(id);
        return stojan;
    }
}
