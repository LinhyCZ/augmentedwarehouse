package cz.zcu.kiv.augmentedwarehouse.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link cz.zcu.kiv.augmentedwarehouse.domain.TypKS} entity.
 */
public class TypKSDTO implements Serializable {
    
    private Long id;

    private Integer pocetJednotekVKS;

    private Integer cenaKS;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPocetJednotekVKS() {
        return pocetJednotekVKS;
    }

    public void setPocetJednotekVKS(Integer pocetJednotekVKS) {
        this.pocetJednotekVKS = pocetJednotekVKS;
    }

    public Integer getCenaKS() {
        return cenaKS;
    }

    public void setCenaKS(Integer cenaKS) {
        this.cenaKS = cenaKS;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TypKSDTO typKSDTO = (TypKSDTO) o;
        if (typKSDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), typKSDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TypKSDTO{" +
            "id=" + getId() +
            ", pocetJednotekVKS=" + getPocetJednotekVKS() +
            ", cenaKS=" + getCenaKS() +
            "}";
    }
}
