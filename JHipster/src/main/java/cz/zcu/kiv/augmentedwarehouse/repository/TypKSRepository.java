package cz.zcu.kiv.augmentedwarehouse.repository;

import cz.zcu.kiv.augmentedwarehouse.domain.TypKS;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the TypKS entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TypKSRepository extends JpaRepository<TypKS, Long> {
}
