package cz.zcu.kiv.augmentedwarehouse.service.mapper;


import cz.zcu.kiv.augmentedwarehouse.domain.*;
import cz.zcu.kiv.augmentedwarehouse.service.dto.SkladDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Sklad} and its DTO {@link SkladDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SkladMapper extends EntityMapper<SkladDTO, Sklad> {


    @Mapping(target = "stojans", ignore = true)
    @Mapping(target = "removeStojan", ignore = true)
    Sklad toEntity(SkladDTO skladDTO);

    default Sklad fromId(Long id) {
        if (id == null) {
            return null;
        }
        Sklad sklad = new Sklad();
        sklad.setId(id);
        return sklad;
    }
}
