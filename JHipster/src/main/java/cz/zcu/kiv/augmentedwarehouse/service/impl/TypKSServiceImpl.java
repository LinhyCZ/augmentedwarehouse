package cz.zcu.kiv.augmentedwarehouse.service.impl;

import cz.zcu.kiv.augmentedwarehouse.service.TypKSService;
import cz.zcu.kiv.augmentedwarehouse.domain.TypKS;
import cz.zcu.kiv.augmentedwarehouse.repository.TypKSRepository;
import cz.zcu.kiv.augmentedwarehouse.service.dto.TypKSDTO;
import cz.zcu.kiv.augmentedwarehouse.service.mapper.TypKSMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link TypKS}.
 */
@Service
@Transactional
public class TypKSServiceImpl implements TypKSService {

    private final Logger log = LoggerFactory.getLogger(TypKSServiceImpl.class);

    private final TypKSRepository typKSRepository;

    private final TypKSMapper typKSMapper;

    public TypKSServiceImpl(TypKSRepository typKSRepository, TypKSMapper typKSMapper) {
        this.typKSRepository = typKSRepository;
        this.typKSMapper = typKSMapper;
    }

    /**
     * Save a typKS.
     *
     * @param typKSDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public TypKSDTO save(TypKSDTO typKSDTO) {
        log.debug("Request to save TypKS : {}", typKSDTO);
        TypKS typKS = typKSMapper.toEntity(typKSDTO);
        typKS = typKSRepository.save(typKS);
        return typKSMapper.toDto(typKS);
    }

    /**
     * Get all the typKS.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<TypKSDTO> findAll() {
        log.debug("Request to get all TypKS");
        return typKSRepository.findAll().stream()
            .map(typKSMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one typKS by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TypKSDTO> findOne(Long id) {
        log.debug("Request to get TypKS : {}", id);
        return typKSRepository.findById(id)
            .map(typKSMapper::toDto);
    }

    /**
     * Delete the typKS by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TypKS : {}", id);
        typKSRepository.deleteById(id);
    }
}
