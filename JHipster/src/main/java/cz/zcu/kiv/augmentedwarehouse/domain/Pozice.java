package cz.zcu.kiv.augmentedwarehouse.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A Pozice.
 */
@Entity
@Table(name = "pozice")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Pozice implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "qr_code")
    private String qrCode;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "pozice_baleni",
               joinColumns = @JoinColumn(name = "pozice_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "baleni_id", referencedColumnName = "id"))
    private Set<Baleni> balenis = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("pozices")
    private Sloupec sloupec;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQrCode() {
        return qrCode;
    }

    public Pozice qrCode(String qrCode) {
        this.qrCode = qrCode;
        return this;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public Set<Baleni> getBalenis() {
        return balenis;
    }

    public Pozice balenis(Set<Baleni> balenis) {
        this.balenis = balenis;
        return this;
    }

    public Pozice addBaleni(Baleni baleni) {
        this.balenis.add(baleni);
        baleni.getPozices().add(this);
        return this;
    }

    public Pozice removeBaleni(Baleni baleni) {
        this.balenis.remove(baleni);
        baleni.getPozices().remove(this);
        return this;
    }

    public void setBalenis(Set<Baleni> balenis) {
        this.balenis = balenis;
    }

    public Sloupec getSloupec() {
        return sloupec;
    }

    public Pozice sloupec(Sloupec sloupec) {
        this.sloupec = sloupec;
        return this;
    }

    public void setSloupec(Sloupec sloupec) {
        this.sloupec = sloupec;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Pozice)) {
            return false;
        }
        return id != null && id.equals(((Pozice) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Pozice{" +
            "id=" + getId() +
            ", qrCode='" + getQrCode() + "'" +
            "}";
    }
}
