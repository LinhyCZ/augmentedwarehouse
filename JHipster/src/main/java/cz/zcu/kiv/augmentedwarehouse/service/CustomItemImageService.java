package cz.zcu.kiv.augmentedwarehouse.service;

import cz.zcu.kiv.augmentedwarehouse.repository.CustomItemImageProjection;

import java.util.List;

public interface CustomItemImageService {
    public CustomItemImageProjection getItemImage(int itemId);
}
