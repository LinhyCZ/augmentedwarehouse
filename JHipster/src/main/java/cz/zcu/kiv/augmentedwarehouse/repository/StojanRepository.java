package cz.zcu.kiv.augmentedwarehouse.repository;

import cz.zcu.kiv.augmentedwarehouse.domain.Stojan;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Stojan entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StojanRepository extends JpaRepository<Stojan, Long> {
}
