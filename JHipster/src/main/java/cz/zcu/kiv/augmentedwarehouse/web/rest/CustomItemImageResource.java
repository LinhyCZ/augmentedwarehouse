package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.repository.CustomItemImageProjection;
import cz.zcu.kiv.augmentedwarehouse.repository.CustomItemImageRepository;
import cz.zcu.kiv.augmentedwarehouse.service.CustomItemImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/custom-api")
@Transactional
public class CustomItemImageResource {
    private final CustomItemImageRepository customItemImageRepository;

    @Autowired
    private CustomItemImageService service;

    public CustomItemImageResource(CustomItemImageRepository customItemImageRepository) {
        this.customItemImageRepository = customItemImageRepository;
    }

    @GetMapping("/image/{id}")
    public ResponseEntity<CustomItemImageProjection> getImage(@PathVariable int id) {
        CustomItemImageProjection customItemImageProjection = this.service.getItemImage(id);
        return new ResponseEntity<>(customItemImageProjection, HttpStatus.OK);
    }
}
