package cz.zcu.kiv.augmentedwarehouse.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A Stojan.
 */
@Entity
@Table(name = "stojan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Stojan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "stojan")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Police> police = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("stojans")
    private Sklad sklad;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Police> getPolice() {
        return police;
    }

    public Stojan police(Set<Police> police) {
        this.police = police;
        return this;
    }

    public Stojan addPolice(Police police) {
        this.police.add(police);
        police.setStojan(this);
        return this;
    }

    public Stojan removePolice(Police police) {
        this.police.remove(police);
        police.setStojan(null);
        return this;
    }

    public void setPolice(Set<Police> police) {
        this.police = police;
    }

    public Sklad getSklad() {
        return sklad;
    }

    public Stojan sklad(Sklad sklad) {
        this.sklad = sklad;
        return this;
    }

    public void setSklad(Sklad sklad) {
        this.sklad = sklad;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Stojan)) {
            return false;
        }
        return id != null && id.equals(((Stojan) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Stojan{" +
            "id=" + getId() +
            "}";
    }
}
