package cz.zcu.kiv.augmentedwarehouse.service.mapper;


import cz.zcu.kiv.augmentedwarehouse.domain.*;
import cz.zcu.kiv.augmentedwarehouse.service.dto.BaleniDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Baleni} and its DTO {@link BaleniDTO}.
 */
@Mapper(componentModel = "spring", uses = {ZboziMapper.class, TypKSMapper.class})
public interface BaleniMapper extends EntityMapper<BaleniDTO, Baleni> {

    @Mapping(source = "zbozi.id", target = "zboziId")
    @Mapping(source = "typKS.id", target = "typKSId")
    BaleniDTO toDto(Baleni baleni);

    @Mapping(source = "zboziId", target = "zbozi")
    @Mapping(source = "typKSId", target = "typKS")
    @Mapping(target = "pozices", ignore = true)
    @Mapping(target = "removePozice", ignore = true)
    Baleni toEntity(BaleniDTO baleniDTO);

    default Baleni fromId(Long id) {
        if (id == null) {
            return null;
        }
        Baleni baleni = new Baleni();
        baleni.setId(id);
        return baleni;
    }
}
