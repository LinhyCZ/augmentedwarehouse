package cz.zcu.kiv.augmentedwarehouse.service.impl;

import cz.zcu.kiv.augmentedwarehouse.service.SloupecService;
import cz.zcu.kiv.augmentedwarehouse.domain.Sloupec;
import cz.zcu.kiv.augmentedwarehouse.repository.SloupecRepository;
import cz.zcu.kiv.augmentedwarehouse.service.dto.SloupecDTO;
import cz.zcu.kiv.augmentedwarehouse.service.mapper.SloupecMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Sloupec}.
 */
@Service
@Transactional
public class SloupecServiceImpl implements SloupecService {

    private final Logger log = LoggerFactory.getLogger(SloupecServiceImpl.class);

    private final SloupecRepository sloupecRepository;

    private final SloupecMapper sloupecMapper;

    public SloupecServiceImpl(SloupecRepository sloupecRepository, SloupecMapper sloupecMapper) {
        this.sloupecRepository = sloupecRepository;
        this.sloupecMapper = sloupecMapper;
    }

    /**
     * Save a sloupec.
     *
     * @param sloupecDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public SloupecDTO save(SloupecDTO sloupecDTO) {
        log.debug("Request to save Sloupec : {}", sloupecDTO);
        Sloupec sloupec = sloupecMapper.toEntity(sloupecDTO);
        sloupec = sloupecRepository.save(sloupec);
        return sloupecMapper.toDto(sloupec);
    }

    /**
     * Get all the sloupecs.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<SloupecDTO> findAll() {
        log.debug("Request to get all Sloupecs");
        return sloupecRepository.findAll().stream()
            .map(sloupecMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one sloupec by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SloupecDTO> findOne(Long id) {
        log.debug("Request to get Sloupec : {}", id);
        return sloupecRepository.findById(id)
            .map(sloupecMapper::toDto);
    }

    /**
     * Delete the sloupec by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Sloupec : {}", id);
        sloupecRepository.deleteById(id);
    }
}
