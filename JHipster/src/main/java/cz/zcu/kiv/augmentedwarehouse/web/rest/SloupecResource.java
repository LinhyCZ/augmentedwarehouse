package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.domain.Sloupec;
import cz.zcu.kiv.augmentedwarehouse.repository.SloupecRepository;
import cz.zcu.kiv.augmentedwarehouse.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link cz.zcu.kiv.augmentedwarehouse.domain.Sloupec}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class SloupecResource {

    private final Logger log = LoggerFactory.getLogger(SloupecResource.class);

    private static final String ENTITY_NAME = "sloupec";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SloupecRepository sloupecRepository;

    public SloupecResource(SloupecRepository sloupecRepository) {
        this.sloupecRepository = sloupecRepository;
    }

    /**
     * {@code POST  /sloupecs} : Create a new sloupec.
     *
     * @param sloupec the sloupec to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new sloupec, or with status {@code 400 (Bad Request)} if the sloupec has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/sloupecs")
    public ResponseEntity<Sloupec> createSloupec(@RequestBody Sloupec sloupec) throws URISyntaxException {
        log.debug("REST request to save Sloupec : {}", sloupec);
        if (sloupec.getId() != null) {
            throw new BadRequestAlertException("A new sloupec cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Sloupec result = sloupecRepository.save(sloupec);
        return ResponseEntity.created(new URI("/api/sloupecs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /sloupecs} : Updates an existing sloupec.
     *
     * @param sloupec the sloupec to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sloupec,
     * or with status {@code 400 (Bad Request)} if the sloupec is not valid,
     * or with status {@code 500 (Internal Server Error)} if the sloupec couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/sloupecs")
    public ResponseEntity<Sloupec> updateSloupec(@RequestBody Sloupec sloupec) throws URISyntaxException {
        log.debug("REST request to update Sloupec : {}", sloupec);
        if (sloupec.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Sloupec result = sloupecRepository.save(sloupec);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, sloupec.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /sloupecs} : get all the sloupecs.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of sloupecs in body.
     */
    @GetMapping("/sloupecs")
    public List<Sloupec> getAllSloupecs() {
        log.debug("REST request to get all Sloupecs");
        return sloupecRepository.findAll();
    }

    /**
     * {@code GET  /sloupecs/:id} : get the "id" sloupec.
     *
     * @param id the id of the sloupec to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the sloupec, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/sloupecs/{id}")
    public ResponseEntity<Sloupec> getSloupec(@PathVariable Long id) {
        log.debug("REST request to get Sloupec : {}", id);
        Optional<Sloupec> sloupec = sloupecRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(sloupec);
    }

    /**
     * {@code DELETE  /sloupecs/:id} : delete the "id" sloupec.
     *
     * @param id the id of the sloupec to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/sloupecs/{id}")
    public ResponseEntity<Void> deleteSloupec(@PathVariable Long id) {
        log.debug("REST request to delete Sloupec : {}", id);
        sloupecRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
