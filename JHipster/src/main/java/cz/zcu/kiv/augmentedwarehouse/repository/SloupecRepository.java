package cz.zcu.kiv.augmentedwarehouse.repository;

import cz.zcu.kiv.augmentedwarehouse.domain.Sloupec;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Sloupec entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SloupecRepository extends JpaRepository<Sloupec, Long> {
}
