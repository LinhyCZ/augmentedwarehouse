package cz.zcu.kiv.augmentedwarehouse.repository;

import cz.zcu.kiv.augmentedwarehouse.domain.Zbozi;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Zbozi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ZboziRepository extends JpaRepository<Zbozi, Long> {
}
