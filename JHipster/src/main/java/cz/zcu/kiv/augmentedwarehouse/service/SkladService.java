package cz.zcu.kiv.augmentedwarehouse.service;

import cz.zcu.kiv.augmentedwarehouse.service.dto.SkladDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link cz.zcu.kiv.augmentedwarehouse.domain.Sklad}.
 */
public interface SkladService {

    /**
     * Save a sklad.
     *
     * @param skladDTO the entity to save.
     * @return the persisted entity.
     */
    SkladDTO save(SkladDTO skladDTO);

    /**
     * Get all the sklads.
     *
     * @return the list of entities.
     */
    List<SkladDTO> findAll();

    /**
     * Get the "id" sklad.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SkladDTO> findOne(Long id);

    /**
     * Delete the "id" sklad.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
