package cz.zcu.kiv.augmentedwarehouse.service;

import cz.zcu.kiv.augmentedwarehouse.service.dto.ZboziDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link cz.zcu.kiv.augmentedwarehouse.domain.Zbozi}.
 */
public interface ZboziService {

    /**
     * Save a zbozi.
     *
     * @param zboziDTO the entity to save.
     * @return the persisted entity.
     */
    ZboziDTO save(ZboziDTO zboziDTO);

    /**
     * Get all the zbozis.
     *
     * @return the list of entities.
     */
    List<ZboziDTO> findAll();

    /**
     * Get the "id" zbozi.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ZboziDTO> findOne(Long id);

    /**
     * Delete the "id" zbozi.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
