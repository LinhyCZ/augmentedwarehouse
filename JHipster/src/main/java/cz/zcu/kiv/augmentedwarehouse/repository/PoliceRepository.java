package cz.zcu.kiv.augmentedwarehouse.repository;

import cz.zcu.kiv.augmentedwarehouse.domain.Police;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Police entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PoliceRepository extends JpaRepository<Police, Long> {
}
