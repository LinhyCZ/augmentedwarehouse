package cz.zcu.kiv.augmentedwarehouse.service.mapper;


import cz.zcu.kiv.augmentedwarehouse.domain.*;
import cz.zcu.kiv.augmentedwarehouse.service.dto.PoliceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Police} and its DTO {@link PoliceDTO}.
 */
@Mapper(componentModel = "spring", uses = {StojanMapper.class})
public interface PoliceMapper extends EntityMapper<PoliceDTO, Police> {

    @Mapping(source = "stojan.id", target = "stojanId")
    PoliceDTO toDto(Police police);

    @Mapping(target = "sloupecs", ignore = true)
    @Mapping(target = "removeSloupec", ignore = true)
    @Mapping(source = "stojanId", target = "stojan")
    Police toEntity(PoliceDTO policeDTO);

    default Police fromId(Long id) {
        if (id == null) {
            return null;
        }
        Police police = new Police();
        police.setId(id);
        return police;
    }
}
