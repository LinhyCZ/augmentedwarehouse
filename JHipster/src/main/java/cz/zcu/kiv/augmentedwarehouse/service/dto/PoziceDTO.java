package cz.zcu.kiv.augmentedwarehouse.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the {@link cz.zcu.kiv.augmentedwarehouse.domain.Pozice} entity.
 */
public class PoziceDTO implements Serializable {
    
    private Long id;

    private Set<BaleniDTO> balenis = new HashSet<>();

    private Long sloupecId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<BaleniDTO> getBalenis() {
        return balenis;
    }

    public void setBalenis(Set<BaleniDTO> balenis) {
        this.balenis = balenis;
    }

    public Long getSloupecId() {
        return sloupecId;
    }

    public void setSloupecId(Long sloupecId) {
        this.sloupecId = sloupecId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PoziceDTO poziceDTO = (PoziceDTO) o;
        if (poziceDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), poziceDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PoziceDTO{" +
            "id=" + getId() +
            ", balenis='" + getBalenis() + "'" +
            ", sloupecId=" + getSloupecId() +
            "}";
    }
}
