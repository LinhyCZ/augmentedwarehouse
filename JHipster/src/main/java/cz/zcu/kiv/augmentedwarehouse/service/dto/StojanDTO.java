package cz.zcu.kiv.augmentedwarehouse.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link cz.zcu.kiv.augmentedwarehouse.domain.Stojan} entity.
 */
public class StojanDTO implements Serializable {
    
    private Long id;


    private Long skladId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSkladId() {
        return skladId;
    }

    public void setSkladId(Long skladId) {
        this.skladId = skladId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StojanDTO stojanDTO = (StojanDTO) o;
        if (stojanDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), stojanDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "StojanDTO{" +
            "id=" + getId() +
            ", skladId=" + getSkladId() +
            "}";
    }
}
