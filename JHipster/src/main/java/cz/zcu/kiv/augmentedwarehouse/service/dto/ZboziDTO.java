package cz.zcu.kiv.augmentedwarehouse.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link cz.zcu.kiv.augmentedwarehouse.domain.Zbozi} entity.
 */
public class ZboziDTO implements Serializable {
    
    private Long id;

    private String kategorie;

    private String slozeni;

    private String popisZbozi;

    private String ean;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKategorie() {
        return kategorie;
    }

    public void setKategorie(String kategorie) {
        this.kategorie = kategorie;
    }

    public String getSlozeni() {
        return slozeni;
    }

    public void setSlozeni(String slozeni) {
        this.slozeni = slozeni;
    }

    public String getPopisZbozi() {
        return popisZbozi;
    }

    public void setPopisZbozi(String popisZbozi) {
        this.popisZbozi = popisZbozi;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ZboziDTO zboziDTO = (ZboziDTO) o;
        if (zboziDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), zboziDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ZboziDTO{" +
            "id=" + getId() +
            ", kategorie='" + getKategorie() + "'" +
            ", slozeni='" + getSlozeni() + "'" +
            ", popisZbozi='" + getPopisZbozi() + "'" +
            ", ean='" + getEan() + "'" +
            "}";
    }
}
