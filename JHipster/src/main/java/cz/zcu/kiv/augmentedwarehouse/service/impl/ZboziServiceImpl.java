package cz.zcu.kiv.augmentedwarehouse.service.impl;

import cz.zcu.kiv.augmentedwarehouse.service.ZboziService;
import cz.zcu.kiv.augmentedwarehouse.domain.Zbozi;
import cz.zcu.kiv.augmentedwarehouse.repository.ZboziRepository;
import cz.zcu.kiv.augmentedwarehouse.service.dto.ZboziDTO;
import cz.zcu.kiv.augmentedwarehouse.service.mapper.ZboziMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Zbozi}.
 */
@Service
@Transactional
public class ZboziServiceImpl implements ZboziService {

    private final Logger log = LoggerFactory.getLogger(ZboziServiceImpl.class);

    private final ZboziRepository zboziRepository;

    private final ZboziMapper zboziMapper;

    public ZboziServiceImpl(ZboziRepository zboziRepository, ZboziMapper zboziMapper) {
        this.zboziRepository = zboziRepository;
        this.zboziMapper = zboziMapper;
    }

    /**
     * Save a zbozi.
     *
     * @param zboziDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ZboziDTO save(ZboziDTO zboziDTO) {
        log.debug("Request to save Zbozi : {}", zboziDTO);
        Zbozi zbozi = zboziMapper.toEntity(zboziDTO);
        zbozi = zboziRepository.save(zbozi);
        return zboziMapper.toDto(zbozi);
    }

    /**
     * Get all the zbozis.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<ZboziDTO> findAll() {
        log.debug("Request to get all Zbozis");
        return zboziRepository.findAll().stream()
            .map(zboziMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one zbozi by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ZboziDTO> findOne(Long id) {
        log.debug("Request to get Zbozi : {}", id);
        return zboziRepository.findById(id)
            .map(zboziMapper::toDto);
    }

    /**
     * Delete the zbozi by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Zbozi : {}", id);
        zboziRepository.deleteById(id);
    }
}
