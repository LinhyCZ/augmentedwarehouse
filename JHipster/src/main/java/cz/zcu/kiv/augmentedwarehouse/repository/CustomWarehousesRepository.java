package cz.zcu.kiv.augmentedwarehouse.repository;

import cz.zcu.kiv.augmentedwarehouse.domain.Sklad;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CustomWarehousesRepository extends JpaRepository<Sklad, Long> {
    @Query(value = "select * from sklad", nativeQuery = true)
    List<CustomWarehousesProjection> findAllWarehouses();


    @Query(
        value = "select * from sklad",
        countQuery = "select count(*) from sklad",
        nativeQuery = true)
    Page<CustomWarehousesProjection> findAllWarehouses(Pageable page);
}
