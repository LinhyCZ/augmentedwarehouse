package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.domain.Baleni;
import cz.zcu.kiv.augmentedwarehouse.repository.BaleniRepository;
import cz.zcu.kiv.augmentedwarehouse.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link cz.zcu.kiv.augmentedwarehouse.domain.Baleni}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class BaleniResource {

    private final Logger log = LoggerFactory.getLogger(BaleniResource.class);

    private static final String ENTITY_NAME = "baleni";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BaleniRepository baleniRepository;

    public BaleniResource(BaleniRepository baleniRepository) {
        this.baleniRepository = baleniRepository;
    }

    /**
     * {@code POST  /balenis} : Create a new baleni.
     *
     * @param baleni the baleni to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new baleni, or with status {@code 400 (Bad Request)} if the baleni has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/balenis")
    public ResponseEntity<Baleni> createBaleni(@RequestBody Baleni baleni) throws URISyntaxException {
        log.debug("REST request to save Baleni : {}", baleni);
        if (baleni.getId() != null) {
            throw new BadRequestAlertException("A new baleni cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Baleni result = baleniRepository.save(baleni);
        return ResponseEntity.created(new URI("/api/balenis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /balenis} : Updates an existing baleni.
     *
     * @param baleni the baleni to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated baleni,
     * or with status {@code 400 (Bad Request)} if the baleni is not valid,
     * or with status {@code 500 (Internal Server Error)} if the baleni couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/balenis")
    public ResponseEntity<Baleni> updateBaleni(@RequestBody Baleni baleni) throws URISyntaxException {
        log.debug("REST request to update Baleni : {}", baleni);
        if (baleni.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Baleni result = baleniRepository.save(baleni);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, baleni.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /balenis} : get all the balenis.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of balenis in body.
     */
    @GetMapping("/balenis")
    public List<Baleni> getAllBalenis() {
        log.debug("REST request to get all Balenis");
        return baleniRepository.findAll();
    }

    /**
     * {@code GET  /balenis/:id} : get the "id" baleni.
     *
     * @param id the id of the baleni to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the baleni, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/balenis/{id}")
    public ResponseEntity<Baleni> getBaleni(@PathVariable Long id) {
        log.debug("REST request to get Baleni : {}", id);
        Optional<Baleni> baleni = baleniRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(baleni);
    }

    /**
     * {@code DELETE  /balenis/:id} : delete the "id" baleni.
     *
     * @param id the id of the baleni to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/balenis/{id}")
    public ResponseEntity<Void> deleteBaleni(@PathVariable Long id) {
        log.debug("REST request to delete Baleni : {}", id);
        baleniRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
