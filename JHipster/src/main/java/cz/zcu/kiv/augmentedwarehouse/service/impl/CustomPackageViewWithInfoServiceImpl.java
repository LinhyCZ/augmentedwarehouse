package cz.zcu.kiv.augmentedwarehouse.service.impl;

import cz.zcu.kiv.augmentedwarehouse.repository.CustomPackageWithInfoViewProjection;
import cz.zcu.kiv.augmentedwarehouse.repository.CustomPackageWithInfoViewRepository;
import cz.zcu.kiv.augmentedwarehouse.service.CustomPackageWithInfoViewService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class CustomPackageViewWithInfoServiceImpl implements CustomPackageWithInfoViewService {
    CustomPackageWithInfoViewRepository customPackageWithInfoViewRepository;

    public CustomPackageViewWithInfoServiceImpl(CustomPackageWithInfoViewRepository customPackageWithInfoViewRepository) {
        this.customPackageWithInfoViewRepository = customPackageWithInfoViewRepository;
    }

    @Override
    public List<CustomPackageWithInfoViewProjection> getZboziInSklad(int skladId) {
        return this.customPackageWithInfoViewRepository.getZboziInSklad(skladId);
    }

    @Override
    public List<CustomPackageWithInfoViewProjection> getZboziInSkladByBaleniId(int skladId, int baleniId) {
        return this.customPackageWithInfoViewRepository.getZboziInSkladByBaleniId(skladId, baleniId);
    }

    @Override
    public List<CustomPackageWithInfoViewProjection> getZboziInSkladByQRCode(int skladId, String qrCode) {
        return this.customPackageWithInfoViewRepository.getZboziInSkladByQRCode(skladId, qrCode);
    }

    @Override
    public List<CustomPackageWithInfoViewProjection> getZboziInSkladByPositionQRCode(int skladId, String qrCode) {
        return this.customPackageWithInfoViewRepository.getZboziInSkladByPositionQRCode(skladId, qrCode);
    }

    @Override
    public List<CustomPackageWithInfoViewProjection> getZboziInSkladByQRCodePageable(int skladId, String qrCode, int pageNo, int pageSize, String sortBy) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<CustomPackageWithInfoViewProjection> pagedResult = this.customPackageWithInfoViewRepository.getZboziInSkladByQRCode(skladId, qrCode, paging);

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<CustomPackageWithInfoViewProjection>();
        }
    }

    @Override
    public List<CustomPackageWithInfoViewProjection> getZboziInSkladPageable(int skladId, Integer pageNo, Integer pageSize, String sortBy) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<CustomPackageWithInfoViewProjection> pagedResult = this.customPackageWithInfoViewRepository.getZboziInSklad(skladId, paging);

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<CustomPackageWithInfoViewProjection>();
        }
    }

    @Override
    public List<CustomPackageWithInfoViewProjection> getZboziInSkladByBaleniIdPageable(int skladId, int baleniId, Integer pageNo, Integer pageSize, String sortBy) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<CustomPackageWithInfoViewProjection> pagedResult = this.customPackageWithInfoViewRepository.getZboziInSkladByBaleniId(skladId, baleniId, paging);

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<CustomPackageWithInfoViewProjection>();
        }
    }

    @Override
    public List<CustomPackageWithInfoViewProjection> getZboziInSkladByPositionQRCodePageable(int skladId, String qrCode, Integer pageNo, Integer pageSize, String sortBy) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<CustomPackageWithInfoViewProjection> pagedResult = this.customPackageWithInfoViewRepository.getZboziInSkladByPositionQRCode(skladId, qrCode, paging);

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<CustomPackageWithInfoViewProjection>();
        }
    }
}
