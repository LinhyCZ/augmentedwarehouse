package cz.zcu.kiv.augmentedwarehouse.service.mapper;


import cz.zcu.kiv.augmentedwarehouse.domain.*;
import cz.zcu.kiv.augmentedwarehouse.service.dto.JednotkyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Jednotky} and its DTO {@link JednotkyDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface JednotkyMapper extends EntityMapper<JednotkyDTO, Jednotky> {



    default Jednotky fromId(Long id) {
        if (id == null) {
            return null;
        }
        Jednotky jednotky = new Jednotky();
        jednotky.setId(id);
        return jednotky;
    }
}
