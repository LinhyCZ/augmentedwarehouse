package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.domain.TypKS;
import cz.zcu.kiv.augmentedwarehouse.repository.TypKSRepository;
import cz.zcu.kiv.augmentedwarehouse.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link cz.zcu.kiv.augmentedwarehouse.domain.TypKS}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class TypKSResource {

    private final Logger log = LoggerFactory.getLogger(TypKSResource.class);

    private static final String ENTITY_NAME = "typKS";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TypKSRepository typKSRepository;

    public TypKSResource(TypKSRepository typKSRepository) {
        this.typKSRepository = typKSRepository;
    }

    /**
     * {@code POST  /typ-ks} : Create a new typKS.
     *
     * @param typKS the typKS to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new typKS, or with status {@code 400 (Bad Request)} if the typKS has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/typ-ks")
    public ResponseEntity<TypKS> createTypKS(@RequestBody TypKS typKS) throws URISyntaxException {
        log.debug("REST request to save TypKS : {}", typKS);
        if (typKS.getId() != null) {
            throw new BadRequestAlertException("A new typKS cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TypKS result = typKSRepository.save(typKS);
        return ResponseEntity.created(new URI("/api/typ-ks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /typ-ks} : Updates an existing typKS.
     *
     * @param typKS the typKS to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated typKS,
     * or with status {@code 400 (Bad Request)} if the typKS is not valid,
     * or with status {@code 500 (Internal Server Error)} if the typKS couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/typ-ks")
    public ResponseEntity<TypKS> updateTypKS(@RequestBody TypKS typKS) throws URISyntaxException {
        log.debug("REST request to update TypKS : {}", typKS);
        if (typKS.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TypKS result = typKSRepository.save(typKS);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, typKS.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /typ-ks} : get all the typKS.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of typKS in body.
     */
    @GetMapping("/typ-ks")
    public List<TypKS> getAllTypKS() {
        log.debug("REST request to get all TypKS");
        return typKSRepository.findAll();
    }

    /**
     * {@code GET  /typ-ks/:id} : get the "id" typKS.
     *
     * @param id the id of the typKS to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the typKS, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/typ-ks/{id}")
    public ResponseEntity<TypKS> getTypKS(@PathVariable Long id) {
        log.debug("REST request to get TypKS : {}", id);
        Optional<TypKS> typKS = typKSRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(typKS);
    }

    /**
     * {@code DELETE  /typ-ks/:id} : delete the "id" typKS.
     *
     * @param id the id of the typKS to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/typ-ks/{id}")
    public ResponseEntity<Void> deleteTypKS(@PathVariable Long id) {
        log.debug("REST request to delete TypKS : {}", id);
        typKSRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
