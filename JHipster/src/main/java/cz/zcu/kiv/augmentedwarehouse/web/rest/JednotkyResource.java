package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.domain.Jednotky;
import cz.zcu.kiv.augmentedwarehouse.repository.JednotkyRepository;
import cz.zcu.kiv.augmentedwarehouse.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link cz.zcu.kiv.augmentedwarehouse.domain.Jednotky}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class JednotkyResource {

    private final Logger log = LoggerFactory.getLogger(JednotkyResource.class);

    private static final String ENTITY_NAME = "jednotky";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final JednotkyRepository jednotkyRepository;

    public JednotkyResource(JednotkyRepository jednotkyRepository) {
        this.jednotkyRepository = jednotkyRepository;
    }

    /**
     * {@code POST  /jednotkies} : Create a new jednotky.
     *
     * @param jednotky the jednotky to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new jednotky, or with status {@code 400 (Bad Request)} if the jednotky has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/jednotkies")
    public ResponseEntity<Jednotky> createJednotky(@RequestBody Jednotky jednotky) throws URISyntaxException {
        log.debug("REST request to save Jednotky : {}", jednotky);
        if (jednotky.getId() != null) {
            throw new BadRequestAlertException("A new jednotky cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Jednotky result = jednotkyRepository.save(jednotky);
        return ResponseEntity.created(new URI("/api/jednotkies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /jednotkies} : Updates an existing jednotky.
     *
     * @param jednotky the jednotky to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated jednotky,
     * or with status {@code 400 (Bad Request)} if the jednotky is not valid,
     * or with status {@code 500 (Internal Server Error)} if the jednotky couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/jednotkies")
    public ResponseEntity<Jednotky> updateJednotky(@RequestBody Jednotky jednotky) throws URISyntaxException {
        log.debug("REST request to update Jednotky : {}", jednotky);
        if (jednotky.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Jednotky result = jednotkyRepository.save(jednotky);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, jednotky.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /jednotkies} : get all the jednotkies.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of jednotkies in body.
     */
    @GetMapping("/jednotkies")
    public List<Jednotky> getAllJednotkies() {
        log.debug("REST request to get all Jednotkies");
        return jednotkyRepository.findAll();
    }

    /**
     * {@code GET  /jednotkies/:id} : get the "id" jednotky.
     *
     * @param id the id of the jednotky to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the jednotky, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/jednotkies/{id}")
    public ResponseEntity<Jednotky> getJednotky(@PathVariable Long id) {
        log.debug("REST request to get Jednotky : {}", id);
        Optional<Jednotky> jednotky = jednotkyRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(jednotky);
    }

    /**
     * {@code DELETE  /jednotkies/:id} : delete the "id" jednotky.
     *
     * @param id the id of the jednotky to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/jednotkies/{id}")
    public ResponseEntity<Void> deleteJednotky(@PathVariable Long id) {
        log.debug("REST request to delete Jednotky : {}", id);
        jednotkyRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
