package cz.zcu.kiv.augmentedwarehouse.service;

import cz.zcu.kiv.augmentedwarehouse.service.dto.JednotkyDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link cz.zcu.kiv.augmentedwarehouse.domain.Jednotky}.
 */
public interface JednotkyService {

    /**
     * Save a jednotky.
     *
     * @param jednotkyDTO the entity to save.
     * @return the persisted entity.
     */
    JednotkyDTO save(JednotkyDTO jednotkyDTO);

    /**
     * Get all the jednotkies.
     *
     * @return the list of entities.
     */
    List<JednotkyDTO> findAll();

    /**
     * Get the "id" jednotky.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<JednotkyDTO> findOne(Long id);

    /**
     * Delete the "id" jednotky.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
