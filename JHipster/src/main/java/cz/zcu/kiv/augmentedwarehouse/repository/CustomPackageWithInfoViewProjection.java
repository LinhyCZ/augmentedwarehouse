package cz.zcu.kiv.augmentedwarehouse.repository;

public interface CustomPackageWithInfoViewProjection {
    String getBaleni_id();
    String getPocet_ks();
    String getBaleni_qr_code();
    String getPocet_jednotek_vks();
    String getCena_ks();
    String getNazev_jednotky();
    String getKategorie();
    String getSlozeni();
    String getPopis_zbozi();
    String getZbozi_ean();
    String getStojan_id();
    String getPolice_id();
    String getSloupec_id();
    String getPozice_id();
}
