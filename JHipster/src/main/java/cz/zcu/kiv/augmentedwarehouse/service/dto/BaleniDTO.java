package cz.zcu.kiv.augmentedwarehouse.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link cz.zcu.kiv.augmentedwarehouse.domain.Baleni} entity.
 */
public class BaleniDTO implements Serializable {
    
    private Long id;

    private Integer pocetKS;

    private String ean;


    private Long zboziId;

    private Long typKSId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPocetKS() {
        return pocetKS;
    }

    public void setPocetKS(Integer pocetKS) {
        this.pocetKS = pocetKS;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public Long getZboziId() {
        return zboziId;
    }

    public void setZboziId(Long zboziId) {
        this.zboziId = zboziId;
    }

    public Long getTypKSId() {
        return typKSId;
    }

    public void setTypKSId(Long typKSId) {
        this.typKSId = typKSId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BaleniDTO baleniDTO = (BaleniDTO) o;
        if (baleniDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), baleniDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BaleniDTO{" +
            "id=" + getId() +
            ", pocetKS=" + getPocetKS() +
            ", ean='" + getEan() + "'" +
            ", zboziId=" + getZboziId() +
            ", typKSId=" + getTypKSId() +
            "}";
    }
}
