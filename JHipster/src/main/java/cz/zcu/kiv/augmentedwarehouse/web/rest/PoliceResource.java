package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.domain.Police;
import cz.zcu.kiv.augmentedwarehouse.repository.PoliceRepository;
import cz.zcu.kiv.augmentedwarehouse.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link cz.zcu.kiv.augmentedwarehouse.domain.Police}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class PoliceResource {

    private final Logger log = LoggerFactory.getLogger(PoliceResource.class);

    private static final String ENTITY_NAME = "police";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PoliceRepository policeRepository;

    public PoliceResource(PoliceRepository policeRepository) {
        this.policeRepository = policeRepository;
    }

    /**
     * {@code POST  /police} : Create a new police.
     *
     * @param police the police to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new police, or with status {@code 400 (Bad Request)} if the police has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/police")
    public ResponseEntity<Police> createPolice(@RequestBody Police police) throws URISyntaxException {
        log.debug("REST request to save Police : {}", police);
        if (police.getId() != null) {
            throw new BadRequestAlertException("A new police cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Police result = policeRepository.save(police);
        return ResponseEntity.created(new URI("/api/police/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /police} : Updates an existing police.
     *
     * @param police the police to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated police,
     * or with status {@code 400 (Bad Request)} if the police is not valid,
     * or with status {@code 500 (Internal Server Error)} if the police couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/police")
    public ResponseEntity<Police> updatePolice(@RequestBody Police police) throws URISyntaxException {
        log.debug("REST request to update Police : {}", police);
        if (police.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Police result = policeRepository.save(police);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, police.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /police} : get all the police.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of police in body.
     */
    @GetMapping("/police")
    public List<Police> getAllPolice() {
        log.debug("REST request to get all Police");
        return policeRepository.findAll();
    }

    /**
     * {@code GET  /police/:id} : get the "id" police.
     *
     * @param id the id of the police to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the police, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/police/{id}")
    public ResponseEntity<Police> getPolice(@PathVariable Long id) {
        log.debug("REST request to get Police : {}", id);
        Optional<Police> police = policeRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(police);
    }

    /**
     * {@code DELETE  /police/:id} : delete the "id" police.
     *
     * @param id the id of the police to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/police/{id}")
    public ResponseEntity<Void> deletePolice(@PathVariable Long id) {
        log.debug("REST request to delete Police : {}", id);
        policeRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
