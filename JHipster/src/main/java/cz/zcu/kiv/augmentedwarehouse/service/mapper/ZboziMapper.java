package cz.zcu.kiv.augmentedwarehouse.service.mapper;


import cz.zcu.kiv.augmentedwarehouse.domain.*;
import cz.zcu.kiv.augmentedwarehouse.service.dto.ZboziDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Zbozi} and its DTO {@link ZboziDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ZboziMapper extends EntityMapper<ZboziDTO, Zbozi> {


    @Mapping(target = "balenis", ignore = true)
    @Mapping(target = "removeBaleni", ignore = true)
    Zbozi toEntity(ZboziDTO zboziDTO);

    default Zbozi fromId(Long id) {
        if (id == null) {
            return null;
        }
        Zbozi zbozi = new Zbozi();
        zbozi.setId(id);
        return zbozi;
    }
}
