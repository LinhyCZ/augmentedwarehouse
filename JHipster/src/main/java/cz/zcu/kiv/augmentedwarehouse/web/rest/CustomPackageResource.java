package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.repository.CustomPackageViewProjection;
import cz.zcu.kiv.augmentedwarehouse.repository.CustomPackageViewRepository;
import cz.zcu.kiv.augmentedwarehouse.service.CustomPackageViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/custom-api")
@Transactional
public class CustomPackageResource {
    private final CustomPackageViewRepository customPackageViewRepository;

    @Autowired
    private CustomPackageViewService service;

    public CustomPackageResource(CustomPackageViewRepository customPackageViewRepository) {
        this.customPackageViewRepository = customPackageViewRepository;
    }

    @GetMapping("/balenis/{id}")
    public ResponseEntity<CustomPackageViewProjection> getBaleni(@PathVariable int id) {
        CustomPackageViewProjection customPackageViewProjection = this.service.findItemViewProjection(id);
        return new ResponseEntity<CustomPackageViewProjection>(customPackageViewProjection, HttpStatus.OK);
    }
}
