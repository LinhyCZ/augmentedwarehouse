package cz.zcu.kiv.augmentedwarehouse.service;

import cz.zcu.kiv.augmentedwarehouse.repository.CustomPackageViewProjection;
import cz.zcu.kiv.augmentedwarehouse.repository.CustomPackageWithInfoViewProjection;

import java.util.List;

public interface CustomPackageWithInfoViewService {
    public List<CustomPackageWithInfoViewProjection> getZboziInSklad(int skladId);
    public List<CustomPackageWithInfoViewProjection> getZboziInSkladByBaleniId(int skladId, int baleniId);
    public List<CustomPackageWithInfoViewProjection> getZboziInSkladByQRCode(int skladId, String qrCode);
    public List<CustomPackageWithInfoViewProjection> getZboziInSkladByPositionQRCode(int skladId, String qrCode);
    public List<CustomPackageWithInfoViewProjection> getZboziInSkladByQRCodePageable(int skladId, String qrCode, int pageNo, int pageSize, String sortBy);
    public List<CustomPackageWithInfoViewProjection> getZboziInSkladPageable(int skladId, Integer pageNo, Integer pageSize, String sortBy);
    public List<CustomPackageWithInfoViewProjection> getZboziInSkladByBaleniIdPageable(int skladId, int baleniId, Integer pageNo, Integer pageSize, String sortBy);
    public List<CustomPackageWithInfoViewProjection> getZboziInSkladByPositionQRCodePageable(int skladId, String qrCode, Integer pageNo, Integer pageSize, String sortBy);
}
