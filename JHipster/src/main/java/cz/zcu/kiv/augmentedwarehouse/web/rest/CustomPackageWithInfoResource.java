package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.repository.CustomPackageWithInfoViewProjection;
import cz.zcu.kiv.augmentedwarehouse.repository.CustomPackageWithInfoViewRepository;
import cz.zcu.kiv.augmentedwarehouse.service.CustomPackageWithInfoViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/custom-api")
@Transactional
public class CustomPackageWithInfoResource {
    private final CustomPackageWithInfoViewRepository customPackageWithInfoViewRepository;

    @Autowired
    CustomPackageWithInfoViewService service;

    public CustomPackageWithInfoResource(CustomPackageWithInfoViewRepository customPackageWithInfoViewRepository) {
        this.customPackageWithInfoViewRepository = customPackageWithInfoViewRepository;
    }

    @GetMapping("/zbozi/{skladId}/{baleniId}")
    public ResponseEntity<List<CustomPackageWithInfoViewProjection>> getZboziInSkladByBaleniId(@PathVariable int skladId, @PathVariable int baleniId) {
        List<CustomPackageWithInfoViewProjection> customPackageWithInfoViewProjection = service.getZboziInSkladByBaleniId(skladId, baleniId);
        return new ResponseEntity<List<CustomPackageWithInfoViewProjection>>(customPackageWithInfoViewProjection, HttpStatus.OK);
    }

    @GetMapping("/zbozi/{skladId}")
    public ResponseEntity<List<CustomPackageWithInfoViewProjection>> getZboziInSklad(@PathVariable int skladId) {
        List<CustomPackageWithInfoViewProjection> customPackageWithInfoViewProjection = service.getZboziInSklad(skladId);
        return new ResponseEntity<List<CustomPackageWithInfoViewProjection>>(customPackageWithInfoViewProjection, HttpStatus.OK);
    }

    @GetMapping("/zbozi/{skladId}/byQr/{qrCode}")
    public ResponseEntity<List<CustomPackageWithInfoViewProjection>> getZboziInSkladByQRCode(@PathVariable int skladId, @PathVariable String qrCode) {
        List<CustomPackageWithInfoViewProjection> customPackageWithInfoViewProjection = service.getZboziInSkladByQRCode(skladId, qrCode);
        return new ResponseEntity<List<CustomPackageWithInfoViewProjection>>(customPackageWithInfoViewProjection, HttpStatus.OK);
    }

    @GetMapping("/zbozi/{skladId}/byPositionQr/{qrCode}")
    public ResponseEntity<List<CustomPackageWithInfoViewProjection>> getZboziInSkladByPositionQRCode(@PathVariable int skladId, @PathVariable String qrCode) {
        List<CustomPackageWithInfoViewProjection> customPackageWithInfoViewProjection = service.getZboziInSkladByPositionQRCode(skladId, qrCode);
        return new ResponseEntity<List<CustomPackageWithInfoViewProjection>>(customPackageWithInfoViewProjection, HttpStatus.OK);
    }

    @GetMapping("/pageable/zbozi/{skladId}/{baleniId}")
    public ResponseEntity<List<CustomPackageWithInfoViewProjection>> getZboziInSkladByBaleniId(
        @PathVariable int skladId,
        @PathVariable int baleniId,
        @RequestParam(defaultValue = "0") Integer pageNo,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @RequestParam(defaultValue = "baleni_id") String sortBy)
    {
        List<CustomPackageWithInfoViewProjection> customPackageWithInfoViewProjection = service.getZboziInSkladByBaleniIdPageable(skladId, baleniId, pageNo, pageSize, sortBy);
        return new ResponseEntity<List<CustomPackageWithInfoViewProjection>>(customPackageWithInfoViewProjection, HttpStatus.OK);
    }

    @GetMapping("/pageable/zbozi/{skladId}")
    public ResponseEntity<List<CustomPackageWithInfoViewProjection>> getZboziInSklad(
        @PathVariable int skladId,
        @RequestParam(defaultValue = "0") Integer pageNo,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @RequestParam(defaultValue = "baleni_id") String sortBy) {
        List<CustomPackageWithInfoViewProjection> customPackageWithInfoViewProjection = service.getZboziInSkladPageable(skladId, pageNo, pageSize, sortBy);
        return new ResponseEntity<List<CustomPackageWithInfoViewProjection>>(customPackageWithInfoViewProjection, HttpStatus.OK);
    }

    @GetMapping("/pageable/zbozi/{skladId}/byQr/{qrCode}")
    public ResponseEntity<List<CustomPackageWithInfoViewProjection>> getZboziInSkladByQRCodePageable(
        @PathVariable int skladId,
        @PathVariable String qrCode,
        @RequestParam(defaultValue = "0") Integer pageNo,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @RequestParam(defaultValue = "baleni_id") String sortBy)
    {
        List<CustomPackageWithInfoViewProjection> customPackageWithInfoViewProjection = service.getZboziInSkladByQRCodePageable(skladId, qrCode, pageNo, pageSize, sortBy);
        return new ResponseEntity<List<CustomPackageWithInfoViewProjection>>(customPackageWithInfoViewProjection, HttpStatus.OK);
    }

    @GetMapping("/pageable/zbozi/{skladId}/byPositionQr/{qrCode}")
    public ResponseEntity<List<CustomPackageWithInfoViewProjection>> getZboziInSkladByPositionQRCodePageable(
        @PathVariable int skladId,
        @PathVariable String qrCode,
        @RequestParam(defaultValue = "0") Integer pageNo,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @RequestParam(defaultValue = "baleni_id") String sortBy)
    {
        List<CustomPackageWithInfoViewProjection> customPackageWithInfoViewProjection = service.getZboziInSkladByPositionQRCodePageable(skladId, qrCode, pageNo, pageSize, sortBy);
        return new ResponseEntity<List<CustomPackageWithInfoViewProjection>>(customPackageWithInfoViewProjection, HttpStatus.OK);
    }
}
