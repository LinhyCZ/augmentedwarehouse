package cz.zcu.kiv.augmentedwarehouse.service.impl;

import cz.zcu.kiv.augmentedwarehouse.repository.CustomPackageWithInfoViewProjection;
import cz.zcu.kiv.augmentedwarehouse.repository.CustomWarehousesProjection;
import cz.zcu.kiv.augmentedwarehouse.repository.CustomWarehousesRepository;
import cz.zcu.kiv.augmentedwarehouse.service.CustomWarehouseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class CustomWarehousesServiceImpl implements CustomWarehouseService {
    CustomWarehousesRepository customWarehousesRepository;

    public CustomWarehousesServiceImpl(CustomWarehousesRepository customWarehousesRepository) {
        this.customWarehousesRepository = customWarehousesRepository;
    }

    @Override
    public List<CustomWarehousesProjection> getAllWarehouses() {
        return this.customWarehousesRepository.findAllWarehouses();
    }

    @Override
    public List<CustomWarehousesProjection> getAllWarehousesPageable(Integer pageNo, Integer pageSize, String sortBy) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<CustomWarehousesProjection> pagedResult = this.customWarehousesRepository.findAllWarehouses(paging);

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<CustomWarehousesProjection>();
        }
    }
}
