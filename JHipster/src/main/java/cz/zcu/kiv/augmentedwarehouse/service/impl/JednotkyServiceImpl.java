package cz.zcu.kiv.augmentedwarehouse.service.impl;

import cz.zcu.kiv.augmentedwarehouse.service.JednotkyService;
import cz.zcu.kiv.augmentedwarehouse.domain.Jednotky;
import cz.zcu.kiv.augmentedwarehouse.repository.JednotkyRepository;
import cz.zcu.kiv.augmentedwarehouse.service.dto.JednotkyDTO;
import cz.zcu.kiv.augmentedwarehouse.service.mapper.JednotkyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Jednotky}.
 */
@Service
@Transactional
public class JednotkyServiceImpl implements JednotkyService {

    private final Logger log = LoggerFactory.getLogger(JednotkyServiceImpl.class);

    private final JednotkyRepository jednotkyRepository;

    private final JednotkyMapper jednotkyMapper;

    public JednotkyServiceImpl(JednotkyRepository jednotkyRepository, JednotkyMapper jednotkyMapper) {
        this.jednotkyRepository = jednotkyRepository;
        this.jednotkyMapper = jednotkyMapper;
    }

    /**
     * Save a jednotky.
     *
     * @param jednotkyDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public JednotkyDTO save(JednotkyDTO jednotkyDTO) {
        log.debug("Request to save Jednotky : {}", jednotkyDTO);
        Jednotky jednotky = jednotkyMapper.toEntity(jednotkyDTO);
        jednotky = jednotkyRepository.save(jednotky);
        return jednotkyMapper.toDto(jednotky);
    }

    /**
     * Get all the jednotkies.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<JednotkyDTO> findAll() {
        log.debug("Request to get all Jednotkies");
        return jednotkyRepository.findAll().stream()
            .map(jednotkyMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one jednotky by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<JednotkyDTO> findOne(Long id) {
        log.debug("Request to get Jednotky : {}", id);
        return jednotkyRepository.findById(id)
            .map(jednotkyMapper::toDto);
    }

    /**
     * Delete the jednotky by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Jednotky : {}", id);
        jednotkyRepository.deleteById(id);
    }
}
