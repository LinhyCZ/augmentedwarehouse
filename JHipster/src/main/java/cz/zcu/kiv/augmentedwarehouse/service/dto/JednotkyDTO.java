package cz.zcu.kiv.augmentedwarehouse.service.dto;

import java.io.Serializable;
import java.util.Objects;
import cz.zcu.kiv.augmentedwarehouse.domain.enumeration.HlavniJednotky;

/**
 * A DTO for the {@link cz.zcu.kiv.augmentedwarehouse.domain.Jednotky} entity.
 */
public class JednotkyDTO implements Serializable {
    
    private Long id;

    private HlavniJednotky hlavniJednotka;

    private Double ratioDoHlavniJednotky;

    private String nazevJednotky;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public HlavniJednotky getHlavniJednotka() {
        return hlavniJednotka;
    }

    public void setHlavniJednotka(HlavniJednotky hlavniJednotka) {
        this.hlavniJednotka = hlavniJednotka;
    }

    public Double getRatioDoHlavniJednotky() {
        return ratioDoHlavniJednotky;
    }

    public void setRatioDoHlavniJednotky(Double ratioDoHlavniJednotky) {
        this.ratioDoHlavniJednotky = ratioDoHlavniJednotky;
    }

    public String getNazevJednotky() {
        return nazevJednotky;
    }

    public void setNazevJednotky(String nazevJednotky) {
        this.nazevJednotky = nazevJednotky;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        JednotkyDTO jednotkyDTO = (JednotkyDTO) o;
        if (jednotkyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), jednotkyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "JednotkyDTO{" +
            "id=" + getId() +
            ", hlavniJednotka='" + getHlavniJednotka() + "'" +
            ", ratioDoHlavniJednotky=" + getRatioDoHlavniJednotky() +
            ", nazevJednotky='" + getNazevJednotky() + "'" +
            "}";
    }
}
