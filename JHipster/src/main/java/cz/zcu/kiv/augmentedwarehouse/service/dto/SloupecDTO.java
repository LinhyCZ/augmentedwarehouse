package cz.zcu.kiv.augmentedwarehouse.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link cz.zcu.kiv.augmentedwarehouse.domain.Sloupec} entity.
 */
public class SloupecDTO implements Serializable {
    
    private Long id;


    private Long policeId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPoliceId() {
        return policeId;
    }

    public void setPoliceId(Long policeId) {
        this.policeId = policeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SloupecDTO sloupecDTO = (SloupecDTO) o;
        if (sloupecDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sloupecDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SloupecDTO{" +
            "id=" + getId() +
            ", policeId=" + getPoliceId() +
            "}";
    }
}
