package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.domain.Zbozi;
import cz.zcu.kiv.augmentedwarehouse.repository.ZboziRepository;
import cz.zcu.kiv.augmentedwarehouse.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link cz.zcu.kiv.augmentedwarehouse.domain.Zbozi}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ZboziResource {

    private final Logger log = LoggerFactory.getLogger(ZboziResource.class);

    private static final String ENTITY_NAME = "zbozi";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ZboziRepository zboziRepository;

    public ZboziResource(ZboziRepository zboziRepository) {
        this.zboziRepository = zboziRepository;
    }

    /**
     * {@code POST  /zbozis} : Create a new zbozi.
     *
     * @param zbozi the zbozi to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new zbozi, or with status {@code 400 (Bad Request)} if the zbozi has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/zbozis")
    public ResponseEntity<Zbozi> createZbozi(@RequestBody Zbozi zbozi) throws URISyntaxException {
        log.debug("REST request to save Zbozi : {}", zbozi);
        if (zbozi.getId() != null) {
            throw new BadRequestAlertException("A new zbozi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Zbozi result = zboziRepository.save(zbozi);
        return ResponseEntity.created(new URI("/api/zbozis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /zbozis} : Updates an existing zbozi.
     *
     * @param zbozi the zbozi to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated zbozi,
     * or with status {@code 400 (Bad Request)} if the zbozi is not valid,
     * or with status {@code 500 (Internal Server Error)} if the zbozi couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/zbozis")
    public ResponseEntity<Zbozi> updateZbozi(@RequestBody Zbozi zbozi) throws URISyntaxException {
        log.debug("REST request to update Zbozi : {}", zbozi);
        if (zbozi.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Zbozi result = zboziRepository.save(zbozi);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, zbozi.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /zbozis} : get all the zbozis.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of zbozis in body.
     */
    @GetMapping("/zbozis")
    public List<Zbozi> getAllZbozis() {
        log.debug("REST request to get all Zbozis");
        return zboziRepository.findAll();
    }

    /**
     * {@code GET  /zbozis/:id} : get the "id" zbozi.
     *
     * @param id the id of the zbozi to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the zbozi, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/zbozis/{id}")
    public ResponseEntity<Zbozi> getZbozi(@PathVariable Long id) {
        log.debug("REST request to get Zbozi : {}", id);
        Optional<Zbozi> zbozi = zboziRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(zbozi);
    }

    /**
     * {@code DELETE  /zbozis/:id} : delete the "id" zbozi.
     *
     * @param id the id of the zbozi to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/zbozis/{id}")
    public ResponseEntity<Void> deleteZbozi(@PathVariable Long id) {
        log.debug("REST request to delete Zbozi : {}", id);
        zboziRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
