package cz.zcu.kiv.augmentedwarehouse.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A TypKS.
 */
@Entity
@Table(name = "typ_ks")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TypKS implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "pocet_jednotek_vks")
    private Integer pocetJednotekVKS;

    @Column(name = "cena_ks")
    private Integer cenaKS;

    @OneToMany(mappedBy = "typKS")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Baleni> balenis = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("typKS")
    private Jednotky jednotky;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPocetJednotekVKS() {
        return pocetJednotekVKS;
    }

    public TypKS pocetJednotekVKS(Integer pocetJednotekVKS) {
        this.pocetJednotekVKS = pocetJednotekVKS;
        return this;
    }

    public void setPocetJednotekVKS(Integer pocetJednotekVKS) {
        this.pocetJednotekVKS = pocetJednotekVKS;
    }

    public Integer getCenaKS() {
        return cenaKS;
    }

    public TypKS cenaKS(Integer cenaKS) {
        this.cenaKS = cenaKS;
        return this;
    }

    public void setCenaKS(Integer cenaKS) {
        this.cenaKS = cenaKS;
    }

    public Set<Baleni> getBalenis() {
        return balenis;
    }

    public TypKS balenis(Set<Baleni> balenis) {
        this.balenis = balenis;
        return this;
    }

    public TypKS addBaleni(Baleni baleni) {
        this.balenis.add(baleni);
        baleni.setTypKS(this);
        return this;
    }

    public TypKS removeBaleni(Baleni baleni) {
        this.balenis.remove(baleni);
        baleni.setTypKS(null);
        return this;
    }

    public void setBalenis(Set<Baleni> balenis) {
        this.balenis = balenis;
    }

    public Jednotky getJednotky() {
        return jednotky;
    }

    public TypKS jednotky(Jednotky jednotky) {
        this.jednotky = jednotky;
        return this;
    }

    public void setJednotky(Jednotky jednotky) {
        this.jednotky = jednotky;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TypKS)) {
            return false;
        }
        return id != null && id.equals(((TypKS) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TypKS{" +
            "id=" + getId() +
            ", pocetJednotekVKS=" + getPocetJednotekVKS() +
            ", cenaKS=" + getCenaKS() +
            "}";
    }
}
