package cz.zcu.kiv.augmentedwarehouse.service.impl;

import cz.zcu.kiv.augmentedwarehouse.repository.CustomPackageViewProjection;
import cz.zcu.kiv.augmentedwarehouse.repository.CustomPackageViewRepository;
import cz.zcu.kiv.augmentedwarehouse.repository.CustomPackageWithInfoViewProjection;
import cz.zcu.kiv.augmentedwarehouse.service.CustomPackageViewService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class CustomPackageViewServiceImpl implements CustomPackageViewService {
    CustomPackageViewRepository customPackageViewRepository;

    public CustomPackageViewServiceImpl(CustomPackageViewRepository customPackageViewRepository) {
        this.customPackageViewRepository = customPackageViewRepository;
    }

    @Override
    public CustomPackageViewProjection findItemViewProjection(int packageId) {
        return this.customPackageViewRepository.findPackageByData(packageId);
    }
}
