/**
 * MapStruct mappers for mapping domain objects and Data Transfer Objects.
 */
package cz.zcu.kiv.augmentedwarehouse.service.mapper;
