package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.domain.Pozice;
import cz.zcu.kiv.augmentedwarehouse.repository.PoziceRepository;
import cz.zcu.kiv.augmentedwarehouse.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link cz.zcu.kiv.augmentedwarehouse.domain.Pozice}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class PoziceResource {

    private final Logger log = LoggerFactory.getLogger(PoziceResource.class);

    private static final String ENTITY_NAME = "pozice";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PoziceRepository poziceRepository;

    public PoziceResource(PoziceRepository poziceRepository) {
        this.poziceRepository = poziceRepository;
    }

    /**
     * {@code POST  /pozices} : Create a new pozice.
     *
     * @param pozice the pozice to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new pozice, or with status {@code 400 (Bad Request)} if the pozice has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/pozices")
    public ResponseEntity<Pozice> createPozice(@RequestBody Pozice pozice) throws URISyntaxException {
        log.debug("REST request to save Pozice : {}", pozice);
        if (pozice.getId() != null) {
            throw new BadRequestAlertException("A new pozice cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Pozice result = poziceRepository.save(pozice);
        return ResponseEntity.created(new URI("/api/pozices/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /pozices} : Updates an existing pozice.
     *
     * @param pozice the pozice to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pozice,
     * or with status {@code 400 (Bad Request)} if the pozice is not valid,
     * or with status {@code 500 (Internal Server Error)} if the pozice couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/pozices")
    public ResponseEntity<Pozice> updatePozice(@RequestBody Pozice pozice) throws URISyntaxException {
        log.debug("REST request to update Pozice : {}", pozice);
        if (pozice.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Pozice result = poziceRepository.save(pozice);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, pozice.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /pozices} : get all the pozices.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of pozices in body.
     */
    @GetMapping("/pozices")
    public List<Pozice> getAllPozices(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all Pozices");
        return poziceRepository.findAllWithEagerRelationships();
    }

    /**
     * {@code GET  /pozices/:id} : get the "id" pozice.
     *
     * @param id the id of the pozice to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the pozice, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/pozices/{id}")
    public ResponseEntity<Pozice> getPozice(@PathVariable Long id) {
        log.debug("REST request to get Pozice : {}", id);
        Optional<Pozice> pozice = poziceRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(pozice);
    }

    /**
     * {@code DELETE  /pozices/:id} : delete the "id" pozice.
     *
     * @param id the id of the pozice to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/pozices/{id}")
    public ResponseEntity<Void> deletePozice(@PathVariable Long id) {
        log.debug("REST request to delete Pozice : {}", id);
        poziceRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
