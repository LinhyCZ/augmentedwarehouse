package cz.zcu.kiv.augmentedwarehouse.service;

import cz.zcu.kiv.augmentedwarehouse.service.dto.TypKSDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link cz.zcu.kiv.augmentedwarehouse.domain.TypKS}.
 */
public interface TypKSService {

    /**
     * Save a typKS.
     *
     * @param typKSDTO the entity to save.
     * @return the persisted entity.
     */
    TypKSDTO save(TypKSDTO typKSDTO);

    /**
     * Get all the typKS.
     *
     * @return the list of entities.
     */
    List<TypKSDTO> findAll();

    /**
     * Get the "id" typKS.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TypKSDTO> findOne(Long id);

    /**
     * Delete the "id" typKS.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
