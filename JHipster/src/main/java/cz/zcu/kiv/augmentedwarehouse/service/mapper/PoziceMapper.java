package cz.zcu.kiv.augmentedwarehouse.service.mapper;


import cz.zcu.kiv.augmentedwarehouse.domain.*;
import cz.zcu.kiv.augmentedwarehouse.service.dto.PoziceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Pozice} and its DTO {@link PoziceDTO}.
 */
@Mapper(componentModel = "spring", uses = {BaleniMapper.class, SloupecMapper.class})
public interface PoziceMapper extends EntityMapper<PoziceDTO, Pozice> {

    @Mapping(source = "sloupec.id", target = "sloupecId")
    PoziceDTO toDto(Pozice pozice);

    @Mapping(target = "removeBaleni", ignore = true)
    @Mapping(source = "sloupecId", target = "sloupec")
    Pozice toEntity(PoziceDTO poziceDTO);

    default Pozice fromId(Long id) {
        if (id == null) {
            return null;
        }
        Pozice pozice = new Pozice();
        pozice.setId(id);
        return pozice;
    }
}
