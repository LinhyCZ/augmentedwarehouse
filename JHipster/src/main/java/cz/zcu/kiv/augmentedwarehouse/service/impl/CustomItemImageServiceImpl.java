package cz.zcu.kiv.augmentedwarehouse.service.impl;

import cz.zcu.kiv.augmentedwarehouse.repository.CustomItemImageProjection;
import cz.zcu.kiv.augmentedwarehouse.repository.CustomItemImageRepository;
import cz.zcu.kiv.augmentedwarehouse.service.CustomItemImageService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CustomItemImageServiceImpl implements CustomItemImageService {
    CustomItemImageRepository customItemImageRepository;

    public CustomItemImageServiceImpl(CustomItemImageRepository customItemImageRepository) {
        this.customItemImageRepository = customItemImageRepository;
    }

    @Override
    public CustomItemImageProjection getItemImage(int itemId) {
        CustomItemImageProjection customItemImageProjection = this.customItemImageRepository.getImageData(itemId);
        return customItemImageProjection;
    }
}
