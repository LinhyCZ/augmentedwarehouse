package cz.zcu.kiv.augmentedwarehouse.service;

import cz.zcu.kiv.augmentedwarehouse.service.dto.BaleniDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link cz.zcu.kiv.augmentedwarehouse.domain.Baleni}.
 */
public interface BaleniService {

    /**
     * Save a baleni.
     *
     * @param baleniDTO the entity to save.
     * @return the persisted entity.
     */
    BaleniDTO save(BaleniDTO baleniDTO);

    /**
     * Get all the balenis.
     *
     * @return the list of entities.
     */
    List<BaleniDTO> findAll();

    /**
     * Get the "id" baleni.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BaleniDTO> findOne(Long id);

    /**
     * Delete the "id" baleni.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
