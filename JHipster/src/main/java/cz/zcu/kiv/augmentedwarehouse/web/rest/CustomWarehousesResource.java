package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.repository.CustomWarehousesProjection;
import cz.zcu.kiv.augmentedwarehouse.repository.CustomWarehousesRepository;
import cz.zcu.kiv.augmentedwarehouse.service.CustomWarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/custom-api")
@Transactional
public class CustomWarehousesResource {
    private final CustomWarehousesRepository customWarehousesRepository;

    @Autowired
    private CustomWarehouseService service;

    public CustomWarehousesResource(CustomWarehousesRepository customWarehousesRepository) {
        this.customWarehousesRepository = customWarehousesRepository;
    }

    @GetMapping("/warehouses")
    public ResponseEntity<List<CustomWarehousesProjection>> getWarehouses() {
        List<CustomWarehousesProjection> customWarehousesProjection = this.service.getAllWarehouses();
        return new ResponseEntity<List<CustomWarehousesProjection>>(customWarehousesProjection, HttpStatus.OK);
    }

    @GetMapping("/pageable/warehouses")
    public ResponseEntity<List<CustomWarehousesProjection>> getWarehouses(
        @RequestParam(defaultValue = "0") Integer pageNo,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @RequestParam(defaultValue = "id") String sortBy
    ) {
        List<CustomWarehousesProjection> customWarehousesProjection = this.service.getAllWarehousesPageable(pageNo, pageSize, sortBy);
        return new ResponseEntity<List<CustomWarehousesProjection>>(customWarehousesProjection, HttpStatus.OK);
    }
}
