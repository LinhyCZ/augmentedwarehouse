package cz.zcu.kiv.augmentedwarehouse.repository;

import cz.zcu.kiv.augmentedwarehouse.domain.Jednotky;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Jednotky entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JednotkyRepository extends JpaRepository<Jednotky, Long> {
}
