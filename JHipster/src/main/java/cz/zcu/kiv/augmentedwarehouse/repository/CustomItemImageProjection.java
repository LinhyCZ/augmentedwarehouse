package cz.zcu.kiv.augmentedwarehouse.repository;

public interface CustomItemImageProjection {
    String getObrazek();
}
