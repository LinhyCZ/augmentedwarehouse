import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { BaleniUpdateComponent } from 'app/entities/baleni/baleni-update.component';
import { BaleniService } from 'app/entities/baleni/baleni.service';
import { Baleni } from 'app/shared/model/baleni.model';

describe('Component Tests', () => {
  describe('Baleni Management Update Component', () => {
    let comp: BaleniUpdateComponent;
    let fixture: ComponentFixture<BaleniUpdateComponent>;
    let service: BaleniService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [BaleniUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(BaleniUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BaleniUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BaleniService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Baleni(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Baleni();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
