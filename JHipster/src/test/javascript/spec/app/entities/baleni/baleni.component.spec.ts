import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { BaleniComponent } from 'app/entities/baleni/baleni.component';
import { BaleniService } from 'app/entities/baleni/baleni.service';
import { Baleni } from 'app/shared/model/baleni.model';

describe('Component Tests', () => {
  describe('Baleni Management Component', () => {
    let comp: BaleniComponent;
    let fixture: ComponentFixture<BaleniComponent>;
    let service: BaleniService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [BaleniComponent]
      })
        .overrideTemplate(BaleniComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BaleniComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BaleniService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Baleni(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.balenis && comp.balenis[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
