import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { BaleniDetailComponent } from 'app/entities/baleni/baleni-detail.component';
import { Baleni } from 'app/shared/model/baleni.model';

describe('Component Tests', () => {
  describe('Baleni Management Detail Component', () => {
    let comp: BaleniDetailComponent;
    let fixture: ComponentFixture<BaleniDetailComponent>;
    const route = ({ data: of({ baleni: new Baleni(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [BaleniDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(BaleniDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BaleniDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load baleni on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.baleni).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
