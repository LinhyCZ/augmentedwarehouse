import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { ZboziComponent } from 'app/entities/zbozi/zbozi.component';
import { ZboziService } from 'app/entities/zbozi/zbozi.service';
import { Zbozi } from 'app/shared/model/zbozi.model';

describe('Component Tests', () => {
  describe('Zbozi Management Component', () => {
    let comp: ZboziComponent;
    let fixture: ComponentFixture<ZboziComponent>;
    let service: ZboziService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [ZboziComponent]
      })
        .overrideTemplate(ZboziComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ZboziComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ZboziService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Zbozi(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.zbozis && comp.zbozis[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
