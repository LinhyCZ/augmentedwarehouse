import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { ZboziUpdateComponent } from 'app/entities/zbozi/zbozi-update.component';
import { ZboziService } from 'app/entities/zbozi/zbozi.service';
import { Zbozi } from 'app/shared/model/zbozi.model';

describe('Component Tests', () => {
  describe('Zbozi Management Update Component', () => {
    let comp: ZboziUpdateComponent;
    let fixture: ComponentFixture<ZboziUpdateComponent>;
    let service: ZboziService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [ZboziUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ZboziUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ZboziUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ZboziService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Zbozi(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Zbozi();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
