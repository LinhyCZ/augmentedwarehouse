import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { JhiDataUtils } from 'ng-jhipster';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { ZboziDetailComponent } from 'app/entities/zbozi/zbozi-detail.component';
import { Zbozi } from 'app/shared/model/zbozi.model';

describe('Component Tests', () => {
  describe('Zbozi Management Detail Component', () => {
    let comp: ZboziDetailComponent;
    let fixture: ComponentFixture<ZboziDetailComponent>;
    let dataUtils: JhiDataUtils;
    const route = ({ data: of({ zbozi: new Zbozi(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [ZboziDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ZboziDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ZboziDetailComponent);
      comp = fixture.componentInstance;
      dataUtils = fixture.debugElement.injector.get(JhiDataUtils);
    });

    describe('OnInit', () => {
      it('Should load zbozi on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.zbozi).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });

    describe('byteSize', () => {
      it('Should call byteSize from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'byteSize');
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.byteSize(fakeBase64);

        // THEN
        expect(dataUtils.byteSize).toBeCalledWith(fakeBase64);
      });
    });

    describe('openFile', () => {
      it('Should call openFile from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'openFile');
        const fakeContentType = 'fake content type';
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.openFile(fakeContentType, fakeBase64);

        // THEN
        expect(dataUtils.openFile).toBeCalledWith(fakeContentType, fakeBase64);
      });
    });
  });
});
