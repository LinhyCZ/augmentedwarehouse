import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { StojanComponent } from 'app/entities/stojan/stojan.component';
import { StojanService } from 'app/entities/stojan/stojan.service';
import { Stojan } from 'app/shared/model/stojan.model';

describe('Component Tests', () => {
  describe('Stojan Management Component', () => {
    let comp: StojanComponent;
    let fixture: ComponentFixture<StojanComponent>;
    let service: StojanService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [StojanComponent]
      })
        .overrideTemplate(StojanComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(StojanComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(StojanService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Stojan(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.stojans && comp.stojans[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
