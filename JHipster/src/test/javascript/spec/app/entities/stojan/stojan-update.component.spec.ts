import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { StojanUpdateComponent } from 'app/entities/stojan/stojan-update.component';
import { StojanService } from 'app/entities/stojan/stojan.service';
import { Stojan } from 'app/shared/model/stojan.model';

describe('Component Tests', () => {
  describe('Stojan Management Update Component', () => {
    let comp: StojanUpdateComponent;
    let fixture: ComponentFixture<StojanUpdateComponent>;
    let service: StojanService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [StojanUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(StojanUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(StojanUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(StojanService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Stojan(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Stojan();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
