import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { StojanDetailComponent } from 'app/entities/stojan/stojan-detail.component';
import { Stojan } from 'app/shared/model/stojan.model';

describe('Component Tests', () => {
  describe('Stojan Management Detail Component', () => {
    let comp: StojanDetailComponent;
    let fixture: ComponentFixture<StojanDetailComponent>;
    const route = ({ data: of({ stojan: new Stojan(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [StojanDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(StojanDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(StojanDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load stojan on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.stojan).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
