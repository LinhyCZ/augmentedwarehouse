import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { PoliceComponent } from 'app/entities/police/police.component';
import { PoliceService } from 'app/entities/police/police.service';
import { Police } from 'app/shared/model/police.model';

describe('Component Tests', () => {
  describe('Police Management Component', () => {
    let comp: PoliceComponent;
    let fixture: ComponentFixture<PoliceComponent>;
    let service: PoliceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [PoliceComponent]
      })
        .overrideTemplate(PoliceComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PoliceComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PoliceService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Police(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.police && comp.police[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
