import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { PoliceUpdateComponent } from 'app/entities/police/police-update.component';
import { PoliceService } from 'app/entities/police/police.service';
import { Police } from 'app/shared/model/police.model';

describe('Component Tests', () => {
  describe('Police Management Update Component', () => {
    let comp: PoliceUpdateComponent;
    let fixture: ComponentFixture<PoliceUpdateComponent>;
    let service: PoliceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [PoliceUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(PoliceUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PoliceUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PoliceService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Police(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Police();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
