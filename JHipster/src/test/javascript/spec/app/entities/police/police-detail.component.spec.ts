import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { PoliceDetailComponent } from 'app/entities/police/police-detail.component';
import { Police } from 'app/shared/model/police.model';

describe('Component Tests', () => {
  describe('Police Management Detail Component', () => {
    let comp: PoliceDetailComponent;
    let fixture: ComponentFixture<PoliceDetailComponent>;
    const route = ({ data: of({ police: new Police(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [PoliceDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(PoliceDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PoliceDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load police on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.police).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
