import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { TypKSUpdateComponent } from 'app/entities/typ-ks/typ-ks-update.component';
import { TypKSService } from 'app/entities/typ-ks/typ-ks.service';
import { TypKS } from 'app/shared/model/typ-ks.model';

describe('Component Tests', () => {
  describe('TypKS Management Update Component', () => {
    let comp: TypKSUpdateComponent;
    let fixture: ComponentFixture<TypKSUpdateComponent>;
    let service: TypKSService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [TypKSUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(TypKSUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TypKSUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TypKSService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TypKS(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TypKS();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
