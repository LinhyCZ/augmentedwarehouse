import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { TypKSComponent } from 'app/entities/typ-ks/typ-ks.component';
import { TypKSService } from 'app/entities/typ-ks/typ-ks.service';
import { TypKS } from 'app/shared/model/typ-ks.model';

describe('Component Tests', () => {
  describe('TypKS Management Component', () => {
    let comp: TypKSComponent;
    let fixture: ComponentFixture<TypKSComponent>;
    let service: TypKSService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [TypKSComponent]
      })
        .overrideTemplate(TypKSComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TypKSComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TypKSService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new TypKS(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.typKS && comp.typKS[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
