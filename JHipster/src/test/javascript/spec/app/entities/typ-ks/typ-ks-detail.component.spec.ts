import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { TypKSDetailComponent } from 'app/entities/typ-ks/typ-ks-detail.component';
import { TypKS } from 'app/shared/model/typ-ks.model';

describe('Component Tests', () => {
  describe('TypKS Management Detail Component', () => {
    let comp: TypKSDetailComponent;
    let fixture: ComponentFixture<TypKSDetailComponent>;
    const route = ({ data: of({ typKS: new TypKS(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [TypKSDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(TypKSDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TypKSDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load typKS on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.typKS).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
