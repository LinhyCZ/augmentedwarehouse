import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { PoziceDetailComponent } from 'app/entities/pozice/pozice-detail.component';
import { Pozice } from 'app/shared/model/pozice.model';

describe('Component Tests', () => {
  describe('Pozice Management Detail Component', () => {
    let comp: PoziceDetailComponent;
    let fixture: ComponentFixture<PoziceDetailComponent>;
    const route = ({ data: of({ pozice: new Pozice(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [PoziceDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(PoziceDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PoziceDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load pozice on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.pozice).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
