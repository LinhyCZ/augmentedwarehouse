import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { PoziceComponent } from 'app/entities/pozice/pozice.component';
import { PoziceService } from 'app/entities/pozice/pozice.service';
import { Pozice } from 'app/shared/model/pozice.model';

describe('Component Tests', () => {
  describe('Pozice Management Component', () => {
    let comp: PoziceComponent;
    let fixture: ComponentFixture<PoziceComponent>;
    let service: PoziceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [PoziceComponent]
      })
        .overrideTemplate(PoziceComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PoziceComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PoziceService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Pozice(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.pozices && comp.pozices[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
