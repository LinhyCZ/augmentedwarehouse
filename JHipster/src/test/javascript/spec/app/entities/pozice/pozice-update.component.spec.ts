import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { PoziceUpdateComponent } from 'app/entities/pozice/pozice-update.component';
import { PoziceService } from 'app/entities/pozice/pozice.service';
import { Pozice } from 'app/shared/model/pozice.model';

describe('Component Tests', () => {
  describe('Pozice Management Update Component', () => {
    let comp: PoziceUpdateComponent;
    let fixture: ComponentFixture<PoziceUpdateComponent>;
    let service: PoziceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [PoziceUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(PoziceUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PoziceUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PoziceService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Pozice(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Pozice();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
