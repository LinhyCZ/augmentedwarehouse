import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { SkladUpdateComponent } from 'app/entities/sklad/sklad-update.component';
import { SkladService } from 'app/entities/sklad/sklad.service';
import { Sklad } from 'app/shared/model/sklad.model';

describe('Component Tests', () => {
  describe('Sklad Management Update Component', () => {
    let comp: SkladUpdateComponent;
    let fixture: ComponentFixture<SkladUpdateComponent>;
    let service: SkladService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [SkladUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(SkladUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SkladUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SkladService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Sklad(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Sklad();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
