import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { SkladComponent } from 'app/entities/sklad/sklad.component';
import { SkladService } from 'app/entities/sklad/sklad.service';
import { Sklad } from 'app/shared/model/sklad.model';

describe('Component Tests', () => {
  describe('Sklad Management Component', () => {
    let comp: SkladComponent;
    let fixture: ComponentFixture<SkladComponent>;
    let service: SkladService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [SkladComponent]
      })
        .overrideTemplate(SkladComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SkladComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SkladService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Sklad(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.sklads && comp.sklads[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
