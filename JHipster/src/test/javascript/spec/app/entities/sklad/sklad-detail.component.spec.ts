import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { SkladDetailComponent } from 'app/entities/sklad/sklad-detail.component';
import { Sklad } from 'app/shared/model/sklad.model';

describe('Component Tests', () => {
  describe('Sklad Management Detail Component', () => {
    let comp: SkladDetailComponent;
    let fixture: ComponentFixture<SkladDetailComponent>;
    const route = ({ data: of({ sklad: new Sklad(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [SkladDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(SkladDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SkladDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load sklad on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.sklad).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
