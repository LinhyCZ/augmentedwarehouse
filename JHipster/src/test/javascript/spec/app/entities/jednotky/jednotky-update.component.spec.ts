import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { JednotkyUpdateComponent } from 'app/entities/jednotky/jednotky-update.component';
import { JednotkyService } from 'app/entities/jednotky/jednotky.service';
import { Jednotky } from 'app/shared/model/jednotky.model';

describe('Component Tests', () => {
  describe('Jednotky Management Update Component', () => {
    let comp: JednotkyUpdateComponent;
    let fixture: ComponentFixture<JednotkyUpdateComponent>;
    let service: JednotkyService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [JednotkyUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(JednotkyUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(JednotkyUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(JednotkyService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Jednotky(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Jednotky();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
