import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { JednotkyDetailComponent } from 'app/entities/jednotky/jednotky-detail.component';
import { Jednotky } from 'app/shared/model/jednotky.model';

describe('Component Tests', () => {
  describe('Jednotky Management Detail Component', () => {
    let comp: JednotkyDetailComponent;
    let fixture: ComponentFixture<JednotkyDetailComponent>;
    const route = ({ data: of({ jednotky: new Jednotky(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [JednotkyDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(JednotkyDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(JednotkyDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load jednotky on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.jednotky).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
