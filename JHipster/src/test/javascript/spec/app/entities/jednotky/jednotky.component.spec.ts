import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { JednotkyComponent } from 'app/entities/jednotky/jednotky.component';
import { JednotkyService } from 'app/entities/jednotky/jednotky.service';
import { Jednotky } from 'app/shared/model/jednotky.model';

describe('Component Tests', () => {
  describe('Jednotky Management Component', () => {
    let comp: JednotkyComponent;
    let fixture: ComponentFixture<JednotkyComponent>;
    let service: JednotkyService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [JednotkyComponent]
      })
        .overrideTemplate(JednotkyComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(JednotkyComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(JednotkyService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Jednotky(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.jednotkies && comp.jednotkies[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
