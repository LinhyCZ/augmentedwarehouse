import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { JednotkyService } from 'app/entities/jednotky/jednotky.service';
import { IJednotky, Jednotky } from 'app/shared/model/jednotky.model';
import { HlavniJednotky } from 'app/shared/model/enumerations/hlavni-jednotky.model';

describe('Service Tests', () => {
  describe('Jednotky Service', () => {
    let injector: TestBed;
    let service: JednotkyService;
    let httpMock: HttpTestingController;
    let elemDefault: IJednotky;
    let expectedResult: IJednotky | IJednotky[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(JednotkyService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new Jednotky(0, HlavniJednotky.Ks, 0, 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Jednotky', () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Jednotky()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Jednotky', () => {
        const returnedFromService = Object.assign(
          {
            hlavniJednotka: 'BBBBBB',
            ratioDoHlavniJednotky: 1,
            nazevJednotky: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Jednotky', () => {
        const returnedFromService = Object.assign(
          {
            hlavniJednotka: 'BBBBBB',
            ratioDoHlavniJednotky: 1,
            nazevJednotky: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Jednotky', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
