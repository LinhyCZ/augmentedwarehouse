import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { SloupecComponent } from 'app/entities/sloupec/sloupec.component';
import { SloupecService } from 'app/entities/sloupec/sloupec.service';
import { Sloupec } from 'app/shared/model/sloupec.model';

describe('Component Tests', () => {
  describe('Sloupec Management Component', () => {
    let comp: SloupecComponent;
    let fixture: ComponentFixture<SloupecComponent>;
    let service: SloupecService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [SloupecComponent]
      })
        .overrideTemplate(SloupecComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SloupecComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SloupecService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Sloupec(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.sloupecs && comp.sloupecs[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
