import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { SloupecDetailComponent } from 'app/entities/sloupec/sloupec-detail.component';
import { Sloupec } from 'app/shared/model/sloupec.model';

describe('Component Tests', () => {
  describe('Sloupec Management Detail Component', () => {
    let comp: SloupecDetailComponent;
    let fixture: ComponentFixture<SloupecDetailComponent>;
    const route = ({ data: of({ sloupec: new Sloupec(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [SloupecDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(SloupecDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SloupecDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load sloupec on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.sloupec).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
