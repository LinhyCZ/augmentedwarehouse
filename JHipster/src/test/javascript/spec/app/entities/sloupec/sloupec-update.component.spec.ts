import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AugmentedWarehouseTestModule } from '../../../test.module';
import { SloupecUpdateComponent } from 'app/entities/sloupec/sloupec-update.component';
import { SloupecService } from 'app/entities/sloupec/sloupec.service';
import { Sloupec } from 'app/shared/model/sloupec.model';

describe('Component Tests', () => {
  describe('Sloupec Management Update Component', () => {
    let comp: SloupecUpdateComponent;
    let fixture: ComponentFixture<SloupecUpdateComponent>;
    let service: SloupecService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AugmentedWarehouseTestModule],
        declarations: [SloupecUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(SloupecUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SloupecUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SloupecService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Sloupec(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Sloupec();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
