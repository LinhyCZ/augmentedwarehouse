package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.AugmentedWarehouseApp;
import cz.zcu.kiv.augmentedwarehouse.domain.Jednotky;
import cz.zcu.kiv.augmentedwarehouse.repository.JednotkyRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import cz.zcu.kiv.augmentedwarehouse.domain.enumeration.HlavniJednotky;
/**
 * Integration tests for the {@link JednotkyResource} REST controller.
 */
@SpringBootTest(classes = AugmentedWarehouseApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class JednotkyResourceIT {

    private static final HlavniJednotky DEFAULT_HLAVNI_JEDNOTKA = HlavniJednotky.Ks;
    private static final HlavniJednotky UPDATED_HLAVNI_JEDNOTKA = HlavniJednotky.Mm;

    private static final Double DEFAULT_RATIO_DO_HLAVNI_JEDNOTKY = 1D;
    private static final Double UPDATED_RATIO_DO_HLAVNI_JEDNOTKY = 2D;

    private static final String DEFAULT_NAZEV_JEDNOTKY = "AAAAAAAAAA";
    private static final String UPDATED_NAZEV_JEDNOTKY = "BBBBBBBBBB";

    @Autowired
    private JednotkyRepository jednotkyRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restJednotkyMockMvc;

    private Jednotky jednotky;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Jednotky createEntity(EntityManager em) {
        Jednotky jednotky = new Jednotky()
            .hlavniJednotka(DEFAULT_HLAVNI_JEDNOTKA)
            .ratioDoHlavniJednotky(DEFAULT_RATIO_DO_HLAVNI_JEDNOTKY)
            .nazevJednotky(DEFAULT_NAZEV_JEDNOTKY);
        return jednotky;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Jednotky createUpdatedEntity(EntityManager em) {
        Jednotky jednotky = new Jednotky()
            .hlavniJednotka(UPDATED_HLAVNI_JEDNOTKA)
            .ratioDoHlavniJednotky(UPDATED_RATIO_DO_HLAVNI_JEDNOTKY)
            .nazevJednotky(UPDATED_NAZEV_JEDNOTKY);
        return jednotky;
    }

    @BeforeEach
    public void initTest() {
        jednotky = createEntity(em);
    }

    @Test
    @Transactional
    public void createJednotky() throws Exception {
        int databaseSizeBeforeCreate = jednotkyRepository.findAll().size();

        // Create the Jednotky
        restJednotkyMockMvc.perform(post("/api/jednotkies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(jednotky)))
            .andExpect(status().isCreated());

        // Validate the Jednotky in the database
        List<Jednotky> jednotkyList = jednotkyRepository.findAll();
        assertThat(jednotkyList).hasSize(databaseSizeBeforeCreate + 1);
        Jednotky testJednotky = jednotkyList.get(jednotkyList.size() - 1);
        assertThat(testJednotky.getHlavniJednotka()).isEqualTo(DEFAULT_HLAVNI_JEDNOTKA);
        assertThat(testJednotky.getRatioDoHlavniJednotky()).isEqualTo(DEFAULT_RATIO_DO_HLAVNI_JEDNOTKY);
        assertThat(testJednotky.getNazevJednotky()).isEqualTo(DEFAULT_NAZEV_JEDNOTKY);
    }

    @Test
    @Transactional
    public void createJednotkyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = jednotkyRepository.findAll().size();

        // Create the Jednotky with an existing ID
        jednotky.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJednotkyMockMvc.perform(post("/api/jednotkies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(jednotky)))
            .andExpect(status().isBadRequest());

        // Validate the Jednotky in the database
        List<Jednotky> jednotkyList = jednotkyRepository.findAll();
        assertThat(jednotkyList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllJednotkies() throws Exception {
        // Initialize the database
        jednotkyRepository.saveAndFlush(jednotky);

        // Get all the jednotkyList
        restJednotkyMockMvc.perform(get("/api/jednotkies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(jednotky.getId().intValue())))
            .andExpect(jsonPath("$.[*].hlavniJednotka").value(hasItem(DEFAULT_HLAVNI_JEDNOTKA.toString())))
            .andExpect(jsonPath("$.[*].ratioDoHlavniJednotky").value(hasItem(DEFAULT_RATIO_DO_HLAVNI_JEDNOTKY.doubleValue())))
            .andExpect(jsonPath("$.[*].nazevJednotky").value(hasItem(DEFAULT_NAZEV_JEDNOTKY)));
    }
    
    @Test
    @Transactional
    public void getJednotky() throws Exception {
        // Initialize the database
        jednotkyRepository.saveAndFlush(jednotky);

        // Get the jednotky
        restJednotkyMockMvc.perform(get("/api/jednotkies/{id}", jednotky.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(jednotky.getId().intValue()))
            .andExpect(jsonPath("$.hlavniJednotka").value(DEFAULT_HLAVNI_JEDNOTKA.toString()))
            .andExpect(jsonPath("$.ratioDoHlavniJednotky").value(DEFAULT_RATIO_DO_HLAVNI_JEDNOTKY.doubleValue()))
            .andExpect(jsonPath("$.nazevJednotky").value(DEFAULT_NAZEV_JEDNOTKY));
    }

    @Test
    @Transactional
    public void getNonExistingJednotky() throws Exception {
        // Get the jednotky
        restJednotkyMockMvc.perform(get("/api/jednotkies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJednotky() throws Exception {
        // Initialize the database
        jednotkyRepository.saveAndFlush(jednotky);

        int databaseSizeBeforeUpdate = jednotkyRepository.findAll().size();

        // Update the jednotky
        Jednotky updatedJednotky = jednotkyRepository.findById(jednotky.getId()).get();
        // Disconnect from session so that the updates on updatedJednotky are not directly saved in db
        em.detach(updatedJednotky);
        updatedJednotky
            .hlavniJednotka(UPDATED_HLAVNI_JEDNOTKA)
            .ratioDoHlavniJednotky(UPDATED_RATIO_DO_HLAVNI_JEDNOTKY)
            .nazevJednotky(UPDATED_NAZEV_JEDNOTKY);

        restJednotkyMockMvc.perform(put("/api/jednotkies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedJednotky)))
            .andExpect(status().isOk());

        // Validate the Jednotky in the database
        List<Jednotky> jednotkyList = jednotkyRepository.findAll();
        assertThat(jednotkyList).hasSize(databaseSizeBeforeUpdate);
        Jednotky testJednotky = jednotkyList.get(jednotkyList.size() - 1);
        assertThat(testJednotky.getHlavniJednotka()).isEqualTo(UPDATED_HLAVNI_JEDNOTKA);
        assertThat(testJednotky.getRatioDoHlavniJednotky()).isEqualTo(UPDATED_RATIO_DO_HLAVNI_JEDNOTKY);
        assertThat(testJednotky.getNazevJednotky()).isEqualTo(UPDATED_NAZEV_JEDNOTKY);
    }

    @Test
    @Transactional
    public void updateNonExistingJednotky() throws Exception {
        int databaseSizeBeforeUpdate = jednotkyRepository.findAll().size();

        // Create the Jednotky

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restJednotkyMockMvc.perform(put("/api/jednotkies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(jednotky)))
            .andExpect(status().isBadRequest());

        // Validate the Jednotky in the database
        List<Jednotky> jednotkyList = jednotkyRepository.findAll();
        assertThat(jednotkyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteJednotky() throws Exception {
        // Initialize the database
        jednotkyRepository.saveAndFlush(jednotky);

        int databaseSizeBeforeDelete = jednotkyRepository.findAll().size();

        // Delete the jednotky
        restJednotkyMockMvc.perform(delete("/api/jednotkies/{id}", jednotky.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Jednotky> jednotkyList = jednotkyRepository.findAll();
        assertThat(jednotkyList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
