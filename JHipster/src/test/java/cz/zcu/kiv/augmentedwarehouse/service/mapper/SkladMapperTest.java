package cz.zcu.kiv.augmentedwarehouse.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class SkladMapperTest {

    private SkladMapper skladMapper;

    @BeforeEach
    public void setUp() {
        skladMapper = new SkladMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(skladMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(skladMapper.fromId(null)).isNull();
    }
}
