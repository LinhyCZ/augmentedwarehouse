package cz.zcu.kiv.augmentedwarehouse.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import cz.zcu.kiv.augmentedwarehouse.web.rest.TestUtil;

public class BaleniTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Baleni.class);
        Baleni baleni1 = new Baleni();
        baleni1.setId(1L);
        Baleni baleni2 = new Baleni();
        baleni2.setId(baleni1.getId());
        assertThat(baleni1).isEqualTo(baleni2);
        baleni2.setId(2L);
        assertThat(baleni1).isNotEqualTo(baleni2);
        baleni1.setId(null);
        assertThat(baleni1).isNotEqualTo(baleni2);
    }
}
