package cz.zcu.kiv.augmentedwarehouse.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import cz.zcu.kiv.augmentedwarehouse.web.rest.TestUtil;

public class PoziceDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PoziceDTO.class);
        PoziceDTO poziceDTO1 = new PoziceDTO();
        poziceDTO1.setId(1L);
        PoziceDTO poziceDTO2 = new PoziceDTO();
        assertThat(poziceDTO1).isNotEqualTo(poziceDTO2);
        poziceDTO2.setId(poziceDTO1.getId());
        assertThat(poziceDTO1).isEqualTo(poziceDTO2);
        poziceDTO2.setId(2L);
        assertThat(poziceDTO1).isNotEqualTo(poziceDTO2);
        poziceDTO1.setId(null);
        assertThat(poziceDTO1).isNotEqualTo(poziceDTO2);
    }
}
