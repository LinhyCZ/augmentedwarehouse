package cz.zcu.kiv.augmentedwarehouse.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import cz.zcu.kiv.augmentedwarehouse.web.rest.TestUtil;

public class ZboziTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Zbozi.class);
        Zbozi zbozi1 = new Zbozi();
        zbozi1.setId(1L);
        Zbozi zbozi2 = new Zbozi();
        zbozi2.setId(zbozi1.getId());
        assertThat(zbozi1).isEqualTo(zbozi2);
        zbozi2.setId(2L);
        assertThat(zbozi1).isNotEqualTo(zbozi2);
        zbozi1.setId(null);
        assertThat(zbozi1).isNotEqualTo(zbozi2);
    }
}
