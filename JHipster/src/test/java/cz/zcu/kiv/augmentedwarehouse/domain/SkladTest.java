package cz.zcu.kiv.augmentedwarehouse.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import cz.zcu.kiv.augmentedwarehouse.web.rest.TestUtil;

public class SkladTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Sklad.class);
        Sklad sklad1 = new Sklad();
        sklad1.setId(1L);
        Sklad sklad2 = new Sklad();
        sklad2.setId(sklad1.getId());
        assertThat(sklad1).isEqualTo(sklad2);
        sklad2.setId(2L);
        assertThat(sklad1).isNotEqualTo(sklad2);
        sklad1.setId(null);
        assertThat(sklad1).isNotEqualTo(sklad2);
    }
}
