package cz.zcu.kiv.augmentedwarehouse.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class PoliceMapperTest {

    private PoliceMapper policeMapper;

    @BeforeEach
    public void setUp() {
        policeMapper = new PoliceMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(policeMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(policeMapper.fromId(null)).isNull();
    }
}
