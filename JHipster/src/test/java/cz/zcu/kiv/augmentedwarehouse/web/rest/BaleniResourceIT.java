package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.AugmentedWarehouseApp;
import cz.zcu.kiv.augmentedwarehouse.domain.Baleni;
import cz.zcu.kiv.augmentedwarehouse.repository.BaleniRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BaleniResource} REST controller.
 */
@SpringBootTest(classes = AugmentedWarehouseApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class BaleniResourceIT {

    private static final Integer DEFAULT_POCET_KS = 1;
    private static final Integer UPDATED_POCET_KS = 2;

    private static final String DEFAULT_QR_CODE = "AAAAAAAAAA";
    private static final String UPDATED_QR_CODE = "BBBBBBBBBB";

    @Autowired
    private BaleniRepository baleniRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBaleniMockMvc;

    private Baleni baleni;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Baleni createEntity(EntityManager em) {
        Baleni baleni = new Baleni()
            .pocetKS(DEFAULT_POCET_KS)
            .qrCode(DEFAULT_QR_CODE);
        return baleni;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Baleni createUpdatedEntity(EntityManager em) {
        Baleni baleni = new Baleni()
            .pocetKS(UPDATED_POCET_KS)
            .qrCode(UPDATED_QR_CODE);
        return baleni;
    }

    @BeforeEach
    public void initTest() {
        baleni = createEntity(em);
    }

    @Test
    @Transactional
    public void createBaleni() throws Exception {
        int databaseSizeBeforeCreate = baleniRepository.findAll().size();

        // Create the Baleni
        restBaleniMockMvc.perform(post("/api/balenis")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(baleni)))
            .andExpect(status().isCreated());

        // Validate the Baleni in the database
        List<Baleni> baleniList = baleniRepository.findAll();
        assertThat(baleniList).hasSize(databaseSizeBeforeCreate + 1);
        Baleni testBaleni = baleniList.get(baleniList.size() - 1);
        assertThat(testBaleni.getPocetKS()).isEqualTo(DEFAULT_POCET_KS);
        assertThat(testBaleni.getQrCode()).isEqualTo(DEFAULT_QR_CODE);
    }

    @Test
    @Transactional
    public void createBaleniWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = baleniRepository.findAll().size();

        // Create the Baleni with an existing ID
        baleni.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBaleniMockMvc.perform(post("/api/balenis")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(baleni)))
            .andExpect(status().isBadRequest());

        // Validate the Baleni in the database
        List<Baleni> baleniList = baleniRepository.findAll();
        assertThat(baleniList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllBalenis() throws Exception {
        // Initialize the database
        baleniRepository.saveAndFlush(baleni);

        // Get all the baleniList
        restBaleniMockMvc.perform(get("/api/balenis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(baleni.getId().intValue())))
            .andExpect(jsonPath("$.[*].pocetKS").value(hasItem(DEFAULT_POCET_KS)))
            .andExpect(jsonPath("$.[*].qrCode").value(hasItem(DEFAULT_QR_CODE)));
    }
    
    @Test
    @Transactional
    public void getBaleni() throws Exception {
        // Initialize the database
        baleniRepository.saveAndFlush(baleni);

        // Get the baleni
        restBaleniMockMvc.perform(get("/api/balenis/{id}", baleni.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(baleni.getId().intValue()))
            .andExpect(jsonPath("$.pocetKS").value(DEFAULT_POCET_KS))
            .andExpect(jsonPath("$.qrCode").value(DEFAULT_QR_CODE));
    }

    @Test
    @Transactional
    public void getNonExistingBaleni() throws Exception {
        // Get the baleni
        restBaleniMockMvc.perform(get("/api/balenis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBaleni() throws Exception {
        // Initialize the database
        baleniRepository.saveAndFlush(baleni);

        int databaseSizeBeforeUpdate = baleniRepository.findAll().size();

        // Update the baleni
        Baleni updatedBaleni = baleniRepository.findById(baleni.getId()).get();
        // Disconnect from session so that the updates on updatedBaleni are not directly saved in db
        em.detach(updatedBaleni);
        updatedBaleni
            .pocetKS(UPDATED_POCET_KS)
            .qrCode(UPDATED_QR_CODE);

        restBaleniMockMvc.perform(put("/api/balenis")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedBaleni)))
            .andExpect(status().isOk());

        // Validate the Baleni in the database
        List<Baleni> baleniList = baleniRepository.findAll();
        assertThat(baleniList).hasSize(databaseSizeBeforeUpdate);
        Baleni testBaleni = baleniList.get(baleniList.size() - 1);
        assertThat(testBaleni.getPocetKS()).isEqualTo(UPDATED_POCET_KS);
        assertThat(testBaleni.getQrCode()).isEqualTo(UPDATED_QR_CODE);
    }

    @Test
    @Transactional
    public void updateNonExistingBaleni() throws Exception {
        int databaseSizeBeforeUpdate = baleniRepository.findAll().size();

        // Create the Baleni

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBaleniMockMvc.perform(put("/api/balenis")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(baleni)))
            .andExpect(status().isBadRequest());

        // Validate the Baleni in the database
        List<Baleni> baleniList = baleniRepository.findAll();
        assertThat(baleniList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBaleni() throws Exception {
        // Initialize the database
        baleniRepository.saveAndFlush(baleni);

        int databaseSizeBeforeDelete = baleniRepository.findAll().size();

        // Delete the baleni
        restBaleniMockMvc.perform(delete("/api/balenis/{id}", baleni.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Baleni> baleniList = baleniRepository.findAll();
        assertThat(baleniList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
