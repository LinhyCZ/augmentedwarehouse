package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.AugmentedWarehouseApp;
import cz.zcu.kiv.augmentedwarehouse.domain.Stojan;
import cz.zcu.kiv.augmentedwarehouse.repository.StojanRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link StojanResource} REST controller.
 */
@SpringBootTest(classes = AugmentedWarehouseApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class StojanResourceIT {

    @Autowired
    private StojanRepository stojanRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restStojanMockMvc;

    private Stojan stojan;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Stojan createEntity(EntityManager em) {
        Stojan stojan = new Stojan();
        return stojan;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Stojan createUpdatedEntity(EntityManager em) {
        Stojan stojan = new Stojan();
        return stojan;
    }

    @BeforeEach
    public void initTest() {
        stojan = createEntity(em);
    }

    @Test
    @Transactional
    public void createStojan() throws Exception {
        int databaseSizeBeforeCreate = stojanRepository.findAll().size();

        // Create the Stojan
        restStojanMockMvc.perform(post("/api/stojans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(stojan)))
            .andExpect(status().isCreated());

        // Validate the Stojan in the database
        List<Stojan> stojanList = stojanRepository.findAll();
        assertThat(stojanList).hasSize(databaseSizeBeforeCreate + 1);
        Stojan testStojan = stojanList.get(stojanList.size() - 1);
    }

    @Test
    @Transactional
    public void createStojanWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = stojanRepository.findAll().size();

        // Create the Stojan with an existing ID
        stojan.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restStojanMockMvc.perform(post("/api/stojans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(stojan)))
            .andExpect(status().isBadRequest());

        // Validate the Stojan in the database
        List<Stojan> stojanList = stojanRepository.findAll();
        assertThat(stojanList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllStojans() throws Exception {
        // Initialize the database
        stojanRepository.saveAndFlush(stojan);

        // Get all the stojanList
        restStojanMockMvc.perform(get("/api/stojans?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(stojan.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getStojan() throws Exception {
        // Initialize the database
        stojanRepository.saveAndFlush(stojan);

        // Get the stojan
        restStojanMockMvc.perform(get("/api/stojans/{id}", stojan.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(stojan.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingStojan() throws Exception {
        // Get the stojan
        restStojanMockMvc.perform(get("/api/stojans/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStojan() throws Exception {
        // Initialize the database
        stojanRepository.saveAndFlush(stojan);

        int databaseSizeBeforeUpdate = stojanRepository.findAll().size();

        // Update the stojan
        Stojan updatedStojan = stojanRepository.findById(stojan.getId()).get();
        // Disconnect from session so that the updates on updatedStojan are not directly saved in db
        em.detach(updatedStojan);

        restStojanMockMvc.perform(put("/api/stojans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedStojan)))
            .andExpect(status().isOk());

        // Validate the Stojan in the database
        List<Stojan> stojanList = stojanRepository.findAll();
        assertThat(stojanList).hasSize(databaseSizeBeforeUpdate);
        Stojan testStojan = stojanList.get(stojanList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingStojan() throws Exception {
        int databaseSizeBeforeUpdate = stojanRepository.findAll().size();

        // Create the Stojan

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restStojanMockMvc.perform(put("/api/stojans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(stojan)))
            .andExpect(status().isBadRequest());

        // Validate the Stojan in the database
        List<Stojan> stojanList = stojanRepository.findAll();
        assertThat(stojanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteStojan() throws Exception {
        // Initialize the database
        stojanRepository.saveAndFlush(stojan);

        int databaseSizeBeforeDelete = stojanRepository.findAll().size();

        // Delete the stojan
        restStojanMockMvc.perform(delete("/api/stojans/{id}", stojan.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Stojan> stojanList = stojanRepository.findAll();
        assertThat(stojanList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
