package cz.zcu.kiv.augmentedwarehouse.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import cz.zcu.kiv.augmentedwarehouse.web.rest.TestUtil;

public class SloupecDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SloupecDTO.class);
        SloupecDTO sloupecDTO1 = new SloupecDTO();
        sloupecDTO1.setId(1L);
        SloupecDTO sloupecDTO2 = new SloupecDTO();
        assertThat(sloupecDTO1).isNotEqualTo(sloupecDTO2);
        sloupecDTO2.setId(sloupecDTO1.getId());
        assertThat(sloupecDTO1).isEqualTo(sloupecDTO2);
        sloupecDTO2.setId(2L);
        assertThat(sloupecDTO1).isNotEqualTo(sloupecDTO2);
        sloupecDTO1.setId(null);
        assertThat(sloupecDTO1).isNotEqualTo(sloupecDTO2);
    }
}
