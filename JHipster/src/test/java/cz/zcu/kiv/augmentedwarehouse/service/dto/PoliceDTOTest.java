package cz.zcu.kiv.augmentedwarehouse.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import cz.zcu.kiv.augmentedwarehouse.web.rest.TestUtil;

public class PoliceDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PoliceDTO.class);
        PoliceDTO policeDTO1 = new PoliceDTO();
        policeDTO1.setId(1L);
        PoliceDTO policeDTO2 = new PoliceDTO();
        assertThat(policeDTO1).isNotEqualTo(policeDTO2);
        policeDTO2.setId(policeDTO1.getId());
        assertThat(policeDTO1).isEqualTo(policeDTO2);
        policeDTO2.setId(2L);
        assertThat(policeDTO1).isNotEqualTo(policeDTO2);
        policeDTO1.setId(null);
        assertThat(policeDTO1).isNotEqualTo(policeDTO2);
    }
}
