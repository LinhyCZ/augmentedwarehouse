package cz.zcu.kiv.augmentedwarehouse.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class SloupecMapperTest {

    private SloupecMapper sloupecMapper;

    @BeforeEach
    public void setUp() {
        sloupecMapper = new SloupecMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(sloupecMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(sloupecMapper.fromId(null)).isNull();
    }
}
