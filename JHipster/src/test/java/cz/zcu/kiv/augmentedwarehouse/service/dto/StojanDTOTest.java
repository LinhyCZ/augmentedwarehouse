package cz.zcu.kiv.augmentedwarehouse.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import cz.zcu.kiv.augmentedwarehouse.web.rest.TestUtil;

public class StojanDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(StojanDTO.class);
        StojanDTO stojanDTO1 = new StojanDTO();
        stojanDTO1.setId(1L);
        StojanDTO stojanDTO2 = new StojanDTO();
        assertThat(stojanDTO1).isNotEqualTo(stojanDTO2);
        stojanDTO2.setId(stojanDTO1.getId());
        assertThat(stojanDTO1).isEqualTo(stojanDTO2);
        stojanDTO2.setId(2L);
        assertThat(stojanDTO1).isNotEqualTo(stojanDTO2);
        stojanDTO1.setId(null);
        assertThat(stojanDTO1).isNotEqualTo(stojanDTO2);
    }
}
