package cz.zcu.kiv.augmentedwarehouse.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ZboziMapperTest {

    private ZboziMapper zboziMapper;

    @BeforeEach
    public void setUp() {
        zboziMapper = new ZboziMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(zboziMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(zboziMapper.fromId(null)).isNull();
    }
}
