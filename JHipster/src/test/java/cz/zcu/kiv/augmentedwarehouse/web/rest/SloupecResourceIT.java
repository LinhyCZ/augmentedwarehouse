package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.AugmentedWarehouseApp;
import cz.zcu.kiv.augmentedwarehouse.domain.Sloupec;
import cz.zcu.kiv.augmentedwarehouse.repository.SloupecRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SloupecResource} REST controller.
 */
@SpringBootTest(classes = AugmentedWarehouseApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class SloupecResourceIT {

    @Autowired
    private SloupecRepository sloupecRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSloupecMockMvc;

    private Sloupec sloupec;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sloupec createEntity(EntityManager em) {
        Sloupec sloupec = new Sloupec();
        return sloupec;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sloupec createUpdatedEntity(EntityManager em) {
        Sloupec sloupec = new Sloupec();
        return sloupec;
    }

    @BeforeEach
    public void initTest() {
        sloupec = createEntity(em);
    }

    @Test
    @Transactional
    public void createSloupec() throws Exception {
        int databaseSizeBeforeCreate = sloupecRepository.findAll().size();

        // Create the Sloupec
        restSloupecMockMvc.perform(post("/api/sloupecs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(sloupec)))
            .andExpect(status().isCreated());

        // Validate the Sloupec in the database
        List<Sloupec> sloupecList = sloupecRepository.findAll();
        assertThat(sloupecList).hasSize(databaseSizeBeforeCreate + 1);
        Sloupec testSloupec = sloupecList.get(sloupecList.size() - 1);
    }

    @Test
    @Transactional
    public void createSloupecWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sloupecRepository.findAll().size();

        // Create the Sloupec with an existing ID
        sloupec.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSloupecMockMvc.perform(post("/api/sloupecs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(sloupec)))
            .andExpect(status().isBadRequest());

        // Validate the Sloupec in the database
        List<Sloupec> sloupecList = sloupecRepository.findAll();
        assertThat(sloupecList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllSloupecs() throws Exception {
        // Initialize the database
        sloupecRepository.saveAndFlush(sloupec);

        // Get all the sloupecList
        restSloupecMockMvc.perform(get("/api/sloupecs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sloupec.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getSloupec() throws Exception {
        // Initialize the database
        sloupecRepository.saveAndFlush(sloupec);

        // Get the sloupec
        restSloupecMockMvc.perform(get("/api/sloupecs/{id}", sloupec.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(sloupec.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSloupec() throws Exception {
        // Get the sloupec
        restSloupecMockMvc.perform(get("/api/sloupecs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSloupec() throws Exception {
        // Initialize the database
        sloupecRepository.saveAndFlush(sloupec);

        int databaseSizeBeforeUpdate = sloupecRepository.findAll().size();

        // Update the sloupec
        Sloupec updatedSloupec = sloupecRepository.findById(sloupec.getId()).get();
        // Disconnect from session so that the updates on updatedSloupec are not directly saved in db
        em.detach(updatedSloupec);

        restSloupecMockMvc.perform(put("/api/sloupecs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedSloupec)))
            .andExpect(status().isOk());

        // Validate the Sloupec in the database
        List<Sloupec> sloupecList = sloupecRepository.findAll();
        assertThat(sloupecList).hasSize(databaseSizeBeforeUpdate);
        Sloupec testSloupec = sloupecList.get(sloupecList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingSloupec() throws Exception {
        int databaseSizeBeforeUpdate = sloupecRepository.findAll().size();

        // Create the Sloupec

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSloupecMockMvc.perform(put("/api/sloupecs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(sloupec)))
            .andExpect(status().isBadRequest());

        // Validate the Sloupec in the database
        List<Sloupec> sloupecList = sloupecRepository.findAll();
        assertThat(sloupecList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSloupec() throws Exception {
        // Initialize the database
        sloupecRepository.saveAndFlush(sloupec);

        int databaseSizeBeforeDelete = sloupecRepository.findAll().size();

        // Delete the sloupec
        restSloupecMockMvc.perform(delete("/api/sloupecs/{id}", sloupec.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Sloupec> sloupecList = sloupecRepository.findAll();
        assertThat(sloupecList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
