package cz.zcu.kiv.augmentedwarehouse.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class PoziceMapperTest {

    private PoziceMapper poziceMapper;

    @BeforeEach
    public void setUp() {
        poziceMapper = new PoziceMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(poziceMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(poziceMapper.fromId(null)).isNull();
    }
}
