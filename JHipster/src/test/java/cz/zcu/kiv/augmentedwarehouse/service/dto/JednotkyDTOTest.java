package cz.zcu.kiv.augmentedwarehouse.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import cz.zcu.kiv.augmentedwarehouse.web.rest.TestUtil;

public class JednotkyDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(JednotkyDTO.class);
        JednotkyDTO jednotkyDTO1 = new JednotkyDTO();
        jednotkyDTO1.setId(1L);
        JednotkyDTO jednotkyDTO2 = new JednotkyDTO();
        assertThat(jednotkyDTO1).isNotEqualTo(jednotkyDTO2);
        jednotkyDTO2.setId(jednotkyDTO1.getId());
        assertThat(jednotkyDTO1).isEqualTo(jednotkyDTO2);
        jednotkyDTO2.setId(2L);
        assertThat(jednotkyDTO1).isNotEqualTo(jednotkyDTO2);
        jednotkyDTO1.setId(null);
        assertThat(jednotkyDTO1).isNotEqualTo(jednotkyDTO2);
    }
}
