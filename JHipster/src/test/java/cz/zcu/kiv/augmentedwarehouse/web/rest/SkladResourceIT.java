package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.AugmentedWarehouseApp;
import cz.zcu.kiv.augmentedwarehouse.domain.Sklad;
import cz.zcu.kiv.augmentedwarehouse.repository.SkladRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SkladResource} REST controller.
 */
@SpringBootTest(classes = AugmentedWarehouseApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class SkladResourceIT {

    private static final String DEFAULT_ADRESA = "AAAAAAAAAA";
    private static final String UPDATED_ADRESA = "BBBBBBBBBB";

    private static final Integer DEFAULT_CISLO_POPISNE = 1;
    private static final Integer UPDATED_CISLO_POPISNE = 2;

    private static final Integer DEFAULT_CISLO_ORIENTACNI = 1;
    private static final Integer UPDATED_CISLO_ORIENTACNI = 2;

    private static final String DEFAULT_MESTO = "AAAAAAAAAA";
    private static final String UPDATED_MESTO = "BBBBBBBBBB";

    private static final Integer DEFAULT_P_SC = 10000;
    private static final Integer UPDATED_P_SC = 10001;

    @Autowired
    private SkladRepository skladRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSkladMockMvc;

    private Sklad sklad;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sklad createEntity(EntityManager em) {
        Sklad sklad = new Sklad()
            .adresa(DEFAULT_ADRESA)
            .cisloPopisne(DEFAULT_CISLO_POPISNE)
            .cisloOrientacni(DEFAULT_CISLO_ORIENTACNI)
            .mesto(DEFAULT_MESTO)
            .pSC(DEFAULT_P_SC);
        return sklad;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sklad createUpdatedEntity(EntityManager em) {
        Sklad sklad = new Sklad()
            .adresa(UPDATED_ADRESA)
            .cisloPopisne(UPDATED_CISLO_POPISNE)
            .cisloOrientacni(UPDATED_CISLO_ORIENTACNI)
            .mesto(UPDATED_MESTO)
            .pSC(UPDATED_P_SC);
        return sklad;
    }

    @BeforeEach
    public void initTest() {
        sklad = createEntity(em);
    }

    @Test
    @Transactional
    public void createSklad() throws Exception {
        int databaseSizeBeforeCreate = skladRepository.findAll().size();

        // Create the Sklad
        restSkladMockMvc.perform(post("/api/sklads")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(sklad)))
            .andExpect(status().isCreated());

        // Validate the Sklad in the database
        List<Sklad> skladList = skladRepository.findAll();
        assertThat(skladList).hasSize(databaseSizeBeforeCreate + 1);
        Sklad testSklad = skladList.get(skladList.size() - 1);
        assertThat(testSklad.getAdresa()).isEqualTo(DEFAULT_ADRESA);
        assertThat(testSklad.getCisloPopisne()).isEqualTo(DEFAULT_CISLO_POPISNE);
        assertThat(testSklad.getCisloOrientacni()).isEqualTo(DEFAULT_CISLO_ORIENTACNI);
        assertThat(testSklad.getMesto()).isEqualTo(DEFAULT_MESTO);
        assertThat(testSklad.getpSC()).isEqualTo(DEFAULT_P_SC);
    }

    @Test
    @Transactional
    public void createSkladWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = skladRepository.findAll().size();

        // Create the Sklad with an existing ID
        sklad.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSkladMockMvc.perform(post("/api/sklads")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(sklad)))
            .andExpect(status().isBadRequest());

        // Validate the Sklad in the database
        List<Sklad> skladList = skladRepository.findAll();
        assertThat(skladList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllSklads() throws Exception {
        // Initialize the database
        skladRepository.saveAndFlush(sklad);

        // Get all the skladList
        restSkladMockMvc.perform(get("/api/sklads?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sklad.getId().intValue())))
            .andExpect(jsonPath("$.[*].adresa").value(hasItem(DEFAULT_ADRESA)))
            .andExpect(jsonPath("$.[*].cisloPopisne").value(hasItem(DEFAULT_CISLO_POPISNE)))
            .andExpect(jsonPath("$.[*].cisloOrientacni").value(hasItem(DEFAULT_CISLO_ORIENTACNI)))
            .andExpect(jsonPath("$.[*].mesto").value(hasItem(DEFAULT_MESTO)))
            .andExpect(jsonPath("$.[*].pSC").value(hasItem(DEFAULT_P_SC)));
    }
    
    @Test
    @Transactional
    public void getSklad() throws Exception {
        // Initialize the database
        skladRepository.saveAndFlush(sklad);

        // Get the sklad
        restSkladMockMvc.perform(get("/api/sklads/{id}", sklad.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(sklad.getId().intValue()))
            .andExpect(jsonPath("$.adresa").value(DEFAULT_ADRESA))
            .andExpect(jsonPath("$.cisloPopisne").value(DEFAULT_CISLO_POPISNE))
            .andExpect(jsonPath("$.cisloOrientacni").value(DEFAULT_CISLO_ORIENTACNI))
            .andExpect(jsonPath("$.mesto").value(DEFAULT_MESTO))
            .andExpect(jsonPath("$.pSC").value(DEFAULT_P_SC));
    }

    @Test
    @Transactional
    public void getNonExistingSklad() throws Exception {
        // Get the sklad
        restSkladMockMvc.perform(get("/api/sklads/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSklad() throws Exception {
        // Initialize the database
        skladRepository.saveAndFlush(sklad);

        int databaseSizeBeforeUpdate = skladRepository.findAll().size();

        // Update the sklad
        Sklad updatedSklad = skladRepository.findById(sklad.getId()).get();
        // Disconnect from session so that the updates on updatedSklad are not directly saved in db
        em.detach(updatedSklad);
        updatedSklad
            .adresa(UPDATED_ADRESA)
            .cisloPopisne(UPDATED_CISLO_POPISNE)
            .cisloOrientacni(UPDATED_CISLO_ORIENTACNI)
            .mesto(UPDATED_MESTO)
            .pSC(UPDATED_P_SC);

        restSkladMockMvc.perform(put("/api/sklads")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedSklad)))
            .andExpect(status().isOk());

        // Validate the Sklad in the database
        List<Sklad> skladList = skladRepository.findAll();
        assertThat(skladList).hasSize(databaseSizeBeforeUpdate);
        Sklad testSklad = skladList.get(skladList.size() - 1);
        assertThat(testSklad.getAdresa()).isEqualTo(UPDATED_ADRESA);
        assertThat(testSklad.getCisloPopisne()).isEqualTo(UPDATED_CISLO_POPISNE);
        assertThat(testSklad.getCisloOrientacni()).isEqualTo(UPDATED_CISLO_ORIENTACNI);
        assertThat(testSklad.getMesto()).isEqualTo(UPDATED_MESTO);
        assertThat(testSklad.getpSC()).isEqualTo(UPDATED_P_SC);
    }

    @Test
    @Transactional
    public void updateNonExistingSklad() throws Exception {
        int databaseSizeBeforeUpdate = skladRepository.findAll().size();

        // Create the Sklad

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSkladMockMvc.perform(put("/api/sklads")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(sklad)))
            .andExpect(status().isBadRequest());

        // Validate the Sklad in the database
        List<Sklad> skladList = skladRepository.findAll();
        assertThat(skladList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSklad() throws Exception {
        // Initialize the database
        skladRepository.saveAndFlush(sklad);

        int databaseSizeBeforeDelete = skladRepository.findAll().size();

        // Delete the sklad
        restSkladMockMvc.perform(delete("/api/sklads/{id}", sklad.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Sklad> skladList = skladRepository.findAll();
        assertThat(skladList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
