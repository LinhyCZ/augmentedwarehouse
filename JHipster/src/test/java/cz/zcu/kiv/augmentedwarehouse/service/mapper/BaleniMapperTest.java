package cz.zcu.kiv.augmentedwarehouse.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class BaleniMapperTest {

    private BaleniMapper baleniMapper;

    @BeforeEach
    public void setUp() {
        baleniMapper = new BaleniMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(baleniMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(baleniMapper.fromId(null)).isNull();
    }
}
