package cz.zcu.kiv.augmentedwarehouse.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import cz.zcu.kiv.augmentedwarehouse.web.rest.TestUtil;

public class PoziceTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Pozice.class);
        Pozice pozice1 = new Pozice();
        pozice1.setId(1L);
        Pozice pozice2 = new Pozice();
        pozice2.setId(pozice1.getId());
        assertThat(pozice1).isEqualTo(pozice2);
        pozice2.setId(2L);
        assertThat(pozice1).isNotEqualTo(pozice2);
        pozice1.setId(null);
        assertThat(pozice1).isNotEqualTo(pozice2);
    }
}
