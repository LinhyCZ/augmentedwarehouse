package cz.zcu.kiv.augmentedwarehouse.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import cz.zcu.kiv.augmentedwarehouse.web.rest.TestUtil;

public class JednotkyTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Jednotky.class);
        Jednotky jednotky1 = new Jednotky();
        jednotky1.setId(1L);
        Jednotky jednotky2 = new Jednotky();
        jednotky2.setId(jednotky1.getId());
        assertThat(jednotky1).isEqualTo(jednotky2);
        jednotky2.setId(2L);
        assertThat(jednotky1).isNotEqualTo(jednotky2);
        jednotky1.setId(null);
        assertThat(jednotky1).isNotEqualTo(jednotky2);
    }
}
