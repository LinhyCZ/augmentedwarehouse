package cz.zcu.kiv.augmentedwarehouse.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import cz.zcu.kiv.augmentedwarehouse.web.rest.TestUtil;

public class PoliceTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Police.class);
        Police police1 = new Police();
        police1.setId(1L);
        Police police2 = new Police();
        police2.setId(police1.getId());
        assertThat(police1).isEqualTo(police2);
        police2.setId(2L);
        assertThat(police1).isNotEqualTo(police2);
        police1.setId(null);
        assertThat(police1).isNotEqualTo(police2);
    }
}
