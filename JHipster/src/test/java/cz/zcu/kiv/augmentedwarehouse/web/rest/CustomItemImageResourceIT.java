package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.AugmentedWarehouseApp;
import cz.zcu.kiv.augmentedwarehouse.domain.Zbozi;
import cz.zcu.kiv.augmentedwarehouse.repository.ZboziRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest(classes = AugmentedWarehouseApp.class)
@AutoConfigureMockMvc

public class CustomItemImageResourceIT {
    @Autowired
    private MockMvc restMockMvc;

    @Autowired
    private ZboziRepository zboziRepository;

    //@Test
    public void getImage() throws Exception {
        Zbozi zbozi = new Zbozi();
        zbozi.setObrazek("Encoded string");
        zboziRepository.saveAndFlush(zbozi);

        restMockMvc.perform(get("/custom-api/image/{id}", zbozi.getId()))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.obrazek").value(zbozi.getObrazek()));
    }
}
