package cz.zcu.kiv.augmentedwarehouse.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TypKSMapperTest {

    private TypKSMapper typKSMapper;

    @BeforeEach
    public void setUp() {
        typKSMapper = new TypKSMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(typKSMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(typKSMapper.fromId(null)).isNull();
    }
}
