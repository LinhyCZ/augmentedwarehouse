package cz.zcu.kiv.augmentedwarehouse.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import cz.zcu.kiv.augmentedwarehouse.web.rest.TestUtil;

public class SloupecTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Sloupec.class);
        Sloupec sloupec1 = new Sloupec();
        sloupec1.setId(1L);
        Sloupec sloupec2 = new Sloupec();
        sloupec2.setId(sloupec1.getId());
        assertThat(sloupec1).isEqualTo(sloupec2);
        sloupec2.setId(2L);
        assertThat(sloupec1).isNotEqualTo(sloupec2);
        sloupec1.setId(null);
        assertThat(sloupec1).isNotEqualTo(sloupec2);
    }
}
