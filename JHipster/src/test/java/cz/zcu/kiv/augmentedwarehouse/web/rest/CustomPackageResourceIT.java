package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.AugmentedWarehouseApp;
import cz.zcu.kiv.augmentedwarehouse.domain.Baleni;
import cz.zcu.kiv.augmentedwarehouse.domain.TypKS;
import cz.zcu.kiv.augmentedwarehouse.domain.Zbozi;
import cz.zcu.kiv.augmentedwarehouse.repository.BaleniRepository;
import cz.zcu.kiv.augmentedwarehouse.repository.TypKSRepository;
import cz.zcu.kiv.augmentedwarehouse.repository.ZboziRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest(classes = AugmentedWarehouseApp.class)
@AutoConfigureMockMvc

public class CustomPackageResourceIT {
    @Autowired
    private MockMvc restMockMvc;

    @Autowired
    private BaleniRepository baleniRepository;

    @Autowired
    private TypKSRepository typKSRepository;

    @Autowired
    private ZboziRepository zboziRepository;

    @Test
    public void getBaleni() throws Exception {
        TypKS typKS = new TypKS();
        Zbozi zbozi = new Zbozi();
        Baleni baleni = new Baleni();

        baleni.setPocetKS(2);
        baleni.setQrCode("ADSADS");
        baleni.setTypKS(typKS);
        baleni.setZbozi(zbozi);

        typKSRepository.saveAndFlush(typKS);
        zboziRepository.saveAndFlush(zbozi);
        baleniRepository.saveAndFlush(baleni);

        restMockMvc.perform(get("/custom-api/balenis/{id}", baleni.getId()))
            .andDo(print())
            .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.pocet_ks").value(baleni.getPocetKS()))
            .andExpect(jsonPath("$.qr_code").value(baleni.getQrCode()))
            .andExpect(jsonPath("$.zbozi_id").value(zbozi.getId()))
            .andExpect(jsonPath("$.typks_id").value(typKS.getId()));
    }
}
