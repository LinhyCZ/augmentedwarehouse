package cz.zcu.kiv.augmentedwarehouse.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import cz.zcu.kiv.augmentedwarehouse.web.rest.TestUtil;

public class TypKSTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TypKS.class);
        TypKS typKS1 = new TypKS();
        typKS1.setId(1L);
        TypKS typKS2 = new TypKS();
        typKS2.setId(typKS1.getId());
        assertThat(typKS1).isEqualTo(typKS2);
        typKS2.setId(2L);
        assertThat(typKS1).isNotEqualTo(typKS2);
        typKS1.setId(null);
        assertThat(typKS1).isNotEqualTo(typKS2);
    }
}
