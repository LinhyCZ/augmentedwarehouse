package cz.zcu.kiv.augmentedwarehouse.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import cz.zcu.kiv.augmentedwarehouse.web.rest.TestUtil;

public class ZboziDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ZboziDTO.class);
        ZboziDTO zboziDTO1 = new ZboziDTO();
        zboziDTO1.setId(1L);
        ZboziDTO zboziDTO2 = new ZboziDTO();
        assertThat(zboziDTO1).isNotEqualTo(zboziDTO2);
        zboziDTO2.setId(zboziDTO1.getId());
        assertThat(zboziDTO1).isEqualTo(zboziDTO2);
        zboziDTO2.setId(2L);
        assertThat(zboziDTO1).isNotEqualTo(zboziDTO2);
        zboziDTO1.setId(null);
        assertThat(zboziDTO1).isNotEqualTo(zboziDTO2);
    }
}
