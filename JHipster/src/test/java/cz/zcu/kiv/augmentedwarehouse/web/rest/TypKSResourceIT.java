package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.AugmentedWarehouseApp;
import cz.zcu.kiv.augmentedwarehouse.domain.TypKS;
import cz.zcu.kiv.augmentedwarehouse.repository.TypKSRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TypKSResource} REST controller.
 */
@SpringBootTest(classes = AugmentedWarehouseApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class TypKSResourceIT {

    private static final Integer DEFAULT_POCET_JEDNOTEK_VKS = 1;
    private static final Integer UPDATED_POCET_JEDNOTEK_VKS = 2;

    private static final Integer DEFAULT_CENA_KS = 1;
    private static final Integer UPDATED_CENA_KS = 2;

    @Autowired
    private TypKSRepository typKSRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTypKSMockMvc;

    private TypKS typKS;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TypKS createEntity(EntityManager em) {
        TypKS typKS = new TypKS()
            .pocetJednotekVKS(DEFAULT_POCET_JEDNOTEK_VKS)
            .cenaKS(DEFAULT_CENA_KS);
        return typKS;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TypKS createUpdatedEntity(EntityManager em) {
        TypKS typKS = new TypKS()
            .pocetJednotekVKS(UPDATED_POCET_JEDNOTEK_VKS)
            .cenaKS(UPDATED_CENA_KS);
        return typKS;
    }

    @BeforeEach
    public void initTest() {
        typKS = createEntity(em);
    }

    @Test
    @Transactional
    public void createTypKS() throws Exception {
        int databaseSizeBeforeCreate = typKSRepository.findAll().size();

        // Create the TypKS
        restTypKSMockMvc.perform(post("/api/typ-ks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(typKS)))
            .andExpect(status().isCreated());

        // Validate the TypKS in the database
        List<TypKS> typKSList = typKSRepository.findAll();
        assertThat(typKSList).hasSize(databaseSizeBeforeCreate + 1);
        TypKS testTypKS = typKSList.get(typKSList.size() - 1);
        assertThat(testTypKS.getPocetJednotekVKS()).isEqualTo(DEFAULT_POCET_JEDNOTEK_VKS);
        assertThat(testTypKS.getCenaKS()).isEqualTo(DEFAULT_CENA_KS);
    }

    @Test
    @Transactional
    public void createTypKSWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = typKSRepository.findAll().size();

        // Create the TypKS with an existing ID
        typKS.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTypKSMockMvc.perform(post("/api/typ-ks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(typKS)))
            .andExpect(status().isBadRequest());

        // Validate the TypKS in the database
        List<TypKS> typKSList = typKSRepository.findAll();
        assertThat(typKSList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTypKS() throws Exception {
        // Initialize the database
        typKSRepository.saveAndFlush(typKS);

        // Get all the typKSList
        restTypKSMockMvc.perform(get("/api/typ-ks?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(typKS.getId().intValue())))
            .andExpect(jsonPath("$.[*].pocetJednotekVKS").value(hasItem(DEFAULT_POCET_JEDNOTEK_VKS)))
            .andExpect(jsonPath("$.[*].cenaKS").value(hasItem(DEFAULT_CENA_KS)));
    }
    
    @Test
    @Transactional
    public void getTypKS() throws Exception {
        // Initialize the database
        typKSRepository.saveAndFlush(typKS);

        // Get the typKS
        restTypKSMockMvc.perform(get("/api/typ-ks/{id}", typKS.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(typKS.getId().intValue()))
            .andExpect(jsonPath("$.pocetJednotekVKS").value(DEFAULT_POCET_JEDNOTEK_VKS))
            .andExpect(jsonPath("$.cenaKS").value(DEFAULT_CENA_KS));
    }

    @Test
    @Transactional
    public void getNonExistingTypKS() throws Exception {
        // Get the typKS
        restTypKSMockMvc.perform(get("/api/typ-ks/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTypKS() throws Exception {
        // Initialize the database
        typKSRepository.saveAndFlush(typKS);

        int databaseSizeBeforeUpdate = typKSRepository.findAll().size();

        // Update the typKS
        TypKS updatedTypKS = typKSRepository.findById(typKS.getId()).get();
        // Disconnect from session so that the updates on updatedTypKS are not directly saved in db
        em.detach(updatedTypKS);
        updatedTypKS
            .pocetJednotekVKS(UPDATED_POCET_JEDNOTEK_VKS)
            .cenaKS(UPDATED_CENA_KS);

        restTypKSMockMvc.perform(put("/api/typ-ks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedTypKS)))
            .andExpect(status().isOk());

        // Validate the TypKS in the database
        List<TypKS> typKSList = typKSRepository.findAll();
        assertThat(typKSList).hasSize(databaseSizeBeforeUpdate);
        TypKS testTypKS = typKSList.get(typKSList.size() - 1);
        assertThat(testTypKS.getPocetJednotekVKS()).isEqualTo(UPDATED_POCET_JEDNOTEK_VKS);
        assertThat(testTypKS.getCenaKS()).isEqualTo(UPDATED_CENA_KS);
    }

    @Test
    @Transactional
    public void updateNonExistingTypKS() throws Exception {
        int databaseSizeBeforeUpdate = typKSRepository.findAll().size();

        // Create the TypKS

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTypKSMockMvc.perform(put("/api/typ-ks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(typKS)))
            .andExpect(status().isBadRequest());

        // Validate the TypKS in the database
        List<TypKS> typKSList = typKSRepository.findAll();
        assertThat(typKSList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTypKS() throws Exception {
        // Initialize the database
        typKSRepository.saveAndFlush(typKS);

        int databaseSizeBeforeDelete = typKSRepository.findAll().size();

        // Delete the typKS
        restTypKSMockMvc.perform(delete("/api/typ-ks/{id}", typKS.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TypKS> typKSList = typKSRepository.findAll();
        assertThat(typKSList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
