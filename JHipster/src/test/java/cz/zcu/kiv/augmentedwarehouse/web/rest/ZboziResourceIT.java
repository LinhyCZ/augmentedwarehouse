package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.AugmentedWarehouseApp;
import cz.zcu.kiv.augmentedwarehouse.domain.Zbozi;
import cz.zcu.kiv.augmentedwarehouse.repository.ZboziRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ZboziResource} REST controller.
 */
@SpringBootTest(classes = AugmentedWarehouseApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class ZboziResourceIT {

    private static final String DEFAULT_KATEGORIE = "AAAAAAAAAA";
    private static final String UPDATED_KATEGORIE = "BBBBBBBBBB";

    private static final String DEFAULT_SLOZENI = "AAAAAAAAAA";
    private static final String UPDATED_SLOZENI = "BBBBBBBBBB";

    private static final String DEFAULT_POPIS_ZBOZI = "AAAAAAAAAA";
    private static final String UPDATED_POPIS_ZBOZI = "BBBBBBBBBB";

    private static final String DEFAULT_EAN = "AAAAAAAAAA";
    private static final String UPDATED_EAN = "BBBBBBBBBB";

    private static final String DEFAULT_OBRAZEK = "AAAAAAAAAA";
    private static final String UPDATED_OBRAZEK = "BBBBBBBBBB";

    @Autowired
    private ZboziRepository zboziRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restZboziMockMvc;

    private Zbozi zbozi;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Zbozi createEntity(EntityManager em) {
        Zbozi zbozi = new Zbozi()
            .kategorie(DEFAULT_KATEGORIE)
            .slozeni(DEFAULT_SLOZENI)
            .popisZbozi(DEFAULT_POPIS_ZBOZI)
            .ean(DEFAULT_EAN)
            .obrazek(DEFAULT_OBRAZEK);
        return zbozi;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Zbozi createUpdatedEntity(EntityManager em) {
        Zbozi zbozi = new Zbozi()
            .kategorie(UPDATED_KATEGORIE)
            .slozeni(UPDATED_SLOZENI)
            .popisZbozi(UPDATED_POPIS_ZBOZI)
            .ean(UPDATED_EAN)
            .obrazek(UPDATED_OBRAZEK);
        return zbozi;
    }

    @BeforeEach
    public void initTest() {
        zbozi = createEntity(em);
    }

    @Test
    @Transactional
    public void createZbozi() throws Exception {
        int databaseSizeBeforeCreate = zboziRepository.findAll().size();

        // Create the Zbozi
        restZboziMockMvc.perform(post("/api/zbozis")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(zbozi)))
            .andExpect(status().isCreated());

        // Validate the Zbozi in the database
        List<Zbozi> zboziList = zboziRepository.findAll();
        assertThat(zboziList).hasSize(databaseSizeBeforeCreate + 1);
        Zbozi testZbozi = zboziList.get(zboziList.size() - 1);
        assertThat(testZbozi.getKategorie()).isEqualTo(DEFAULT_KATEGORIE);
        assertThat(testZbozi.getSlozeni()).isEqualTo(DEFAULT_SLOZENI);
        assertThat(testZbozi.getPopisZbozi()).isEqualTo(DEFAULT_POPIS_ZBOZI);
        assertThat(testZbozi.getEan()).isEqualTo(DEFAULT_EAN);
        assertThat(testZbozi.getObrazek()).isEqualTo(DEFAULT_OBRAZEK);
    }

    @Test
    @Transactional
    public void createZboziWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = zboziRepository.findAll().size();

        // Create the Zbozi with an existing ID
        zbozi.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restZboziMockMvc.perform(post("/api/zbozis")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(zbozi)))
            .andExpect(status().isBadRequest());

        // Validate the Zbozi in the database
        List<Zbozi> zboziList = zboziRepository.findAll();
        assertThat(zboziList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllZbozis() throws Exception {
        // Initialize the database
        zboziRepository.saveAndFlush(zbozi);

        // Get all the zboziList
        restZboziMockMvc.perform(get("/api/zbozis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(zbozi.getId().intValue())))
            .andExpect(jsonPath("$.[*].kategorie").value(hasItem(DEFAULT_KATEGORIE)))
            .andExpect(jsonPath("$.[*].slozeni").value(hasItem(DEFAULT_SLOZENI)))
            .andExpect(jsonPath("$.[*].popisZbozi").value(hasItem(DEFAULT_POPIS_ZBOZI)))
            .andExpect(jsonPath("$.[*].ean").value(hasItem(DEFAULT_EAN)))
            .andExpect(jsonPath("$.[*].obrazek").value(hasItem(DEFAULT_OBRAZEK.toString())));
    }
    
    @Test
    @Transactional
    public void getZbozi() throws Exception {
        // Initialize the database
        zboziRepository.saveAndFlush(zbozi);

        // Get the zbozi
        restZboziMockMvc.perform(get("/api/zbozis/{id}", zbozi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(zbozi.getId().intValue()))
            .andExpect(jsonPath("$.kategorie").value(DEFAULT_KATEGORIE))
            .andExpect(jsonPath("$.slozeni").value(DEFAULT_SLOZENI))
            .andExpect(jsonPath("$.popisZbozi").value(DEFAULT_POPIS_ZBOZI))
            .andExpect(jsonPath("$.ean").value(DEFAULT_EAN))
            .andExpect(jsonPath("$.obrazek").value(DEFAULT_OBRAZEK.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingZbozi() throws Exception {
        // Get the zbozi
        restZboziMockMvc.perform(get("/api/zbozis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateZbozi() throws Exception {
        // Initialize the database
        zboziRepository.saveAndFlush(zbozi);

        int databaseSizeBeforeUpdate = zboziRepository.findAll().size();

        // Update the zbozi
        Zbozi updatedZbozi = zboziRepository.findById(zbozi.getId()).get();
        // Disconnect from session so that the updates on updatedZbozi are not directly saved in db
        em.detach(updatedZbozi);
        updatedZbozi
            .kategorie(UPDATED_KATEGORIE)
            .slozeni(UPDATED_SLOZENI)
            .popisZbozi(UPDATED_POPIS_ZBOZI)
            .ean(UPDATED_EAN)
            .obrazek(UPDATED_OBRAZEK);

        restZboziMockMvc.perform(put("/api/zbozis")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedZbozi)))
            .andExpect(status().isOk());

        // Validate the Zbozi in the database
        List<Zbozi> zboziList = zboziRepository.findAll();
        assertThat(zboziList).hasSize(databaseSizeBeforeUpdate);
        Zbozi testZbozi = zboziList.get(zboziList.size() - 1);
        assertThat(testZbozi.getKategorie()).isEqualTo(UPDATED_KATEGORIE);
        assertThat(testZbozi.getSlozeni()).isEqualTo(UPDATED_SLOZENI);
        assertThat(testZbozi.getPopisZbozi()).isEqualTo(UPDATED_POPIS_ZBOZI);
        assertThat(testZbozi.getEan()).isEqualTo(UPDATED_EAN);
        assertThat(testZbozi.getObrazek()).isEqualTo(UPDATED_OBRAZEK);
    }

    @Test
    @Transactional
    public void updateNonExistingZbozi() throws Exception {
        int databaseSizeBeforeUpdate = zboziRepository.findAll().size();

        // Create the Zbozi

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restZboziMockMvc.perform(put("/api/zbozis")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(zbozi)))
            .andExpect(status().isBadRequest());

        // Validate the Zbozi in the database
        List<Zbozi> zboziList = zboziRepository.findAll();
        assertThat(zboziList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteZbozi() throws Exception {
        // Initialize the database
        zboziRepository.saveAndFlush(zbozi);

        int databaseSizeBeforeDelete = zboziRepository.findAll().size();

        // Delete the zbozi
        restZboziMockMvc.perform(delete("/api/zbozis/{id}", zbozi.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Zbozi> zboziList = zboziRepository.findAll();
        assertThat(zboziList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
