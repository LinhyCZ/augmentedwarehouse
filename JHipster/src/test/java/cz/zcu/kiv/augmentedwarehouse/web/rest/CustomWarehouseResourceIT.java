package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.AugmentedWarehouseApp;
import cz.zcu.kiv.augmentedwarehouse.domain.Sklad;
import cz.zcu.kiv.augmentedwarehouse.repository.SkladRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = AugmentedWarehouseApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class CustomWarehouseResourceIT {
    @Autowired
    private MockMvc restImageMockMvc;

    @Autowired
    private SkladRepository skladRepository;

    Sklad sklad;

    @BeforeEach
    public void setUp() {
        sklad = new Sklad();
        sklad.setAdresa("Testovací sklad");
        sklad.setMesto("Test");
        sklad.setCisloOrientacni(12);
        sklad.setCisloPopisne(1234);
        sklad.setpSC(12300);

        skladRepository.saveAndFlush(sklad);
    }

    @Test
    @Transactional
    public void getWarehouse() throws Exception {
        restImageMockMvc.perform(get("/custom-api/warehouses"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[0].adresa").value(sklad.getAdresa()))
            .andExpect(jsonPath("$.[0].mesto").value(sklad.getMesto()))
            .andExpect(jsonPath("$.[0].cislo_orientacni").value(sklad.getCisloOrientacni()))
            .andExpect(jsonPath("$.[0].p_sc").value(sklad.getpSC()))
            .andExpect(jsonPath("$.[0].cislo_popisne").value(sklad.getCisloPopisne()));
    }

    @Test
    @Transactional
    public void getWarehousePageable() throws Exception {
        Sklad sklad2 = new Sklad();
        sklad2.setAdresa("Testovací sklad 2");
        sklad2.setMesto("Test 2");
        sklad2.setCisloOrientacni(152);
        sklad2.setCisloPopisne(12342);
        sklad2.setpSC(12400);
        sklad2.setId(sklad.getId() + 1);

        skladRepository.saveAndFlush(sklad2);

        restImageMockMvc.perform(get("/custom-api/pageable/warehouses?pageNo=1&pageSize=1&sortBy=id"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[0].adresa").value(sklad2.getAdresa()))
            .andExpect(jsonPath("$.[0].mesto").value(sklad2.getMesto()))
            .andExpect(jsonPath("$.[0].cislo_orientacni").value(sklad2.getCisloOrientacni()))
            .andExpect(jsonPath("$.[0].p_sc").value(sklad2.getpSC()))
            .andExpect(jsonPath("$.[0].cislo_popisne").value(sklad2.getCisloPopisne()));
    }
}
