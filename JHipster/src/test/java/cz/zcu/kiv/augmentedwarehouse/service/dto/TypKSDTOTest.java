package cz.zcu.kiv.augmentedwarehouse.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import cz.zcu.kiv.augmentedwarehouse.web.rest.TestUtil;

public class TypKSDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TypKSDTO.class);
        TypKSDTO typKSDTO1 = new TypKSDTO();
        typKSDTO1.setId(1L);
        TypKSDTO typKSDTO2 = new TypKSDTO();
        assertThat(typKSDTO1).isNotEqualTo(typKSDTO2);
        typKSDTO2.setId(typKSDTO1.getId());
        assertThat(typKSDTO1).isEqualTo(typKSDTO2);
        typKSDTO2.setId(2L);
        assertThat(typKSDTO1).isNotEqualTo(typKSDTO2);
        typKSDTO1.setId(null);
        assertThat(typKSDTO1).isNotEqualTo(typKSDTO2);
    }
}
