package cz.zcu.kiv.augmentedwarehouse.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import cz.zcu.kiv.augmentedwarehouse.web.rest.TestUtil;

public class SkladDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SkladDTO.class);
        SkladDTO skladDTO1 = new SkladDTO();
        skladDTO1.setId(1L);
        SkladDTO skladDTO2 = new SkladDTO();
        assertThat(skladDTO1).isNotEqualTo(skladDTO2);
        skladDTO2.setId(skladDTO1.getId());
        assertThat(skladDTO1).isEqualTo(skladDTO2);
        skladDTO2.setId(2L);
        assertThat(skladDTO1).isNotEqualTo(skladDTO2);
        skladDTO1.setId(null);
        assertThat(skladDTO1).isNotEqualTo(skladDTO2);
    }
}
