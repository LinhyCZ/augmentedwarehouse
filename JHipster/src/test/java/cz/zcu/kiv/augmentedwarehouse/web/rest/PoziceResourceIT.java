package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.AugmentedWarehouseApp;
import cz.zcu.kiv.augmentedwarehouse.domain.Pozice;
import cz.zcu.kiv.augmentedwarehouse.repository.PoziceRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PoziceResource} REST controller.
 */
@SpringBootTest(classes = AugmentedWarehouseApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class PoziceResourceIT {

    private static final String DEFAULT_QR_CODE = "AAAAAAAAAA";
    private static final String UPDATED_QR_CODE = "BBBBBBBBBB";

    @Autowired
    private PoziceRepository poziceRepository;

    @Mock
    private PoziceRepository poziceRepositoryMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPoziceMockMvc;

    private Pozice pozice;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pozice createEntity(EntityManager em) {
        Pozice pozice = new Pozice()
            .qrCode(DEFAULT_QR_CODE);
        return pozice;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pozice createUpdatedEntity(EntityManager em) {
        Pozice pozice = new Pozice()
            .qrCode(UPDATED_QR_CODE);
        return pozice;
    }

    @BeforeEach
    public void initTest() {
        pozice = createEntity(em);
    }

    @Test
    @Transactional
    public void createPozice() throws Exception {
        int databaseSizeBeforeCreate = poziceRepository.findAll().size();

        // Create the Pozice
        restPoziceMockMvc.perform(post("/api/pozices")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pozice)))
            .andExpect(status().isCreated());

        // Validate the Pozice in the database
        List<Pozice> poziceList = poziceRepository.findAll();
        assertThat(poziceList).hasSize(databaseSizeBeforeCreate + 1);
        Pozice testPozice = poziceList.get(poziceList.size() - 1);
        assertThat(testPozice.getQrCode()).isEqualTo(DEFAULT_QR_CODE);
    }

    @Test
    @Transactional
    public void createPoziceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = poziceRepository.findAll().size();

        // Create the Pozice with an existing ID
        pozice.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPoziceMockMvc.perform(post("/api/pozices")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pozice)))
            .andExpect(status().isBadRequest());

        // Validate the Pozice in the database
        List<Pozice> poziceList = poziceRepository.findAll();
        assertThat(poziceList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPozices() throws Exception {
        // Initialize the database
        poziceRepository.saveAndFlush(pozice);

        // Get all the poziceList
        restPoziceMockMvc.perform(get("/api/pozices?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pozice.getId().intValue())))
            .andExpect(jsonPath("$.[*].qrCode").value(hasItem(DEFAULT_QR_CODE)));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllPozicesWithEagerRelationshipsIsEnabled() throws Exception {
        PoziceResource poziceResource = new PoziceResource(poziceRepositoryMock);
        when(poziceRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restPoziceMockMvc.perform(get("/api/pozices?eagerload=true"))
            .andExpect(status().isOk());

        verify(poziceRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllPozicesWithEagerRelationshipsIsNotEnabled() throws Exception {
        PoziceResource poziceResource = new PoziceResource(poziceRepositoryMock);
        when(poziceRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restPoziceMockMvc.perform(get("/api/pozices?eagerload=true"))
            .andExpect(status().isOk());

        verify(poziceRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getPozice() throws Exception {
        // Initialize the database
        poziceRepository.saveAndFlush(pozice);

        // Get the pozice
        restPoziceMockMvc.perform(get("/api/pozices/{id}", pozice.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(pozice.getId().intValue()))
            .andExpect(jsonPath("$.qrCode").value(DEFAULT_QR_CODE));
    }

    @Test
    @Transactional
    public void getNonExistingPozice() throws Exception {
        // Get the pozice
        restPoziceMockMvc.perform(get("/api/pozices/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePozice() throws Exception {
        // Initialize the database
        poziceRepository.saveAndFlush(pozice);

        int databaseSizeBeforeUpdate = poziceRepository.findAll().size();

        // Update the pozice
        Pozice updatedPozice = poziceRepository.findById(pozice.getId()).get();
        // Disconnect from session so that the updates on updatedPozice are not directly saved in db
        em.detach(updatedPozice);
        updatedPozice
            .qrCode(UPDATED_QR_CODE);

        restPoziceMockMvc.perform(put("/api/pozices")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedPozice)))
            .andExpect(status().isOk());

        // Validate the Pozice in the database
        List<Pozice> poziceList = poziceRepository.findAll();
        assertThat(poziceList).hasSize(databaseSizeBeforeUpdate);
        Pozice testPozice = poziceList.get(poziceList.size() - 1);
        assertThat(testPozice.getQrCode()).isEqualTo(UPDATED_QR_CODE);
    }

    @Test
    @Transactional
    public void updateNonExistingPozice() throws Exception {
        int databaseSizeBeforeUpdate = poziceRepository.findAll().size();

        // Create the Pozice

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPoziceMockMvc.perform(put("/api/pozices")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pozice)))
            .andExpect(status().isBadRequest());

        // Validate the Pozice in the database
        List<Pozice> poziceList = poziceRepository.findAll();
        assertThat(poziceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePozice() throws Exception {
        // Initialize the database
        poziceRepository.saveAndFlush(pozice);

        int databaseSizeBeforeDelete = poziceRepository.findAll().size();

        // Delete the pozice
        restPoziceMockMvc.perform(delete("/api/pozices/{id}", pozice.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Pozice> poziceList = poziceRepository.findAll();
        assertThat(poziceList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
