package cz.zcu.kiv.augmentedwarehouse;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("cz.zcu.kiv.augmentedwarehouse");

        noClasses()
            .that()
                .resideInAnyPackage("cz.zcu.kiv.augmentedwarehouse.service..")
            .or()
                .resideInAnyPackage("cz.zcu.kiv.augmentedwarehouse.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..cz.zcu.kiv.augmentedwarehouse.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
