package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.AugmentedWarehouseApp;
import cz.zcu.kiv.augmentedwarehouse.domain.*;
import cz.zcu.kiv.augmentedwarehouse.domain.enumeration.HlavniJednotky;
import cz.zcu.kiv.augmentedwarehouse.repository.*;
import liquibase.pro.packaged.A;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

import static org.apache.commons.lang3.SerializationUtils.deserialize;
import static org.apache.commons.lang3.SerializationUtils.serialize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = AugmentedWarehouseApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class CustomPackageWithInfoResourceIT {
    @Autowired
    private MockMvc restMockMvc;

    @Autowired
    private BaleniRepository baleniRepository;

    @Autowired
    private ZboziRepository zboziRepository;

    @Autowired
    private JednotkyRepository jednotkyRepository;

    @Autowired
    private TypKSRepository typKSRepository;

    @Autowired
    private SkladRepository skladRepository;

    @Autowired
    private StojanRepository stojanRepository;

    @Autowired
    private PoliceRepository policeRepository;

    @Autowired
    private SloupecRepository sloupecRepository;

    @Autowired
    private PoziceRepository poziceRepository;

    private Zbozi zbozi;
    private Jednotky jednotky;
    private TypKS typKS;
    private Sklad sklad;
    private Stojan stojan;
    private Police police;
    private Sloupec sloupec;
    private Pozice pozice;
    private Baleni baleni;

    @BeforeEach
    @Transactional
    public void setUp() {
        zbozi = new Zbozi();
        zbozi.setEan("121212");
        zbozi.setKategorie("Testovací kategorie");
        zbozi.setSlozeni("Testovací složení");
        zbozi.setPopisZbozi("Testovací popis zboží");

        zboziRepository.saveAndFlush(zbozi);

        jednotky = new Jednotky();
        jednotky.setHlavniJednotka(HlavniJednotky.L);
        jednotky.setNazevJednotky("Testovací jednotka");
        jednotky.setRatioDoHlavniJednotky(12.0);

        jednotkyRepository.saveAndFlush(jednotky);

        typKS = new TypKS();
        typKS.setJednotky(jednotky);
        typKS.setPocetJednotekVKS(3);
        typKS.setCenaKS(55);

        typKSRepository.saveAndFlush(typKS);

        sklad = new Sklad();
        sklad.setAdresa("Testovací sklad");
        sklad.setMesto("Test");
        sklad.setCisloOrientacni(12);
        sklad.setCisloPopisne(1234);
        sklad.setpSC(12300);

        skladRepository.saveAndFlush(sklad);

        stojan = new Stojan();
        stojan.setSklad(sklad);

        stojanRepository.saveAndFlush(stojan);

        police = new Police();
        police.setStojan(stojan);

        policeRepository.saveAndFlush(police);

        sloupec = new Sloupec();
        sloupec.setPolice(police);

        sloupecRepository.saveAndFlush(sloupec);

        pozice = new Pozice();
        pozice.setSloupec(sloupec);
        pozice.setQrCode("ADDDSSSAAS");

        baleni = new Baleni();
        baleni.setPocetKS(12);
        baleni.setZbozi(zbozi);
        baleni.setTypKS(typKS);
        baleni.setQrCode("ASDDSA");

        Set<Pozice> poziceSet = new HashSet<>();
        poziceSet.add(pozice);

        baleni.setPozices(poziceSet);

        Set<Baleni> baleniSet = new HashSet<>();
        baleniSet.add(baleni);

        pozice.setBalenis(baleniSet);

        baleniRepository.saveAndFlush(baleni);
        poziceRepository.saveAndFlush(pozice);
    }

    @Test
    @Transactional
    public void getZboziInSklad() throws Exception {
        restMockMvc.perform(get("/custom-api/zbozi/{id}", sklad.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[0].baleni_id").value(baleni.getId()))
            .andExpect(jsonPath("$.[0].pocet_ks").value(baleni.getPocetKS()))
            .andExpect(jsonPath("$.[0].pozice_id").value(pozice.getId()))
            .andExpect(jsonPath("$.[0].zbozi_ean").value(zbozi.getEan()))
            .andExpect(jsonPath("$.[0].popis_zbozi").value(zbozi.getPopisZbozi()))
            .andExpect(jsonPath("$.[0].cena_ks").value(typKS.getCenaKS()))
            .andExpect(jsonPath("$.[0].stojan_id").value(stojan.getId()))
            .andExpect(jsonPath("$.[0].sloupec_id").value(sloupec.getId()))
            .andExpect(jsonPath("$.[0].nazev_jednotky").value(jednotky.getNazevJednotky()))
            .andExpect(jsonPath("$.[0].baleni_qr_code").value(baleni.getQrCode()))
            .andExpect(jsonPath("$.[0].police_id").value(police.getId()))
            .andExpect(jsonPath("$.[0].slozeni").value(zbozi.getSlozeni()))
            .andExpect(jsonPath("$.[0].kategorie").value(zbozi.getKategorie()))
            .andExpect(jsonPath("$.[0].pocet_jednotek_vks").value(typKS.getPocetJednotekVKS()));
    }

    @Test
    @Transactional
    public void getZboziInSkladByPositionQRCode() throws Exception {
        restMockMvc.perform(get("/custom-api/zbozi/{skladId}/byPositionQr/{qrCode}", sklad.getId(), pozice.getQrCode()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[0].baleni_id").value(baleni.getId()))
            .andExpect(jsonPath("$.[0].pocet_ks").value(baleni.getPocetKS()))
            .andExpect(jsonPath("$.[0].pozice_id").value(pozice.getId()))
            .andExpect(jsonPath("$.[0].zbozi_ean").value(zbozi.getEan()))
            .andExpect(jsonPath("$.[0].popis_zbozi").value(zbozi.getPopisZbozi()))
            .andExpect(jsonPath("$.[0].cena_ks").value(typKS.getCenaKS()))
            .andExpect(jsonPath("$.[0].stojan_id").value(stojan.getId()))
            .andExpect(jsonPath("$.[0].sloupec_id").value(sloupec.getId()))
            .andExpect(jsonPath("$.[0].nazev_jednotky").value(jednotky.getNazevJednotky()))
            .andExpect(jsonPath("$.[0].baleni_qr_code").value(baleni.getQrCode()))
            .andExpect(jsonPath("$.[0].police_id").value(police.getId()))
            .andExpect(jsonPath("$.[0].slozeni").value(zbozi.getSlozeni()))
            .andExpect(jsonPath("$.[0].kategorie").value(zbozi.getKategorie()))
            .andExpect(jsonPath("$.[0].pocet_jednotek_vks").value(typKS.getPocetJednotekVKS()));
    }

    @Test
    @Transactional
    public void getZboziInSkladByQRCode() throws Exception {
        restMockMvc.perform(get("/custom-api/zbozi/{skladId}/byQr/{qrCode}", sklad.getId(), baleni.getQrCode()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[0].baleni_id").value(baleni.getId()))
            .andExpect(jsonPath("$.[0].pocet_ks").value(baleni.getPocetKS()))
            .andExpect(jsonPath("$.[0].pozice_id").value(pozice.getId()))
            .andExpect(jsonPath("$.[0].zbozi_ean").value(zbozi.getEan()))
            .andExpect(jsonPath("$.[0].popis_zbozi").value(zbozi.getPopisZbozi()))
            .andExpect(jsonPath("$.[0].cena_ks").value(typKS.getCenaKS()))
            .andExpect(jsonPath("$.[0].stojan_id").value(stojan.getId()))
            .andExpect(jsonPath("$.[0].sloupec_id").value(sloupec.getId()))
            .andExpect(jsonPath("$.[0].nazev_jednotky").value(jednotky.getNazevJednotky()))
            .andExpect(jsonPath("$.[0].baleni_qr_code").value(baleni.getQrCode()))
            .andExpect(jsonPath("$.[0].police_id").value(police.getId()))
            .andExpect(jsonPath("$.[0].slozeni").value(zbozi.getSlozeni()))
            .andExpect(jsonPath("$.[0].kategorie").value(zbozi.getKategorie()))
            .andExpect(jsonPath("$.[0].pocet_jednotek_vks").value(typKS.getPocetJednotekVKS()));
    }

    @Test
    @Transactional
    public void getZboziInSkladByBaleniId() throws Exception {
        restMockMvc.perform(get("/custom-api/zbozi/{skladId}/{baleniId}", sklad.getId(), baleni.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[0].baleni_id").value(baleni.getId()))
            .andExpect(jsonPath("$.[0].pocet_ks").value(baleni.getPocetKS()))
            .andExpect(jsonPath("$.[0].pozice_id").value(pozice.getId()))
            .andExpect(jsonPath("$.[0].zbozi_ean").value(zbozi.getEan()))
            .andExpect(jsonPath("$.[0].popis_zbozi").value(zbozi.getPopisZbozi()))
            .andExpect(jsonPath("$.[0].cena_ks").value(typKS.getCenaKS()))
            .andExpect(jsonPath("$.[0].stojan_id").value(stojan.getId()))
            .andExpect(jsonPath("$.[0].sloupec_id").value(sloupec.getId()))
            .andExpect(jsonPath("$.[0].nazev_jednotky").value(jednotky.getNazevJednotky()))
            .andExpect(jsonPath("$.[0].baleni_qr_code").value(baleni.getQrCode()))
            .andExpect(jsonPath("$.[0].police_id").value(police.getId()))
            .andExpect(jsonPath("$.[0].slozeni").value(zbozi.getSlozeni()))
            .andExpect(jsonPath("$.[0].kategorie").value(zbozi.getKategorie()))
            .andExpect(jsonPath("$.[0].pocet_jednotek_vks").value(typKS.getPocetJednotekVKS()));
    }

    @Test
    @Transactional
    public void getZboziInSkladPageable() throws Exception {
        Baleni baleni2 = new Baleni();
        baleni2.setPocetKS(12);
        baleni2.setZbozi(zbozi);
        baleni2.setTypKS(typKS);
        baleni2.setQrCode("ASDDSA");

        Pozice pozice2 = new Pozice();
        pozice2.setSloupec(sloupec);

        Set<Pozice> pozice2Set = new HashSet<>();
        pozice2Set.add(pozice2);

        baleni.setPozices(pozice2Set);

        Set<Baleni> baleni2Set = new HashSet<>();
        baleni2Set.add(baleni2);

        pozice2.setBalenis(baleni2Set);

        baleniRepository.saveAndFlush(baleni2);
        poziceRepository.saveAndFlush(pozice2);

        restMockMvc.perform(get("/custom-api/pageable/zbozi/{id}?pageNo=1&pageSize=1&sortBy=baleni_id", sklad.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[0].baleni_id").value(baleni2.getId()))
            .andExpect(jsonPath("$.[0].pocet_ks").value(baleni2.getPocetKS()))
            .andExpect(jsonPath("$.[0].pozice_id").value(pozice2.getId()))
            .andExpect(jsonPath("$.[0].zbozi_ean").value(zbozi.getEan()))
            .andExpect(jsonPath("$.[0].popis_zbozi").value(zbozi.getPopisZbozi()))
            .andExpect(jsonPath("$.[0].cena_ks").value(typKS.getCenaKS()))
            .andExpect(jsonPath("$.[0].stojan_id").value(stojan.getId()))
            .andExpect(jsonPath("$.[0].sloupec_id").value(sloupec.getId()))
            .andExpect(jsonPath("$.[0].nazev_jednotky").value(jednotky.getNazevJednotky()))
            .andExpect(jsonPath("$.[0].baleni_qr_code").value(baleni2.getQrCode()))
            .andExpect(jsonPath("$.[0].police_id").value(police.getId()))
            .andExpect(jsonPath("$.[0].slozeni").value(zbozi.getSlozeni()))
            .andExpect(jsonPath("$.[0].kategorie").value(zbozi.getKategorie()))
            .andExpect(jsonPath("$.[0].pocet_jednotek_vks").value(typKS.getPocetJednotekVKS()));
    }

    @Test
    @Transactional
    public void getZboziInSkladByQRCodePageable() throws Exception {
        Baleni baleni2 = new Baleni();
        baleni2.setPocetKS(12);
        baleni2.setZbozi(zbozi);
        baleni2.setTypKS(typKS);
        baleni2.setQrCode("ASDDSA");

        Pozice pozice2 = new Pozice();
        pozice2.setSloupec(sloupec);

        Set<Pozice> pozice2Set = new HashSet<>();
        pozice2Set.add(pozice2);

        baleni.setPozices(pozice2Set);

        Set<Baleni> baleni2Set = new HashSet<>();
        baleni2Set.add(baleni2);

        pozice2.setBalenis(baleni2Set);

        baleniRepository.saveAndFlush(baleni2);
        poziceRepository.saveAndFlush(pozice2);

        restMockMvc.perform(get("/custom-api/pageable/zbozi/{skladId}/byQr/{qrCode}?pageNo=1&pageSize=1&sortBy=baleni_id", sklad.getId(), baleni.getQrCode()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[0].baleni_id").value(baleni2.getId()))
            .andExpect(jsonPath("$.[0].pocet_ks").value(baleni2.getPocetKS()))
            .andExpect(jsonPath("$.[0].pozice_id").value(pozice2.getId()))
            .andExpect(jsonPath("$.[0].zbozi_ean").value(zbozi.getEan()))
            .andExpect(jsonPath("$.[0].popis_zbozi").value(zbozi.getPopisZbozi()))
            .andExpect(jsonPath("$.[0].cena_ks").value(typKS.getCenaKS()))
            .andExpect(jsonPath("$.[0].stojan_id").value(stojan.getId()))
            .andExpect(jsonPath("$.[0].sloupec_id").value(sloupec.getId()))
            .andExpect(jsonPath("$.[0].nazev_jednotky").value(jednotky.getNazevJednotky()))
            .andExpect(jsonPath("$.[0].baleni_qr_code").value(baleni2.getQrCode()))
            .andExpect(jsonPath("$.[0].police_id").value(police.getId()))
            .andExpect(jsonPath("$.[0].slozeni").value(zbozi.getSlozeni()))
            .andExpect(jsonPath("$.[0].kategorie").value(zbozi.getKategorie()))
            .andExpect(jsonPath("$.[0].pocet_jednotek_vks").value(typKS.getPocetJednotekVKS()));
    }

    @Test
    @Transactional
    public void getZboziInSkladByQRPositionCodePageable() throws Exception {
        Baleni baleni2 = new Baleni();
        baleni2.setPocetKS(12);
        baleni2.setZbozi(zbozi);
        baleni2.setTypKS(typKS);
        baleni2.setQrCode("ASDDSA");

        Pozice pozice2 = new Pozice();
        pozice2.setSloupec(sloupec);
        pozice2.setQrCode("ADDDSSSAAS");

        Set<Pozice> pozice2Set = new HashSet<>();
        pozice2Set.add(pozice2);

        baleni.setPozices(pozice2Set);

        Set<Baleni> baleni2Set = new HashSet<>();
        baleni2Set.add(baleni2);

        pozice2.setBalenis(baleni2Set);

        baleniRepository.saveAndFlush(baleni2);
        poziceRepository.saveAndFlush(pozice2);

        restMockMvc.perform(get("/custom-api/pageable/zbozi/{skladId}/byPositionQr/{qrCode}?pageNo=1&pageSize=1&sortBy=baleni_id", sklad.getId(), pozice2.getQrCode()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[0].baleni_id").value(baleni2.getId()))
            .andExpect(jsonPath("$.[0].pocet_ks").value(baleni2.getPocetKS()))
            .andExpect(jsonPath("$.[0].pozice_id").value(pozice2.getId()))
            .andExpect(jsonPath("$.[0].zbozi_ean").value(zbozi.getEan()))
            .andExpect(jsonPath("$.[0].popis_zbozi").value(zbozi.getPopisZbozi()))
            .andExpect(jsonPath("$.[0].cena_ks").value(typKS.getCenaKS()))
            .andExpect(jsonPath("$.[0].stojan_id").value(stojan.getId()))
            .andExpect(jsonPath("$.[0].sloupec_id").value(sloupec.getId()))
            .andExpect(jsonPath("$.[0].nazev_jednotky").value(jednotky.getNazevJednotky()))
            .andExpect(jsonPath("$.[0].baleni_qr_code").value(baleni2.getQrCode()))
            .andExpect(jsonPath("$.[0].police_id").value(police.getId()))
            .andExpect(jsonPath("$.[0].slozeni").value(zbozi.getSlozeni()))
            .andExpect(jsonPath("$.[0].kategorie").value(zbozi.getKategorie()))
            .andExpect(jsonPath("$.[0].pocet_jednotek_vks").value(typKS.getPocetJednotekVKS()));
    }
}
