package cz.zcu.kiv.augmentedwarehouse.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import cz.zcu.kiv.augmentedwarehouse.web.rest.TestUtil;

public class BaleniDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BaleniDTO.class);
        BaleniDTO baleniDTO1 = new BaleniDTO();
        baleniDTO1.setId(1L);
        BaleniDTO baleniDTO2 = new BaleniDTO();
        assertThat(baleniDTO1).isNotEqualTo(baleniDTO2);
        baleniDTO2.setId(baleniDTO1.getId());
        assertThat(baleniDTO1).isEqualTo(baleniDTO2);
        baleniDTO2.setId(2L);
        assertThat(baleniDTO1).isNotEqualTo(baleniDTO2);
        baleniDTO1.setId(null);
        assertThat(baleniDTO1).isNotEqualTo(baleniDTO2);
    }
}
