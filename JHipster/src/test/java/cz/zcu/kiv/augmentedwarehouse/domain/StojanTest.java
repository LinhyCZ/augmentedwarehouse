package cz.zcu.kiv.augmentedwarehouse.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import cz.zcu.kiv.augmentedwarehouse.web.rest.TestUtil;

public class StojanTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Stojan.class);
        Stojan stojan1 = new Stojan();
        stojan1.setId(1L);
        Stojan stojan2 = new Stojan();
        stojan2.setId(stojan1.getId());
        assertThat(stojan1).isEqualTo(stojan2);
        stojan2.setId(2L);
        assertThat(stojan1).isNotEqualTo(stojan2);
        stojan1.setId(null);
        assertThat(stojan1).isNotEqualTo(stojan2);
    }
}
