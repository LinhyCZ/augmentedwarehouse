package cz.zcu.kiv.augmentedwarehouse.web.rest;

import cz.zcu.kiv.augmentedwarehouse.AugmentedWarehouseApp;
import cz.zcu.kiv.augmentedwarehouse.domain.Police;
import cz.zcu.kiv.augmentedwarehouse.repository.PoliceRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.client.RequestMatcher;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PoliceResource} REST controller.
 */
@SpringBootTest(classes = AugmentedWarehouseApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class PoliceResourceIT {

    @Autowired
    private PoliceRepository policeRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPoliceMockMvc;

    private Police police;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Police createEntity(EntityManager em) {
        Police police = new Police();
        return police;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Police createUpdatedEntity(EntityManager em) {
        Police police = new Police();
        return police;
    }

    @BeforeEach
    public void initTest() {
        police = createEntity(em);
    }

    @Test
    @Transactional
    public void createPolice() throws Exception {
        int databaseSizeBeforeCreate = policeRepository.findAll().size();

        // Create the Police
        restPoliceMockMvc.perform(post("/api/police")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(police)))
            .andExpect(status().isCreated());

        // Validate the Police in the database
        List<Police> policeList = policeRepository.findAll();
        assertThat(policeList).hasSize(databaseSizeBeforeCreate + 1);
        Police testPolice = policeList.get(policeList.size() - 1);
    }

    @Test
    @Transactional
    public void createPoliceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = policeRepository.findAll().size();

        // Create the Police with an existing ID
        police.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPoliceMockMvc.perform(post("/api/police")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(police)))
            .andExpect(status().isBadRequest());

        // Validate the Police in the database
        List<Police> policeList = policeRepository.findAll();
        assertThat(policeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPolice() throws Exception {
        // Initialize the database
        policeRepository.saveAndFlush(police);

        // Get all the policeList
        restPoliceMockMvc.perform(get("/api/police?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(police.getId().intValue())));
    }

    @Test
    @Transactional
    public void getPolice() throws Exception {
        // Initialize the database
        policeRepository.saveAndFlush(police);

        // Get the police
        restPoliceMockMvc.perform(get("/api/police/{id}", police.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(police.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingPolice() throws Exception {
        // Get the police
        restPoliceMockMvc.perform(get("/api/police/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePolice() throws Exception {
        // Initialize the database
        policeRepository.saveAndFlush(police);

        int databaseSizeBeforeUpdate = policeRepository.findAll().size();

        // Update the police
        Police updatedPolice = policeRepository.findById(police.getId()).get();
        // Disconnect from session so that the updates on updatedPolice are not directly saved in db
        em.detach(updatedPolice);

        restPoliceMockMvc.perform(put("/api/police")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedPolice)))
            .andExpect(status().isOk());

        // Validate the Police in the database
        List<Police> policeList = policeRepository.findAll();
        assertThat(policeList).hasSize(databaseSizeBeforeUpdate);
        Police testPolice = policeList.get(policeList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingPolice() throws Exception {
        int databaseSizeBeforeUpdate = policeRepository.findAll().size();

        // Create the Police

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPoliceMockMvc.perform(put("/api/police")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(police)))
            .andExpect(status().isBadRequest());

        // Validate the Police in the database
        List<Police> policeList = policeRepository.findAll();
        assertThat(policeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePolice() throws Exception {
        // Initialize the database
        policeRepository.saveAndFlush(police);

        int databaseSizeBeforeDelete = policeRepository.findAll().size();

        // Delete the police
        restPoliceMockMvc.perform(delete("/api/police/{id}", police.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Police> policeList = policeRepository.findAll();
        assertThat(policeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
