package cz.zcu.kiv.augmentedwarehouse.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class JednotkyMapperTest {

    private JednotkyMapper jednotkyMapper;

    @BeforeEach
    public void setUp() {
        jednotkyMapper = new JednotkyMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(jednotkyMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(jednotkyMapper.fromId(null)).isNull();
    }
}
