package cz.zcu.kiv.augmentedwarehouse.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class StojanMapperTest {

    private StojanMapper stojanMapper;

    @BeforeEach
    public void setUp() {
        stojanMapper = new StojanMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(stojanMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(stojanMapper.fromId(null)).isNull();
    }
}
